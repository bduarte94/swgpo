<%@ include file="include/cabecera.jspf"%>
<script type="text/javascript">
	$(document).ready(function()
		{
		$("#boton").click(function () {	 
			$('#dialog').toggle("slow");
			$('#Present').toggle("slow");
			});
		$("#boton1").click(function () {	 
			$('#target').toggle("fast");
			});
		$("#boton2").click(function () {	 
			$('#target').toggle(5000);
			});
		 });
</script>
<% 
String log;
if(request.getParameter("log")!=null){
		log=request.getParameter("log").toString();
		if(log.compareTo("nok")==0)
			out.write("<script type=\"text/javascript\">"+
		"$(function(){"+
			"$('#dialog').toggle('slow',function(){"+
			"$('#logpass').show('slow')"+
			"});"+
			"$('#Present').toggle('slow');"+	
					"})</script>");
		else
			out.write("<script type=\"text/javascript\">"+
					"$(function(){"+
						"$('#dialog').toggle('slow',function(){"+
						"$('#estado').show('slow')"+
						"});"+
						"$('#Present').toggle('slow');"+	
								"})</script>");
}			
%>
</head>
<body>

<%@ include file="include/menu.jspf"%>

<!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container-wrapper">
			<br><br><br>
			<div id="dialog" title="dialog simple" style="display:none;" >
				<form action="ManejarPeticiones" method="POST">
				<div id="form2" class="panel panel-primary" style="widht:500px;height:300px;">
					<div class="panel panel-heading">
						<h2>Iniciar Sesion</h2>
					</div>
					<div class="panel panel-body">
						<div class="Container-fluis">
                 			 <div class="row row-centered">
                   				<div class="col-xs-2 col-centered""><label style="Color:Black">Usuario:</label></div>
                  				<div class="col-xs-6 col-centered""><input id="user" type="text" class="form-control" name="txtUsuario" placeholder="Ingrese Usuario"></div>
                        		
                        	</div>
                        	<div class="row row-centered">
                    			<div class="col-xs-2 col-centered"><label style="Color:Black">Contrasena:</label></div>
                  				<div class="col-xs-6 col-centered"><input id="pass" type="password" class="form-control" name="txtPasword" placeholder="Ingrese Contrasena"></div>     	 
							</div>
							<br>
							<div id="logpass" class="alert alert-danger" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Error!</strong>&nbsp; Ingrese valores validos para usuario y contrasena
                    		</div>
                 			<div id="estado" class="alert alert-warning" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Aviso!</strong>&nbsp; La cuenta no esta activada
                    		</div>	
							
							<input type="hidden" id="class" name="class" value="Login.Login">
                        	<input type="hidden" id="method" name="method" value="iniciarSesion">
                        	
					<div class="panel-footer">
						<button class="btn btn-large btn-primary" type="submit" value="login" name="Login">
                  		 <span class="glyphicon glyphicon-log-in"></span> Login
                   		</button>
                   		<br>
                   		<a href="RegistrarOdontologo.jsp">No tienes una Cuenta? Resgistrate aqui</a>
					</div>
				</div>
				</div>
			</div>
			</form>
			</div>
	
			
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message" id="Present">
                        <h1>Unete a nuestro sistema</h1>
                        <h3>Y se parte del nuevo mundo de oportunidades en tu gestion</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                           <!--  <li>
                                <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                            </li> -->
                            <li>
                                <a href="RegistrarClinica.jsp" class="btn btn-info btn-lg"><i class="fa fa-star fa-fw"></i> <span class="network-name">Registrate</span></a>
                            </li>
                            <!--  <li>
                                <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Registra a tus pacientes<br>de la manera mas facil</h2>
                    <p class="lead">Podras registrar y actualizar informacion acerca de tus pacientes, desde cualquier dispositivo conectado a internet</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="img/ipad.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Programa Citas<br>por medio del sistema</h2>
                    <p class="lead">Ahora podras mentener tu agenda de servicios a realizar bien organizada, recibe notificaciones de citas proximas</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="img/dog.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Generar Reportes<br>para mejorar tu productividad</h2>
                    <p class="lead">Ahora podras gestionar mejor tus recursos y acelerar la forma en como realizabas tus reportes medicos.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="img/phones.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

	<a  name="contact"></a>
    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Connectate y comienza a usar ODONTOWEB:</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                        </li>
                        <li>
                            <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

   <%@include file="include/footer.jspf" %>
</body>

</html>
