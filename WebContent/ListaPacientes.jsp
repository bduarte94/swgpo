<%@ include file="include/cabecera.jspf"%>
<%@page import="Common.PacienteCOM"%>

<style>
<!--

body {
	
}
/* #content{
	padding-top: 100px;
    padding-right: 50px;
    padding-bottom: 25px;
    padding-left: 50px;
} */

.img {
  display: block;
  max-width:50px;
  max-height:75px;
  width: auto;
  height: auto;
}
#busq2{
	width: 100%;
	height: 350px;
	overflow-y: scroll;
	overflow:auto;
    background: #fff;
   
}
#content{
	margin-top: 50px;
}

-->
</style>
<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

	String idOdontologo = "";
	String idClinica = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
%>

<script type="text/javascript">
	$(document).ready(function() { //*****************READY******************************************************
		 $.ajax({
	        	type:'POST',
		        url: 'ManejarPeticiones',
		        data:{
				'class' : 'Common.PacienteCOM',
				'method': 'ListaPaciente',
				idClinica: <%=idClinica%> 
		        },
		        success: function(data) {
		        	$('#busq2').html(data);
		        }
		
	        });
	});
</script>
</head>
<body>
<%@ include file="include/menu.jspf" %>
	
	<div id="content" class="container" >
		<div class="row">
			<h4><span class="glyphicon glyphicon-list"></span>Lista De Pacientes</h4>
		</div>
		<div class="row" id="busq2">
			<%-- <%  
				out.write(new PacienteCOM().ListaPaciente(idClinica));
			%> --%>
		</div> 
	</div>
</body>
	<%@ include file="include/footer2.jspf" %>
</html>
