<%@page import="javax.swing.JOptionPane"%>
<%@page import="Dao.OdontogramaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro odontograma</title>
        <style type="text/css">
            input{
                border-radius:10px;
                box-shadow:0px 0px 25px rgba(30,144,255,0.3) inset;
                width: 200px;
                height: 30px;
                padding: 5px;
                margin: 5px;

            }
            #contenedor{
                width: 500px;
                border: 1px solid rgba(255,255,255,0.4);
                padding: 20px;
                margin: 50px;
                border-radius: 25px 25px 25px 25px;
                box-shadow:0px 10px 25px #0066cc;
                border-color: #ffffff;

            }
            td{
                text-align: right;
                font-size: 25px italic;

            }

        </style>
    </head>
    <body>

      

    <center>
        <h3>REGISTRO ODONTOGRAMA</h3>
        <hr color="#003399"/>
        <div id="contenedor">
            <form action="odonto.jsp">
                <table>

                    <tr>
                        
                          <% 
        
        OdontogramaDAO objetoOdonto = new OdontogramaDAO();
        String idPaciente,IdDiente,IdTratamiento,IdServicio,IdEstadoDiente,IdPadecimiento;
        String idOdonto = "null";
        String atencion;
        
        idPaciente = request.getParameter("selecPaciente");
        IdDiente = request.getParameter("selecDiente");
        IdTratamiento = request.getParameter("selecTratamiento");
        IdServicio = request.getParameter("selecServicio");
        IdEstadoDiente = request.getParameter("selectEstadoDiente");
        IdPadecimiento = request.getParameter("selectPadecimiento");
        atencion = request.getParameter("selectAtencion");
    
             
        if (objetoOdonto.insertarRegistro(idPaciente, IdDiente, IdTratamiento, 
                IdServicio, IdEstadoDiente, IdPadecimiento, atencion))
        {
            
            out.println(" <td colspan='2'><h3>El odontograma se ha registrado con éxito</h3></td>");
            
        }
        
        else{
            out.println(" <td colspan='2'><h3>Error al registrar Odontograma</h3></td>");
            JOptionPane.showMessageDialog(null, idPaciente);
            JOptionPane.showMessageDialog(null, IdDiente);
            JOptionPane.showMessageDialog(null, IdTratamiento);
            JOptionPane.showMessageDialog(null, IdServicio);
            JOptionPane.showMessageDialog(null, IdEstadoDiente);
            JOptionPane.showMessageDialog(null, IdPadecimiento);
            
        }
       
            
     
        
        
        %>
                       
                        
                    </tr> 
                  
                    <tr>
                        <td colspan="2"> <input type="submit" name="btnRegresar" value="Regresar" /> </td>
                    </tr> 
            </form>
            </table>   
            </form>

        </div>

    </center>
</body>

</html>
