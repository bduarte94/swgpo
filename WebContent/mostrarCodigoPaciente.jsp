
<%@page import="Entidades.Odontograma"%>


<head>
    <link href="css/registrar_pac.css" rel="stylesheet">

    <style type="text/css">
        input{
            border-radius:10px;
            box-shadow:0px 0px 25px rgba(30,144,255,0.3) inset;
            width: 200px;
            height: 30px;
            padding: 5px;
            margin: 5px;

        }
        table{
            width: 500px;
            border: 1px solid rgba(255,255,255,0.4);
            padding: 20px;
            margin: 50px;
            border-radius: 25px 25px 25px 25px;
            box-shadow:0px 10px 25px #0066cc;
            border-color: #ffffff;
            overflow: scroll;

        }
        td{
            text-align: left;
            font-size: 18px italic;

        }
        .titulo{
            font:bold 25px;

        }

    </style>


</head>
<body>
    <%@ include file="include/cabecera.jspf"%>

    <%Odontograma odonto = (Odontograma) request.getAttribute("opaciente");

        %>
    <div align="center">


        <%@ include file="include/menu.jspf" %>
        <br><br>
        <div id="contenido" class="container-wrapper" align="Center">	


            <div class="page-header" align="center">
                <h3 class="text text-primary">Editar Odontrograma</h3>
            </div>

            <form id="formRP" class="form"method="GET" action="verificarCodigoOdonto">
                <div class="row">

                    <div class="col-sm-9">
                        <div class="panel panel-primary" align="Center" style="width: 80em" >
                            <div class="panel panel-heading">
                                <h4>Exito...</h4>
                            </div>

                            <div class="row" align="center">

                                <table>
                                    <div class="col-sm-3" style="width:20%; left: 40%">
                                        <table>
                                            <tr>
                                                <td>Nombre:</td>
                                                           <td><input type="text" size="20" name="nombre" value="<%= odonto.getIdPacienta()%>"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </table>

                            </div>



                        </div>
                    </div>
                </div>


            </form>
        </div>

        <%@ include file="include/footer.jspf" %>
</body>
