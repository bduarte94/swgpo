<%@page import="java.util.ArrayList"%>
<%@ include file="include/cabecera.jspf"%>



<head>

    <style type="text/css">
        input{
            border-radius:10px;
            box-shadow:0px 0px 25px rgba(30,144,255,0.3) inset;
            width: 200px;
            height: 30px;
            padding: 5px;
            margin: 5px;

        }
        table{
            width: 700px;
            border: 1px solid rgba(255,255,255,0.4);
            padding: 20px;
            margin: 50px;
            border-radius: 25px 25px 25px 25px;
            box-shadow:0px 10px 25px #0066cc;
            border-color: #ffffff;
            overflow: scroll;

        }
        td{
            text-align:  center;
            font-size: 18px italic;


        }

        th{
            text-align:  center;
            font-size: 18px italic;


        }
        .titulo{
            font:bold 25px;

        }

    </style>

    <script type="text/javascript">
        $(function () {
            $('#fecha').datetimepicker({
                format: 'MM/DD/YYYY',
                viewMode: 'years'

            });

        });
    </script>	

    <script type="text/javascript">
        $(function () {
            $('#fecha2').datetimepicker({
                format: 'MM/DD/YYYY',
                viewMode: 'years'

            });

        });
    </script>	


</head>
<body>
    <%@ include file="include/menu.jspf" %>
    <br><br>
    <div id="contenido" class="container-wrapper" align="Center">	



        <form id="formRP" class="form" method="GET" action="Reportes/RCitas.jsp">
            <div class="row">

                <div class="col-sm-9">
                    <div class="panel panel-primary" align="Center" style="width: 80em" >
                        <div class="panel panel-heading">
                            <h4>Reporte de las citas atendidas</h4>
                        </div>

                        <div class="row">

                            <table border="2" align="center" cellpadding="1px" cellspacing="1px">
                                <tr>
                                    <td><label>Ingrese el c�digo del Paciente</label></td>
                                    <td><input type="text" name="txtCodigoPaciente"></td>
                                </tr>
                                <tr>
                                    <td><label>Ingrese el c�digo del Odontologo</label></td>
                                    <td><input type="text" name="txtCodigoOdontologo"></td>

                                </tr>

                                <tr>
                                    <td><label>Ingrese la fecha Inicial</label></td>
                                    <td style="width: 15em">

                                        <input name="fechaI" type="date" />
                                        </td>

                                </tr>

                                <tr>
                                    <td><label>Ingrese la fecha Final</label></td>
                                    <td style="width: 15em">
                                    <input name="fechaF" type="date"/>
                                        
                                    </td>

                                </tr>

                            </table>
                        </div>
                        <input type="submit" class="btn-success" id="btnEnviar" name="btnEnviar" value="Generar Reporte">

                    </div>



                </div>



            </div>

        </form>



    </div>

    <%@ include file="include/footer.jspf" %>
    <script src="js/bootstrap-datetimepicker.js"></script>
</body>
