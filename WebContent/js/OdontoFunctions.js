//archivo que contiene las funciones del odontograma, asi como los eventos del camvas

function getPrecioServicio(idServicio){
	var ret="";
	$.ajax({
		type:'GET',
        url: 'ManejarPeticiones',
        data:{
		'class' : 'Common.CatalogosCOM',
		'method' : 'AjaxGetPrecioServicioById',
		idServicio: idServicio
        },
		async: false,
		success: function(data) {
			if(data!=undefined){	
				ret= data;
			}
		}
	});
	return ret;
}

function getSaldo(idCredito){
	var ret="";
	$.ajax({
		type:'GET',
        url: 'ManejarPeticiones',
        data:{
		'class' : 'Common.CreditosCOM',
		'method' : 'AjaxSaldoByCredito',
		idCredito: idCredito
        },
		async: false,
		success: function(data) {
			if(data!=undefined){	
				ret= data;
			}
		}
	});
	return ret;
}

function getLimiteCredito(idCredito){
	var ret="";
	$.ajax({
		type:'GET',
        url: 'ManejarPeticiones',
        data:{
		'class' : 'Common.CreditosCOM',
		'method' : 'AjaxgetLimiteCredito',
		idCredito: idCredito
        },
		async: false,
		success: function(data) {
			if(data!=undefined){	
				ret= data;
			}
		}
	});
	return ret;
}


var X, Y, W, H, r;
function inicializarCanvas() {
	// var canvas = document.querySelector("#canvas");
	var canvas = new fabric.Canvas('canvas');
	var selectedObject;
	var source;
	var img = new Image();
	// indico la URL de la imagen
	img.src = 'img/ODO.png';
	img.id = "odo";
	// defino el evento onload del objeto imagen
	img.onload = function() {
		// incluyo la imagen en el canvas
		// ctx.drawImage(img,0,0,W,H);
		fabric.Image.fromURL(img.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:0,
				left : 0,
				top : 0,
				// width: canvas.getWidth(),
				// height: canvas.getHeight(),
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})

			canvas.add(oImg);
		});

		var img18 = new Image();
		img18.src = 'img/dientes/18-11/18.png';
		fabric.Image.fromURL(img18.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:18,
				left : 52,
				top : 279,
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})

		var img17 = new Image();
		img17.src = 'img/dientes/18-11/17.png';
		fabric.Image.fromURL(img17.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:17,
				left : 59,
				top : 235,
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img16 = new Image();
		img16.src = 'img/dientes/18-11/16.png';
		fabric.Image.fromURL(img16.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:16,
				left : 68,
				top : 190,
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img15 = new Image();
		img15.src = 'img/dientes/18-11/15.png';
		fabric.Image.fromURL(img15.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:15,
				left : 92,
				top : 152,
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img14 = new Image();
		img14.src = 'img/dientes/18-11/14.png';
		fabric.Image.fromURL(img14.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:14,
				left : 100,//114
				top : 118,//138
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img13 = new Image();
		img13.src = 'img/dientes/18-11/13.png';
		fabric.Image.fromURL(img13.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:13,
				left : 109,//14
				top : 81,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img12 = new Image();
		img12.src = 'img/dientes/18-11/12.png';
		fabric.Image.fromURL(img12.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:12,
				left : 142,//21
				top : 66,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img11 = new Image();
		img11.src = 'img/dientes/18-11/11.png';
		fabric.Image.fromURL(img11.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:12,
				left : 179,//21
				top : 71,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img28 = new Image();
		img28.src = 'img/dientes/28-21/28.png';
		fabric.Image.fromURL(img28.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:28,
				left : 335,//21
				top : 280,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img27 = new Image();
		img27.src = 'img/dientes/28-21/27.png';
		fabric.Image.fromURL(img27.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:27,
				left : 326,//21
				top : 235,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img26 = new Image();
		img26.src = 'img/dientes/28-21/26.png';
		fabric.Image.fromURL(img26.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:26,
				left : 316,//21
				top : 188,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img25 = new Image();
		img25.src = 'img/dientes/28-21/25.png';
		fabric.Image.fromURL(img25.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:25,
				left : 311,//21
				top : 150,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img24 = new Image();
		img24.src = 'img/dientes/28-21/24.png';
		fabric.Image.fromURL(img24.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:24,
				left : 303,//21
				top : 116,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img23 = new Image();
		img23.src = 'img/dientes/28-21/23.png';
		fabric.Image.fromURL(img23.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:23,
				left : 290,//21
				top : 85,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img22 = new Image();
		img22.src = 'img/dientes/28-21/22.png';
		fabric.Image.fromURL(img22.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:22,
				left : 256,//21
				top : 69,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img21 = new Image();
		img21.src = 'img/dientes/28-21/21.png';
		fabric.Image.fromURL(img21.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:21,
				left : 219,//21
				top : 70,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		
		var img38 = new Image();
		img38.src = 'img/dientes/38-31/38.png';
		fabric.Image.fromURL(img38.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:38,
				left : 327,//21
				top : 385,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img37 = new Image();
		img37.src = 'img/dientes/38-31/37.png';
		fabric.Image.fromURL(img37.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:37,
				left : 319,//21
				top : 431,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img36 = new Image();
		img36.src = 'img/dientes/38-31/36.png';
		fabric.Image.fromURL(img36.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:36,
				left : 310,//21
				top : 475,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img35 = new Image();
		img35.src = 'img/dientes/38-31/35.png';
		fabric.Image.fromURL(img35.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:35,
				left : 302,//21
				top : 519,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img34 = new Image();
		img34.src = 'img/dientes/38-31/34.png';
		fabric.Image.fromURL(img34.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:34,
				left : 293,//21
				top : 553,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img33 = new Image();
		img33.src = 'img/dientes/38-31/33.png';
		fabric.Image.fromURL(img33.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:33,
				left : 270,//21
				top : 584,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		var img32 = new Image();
		img32.src = 'img/dientes/38-31/32.png';
		fabric.Image.fromURL(img32.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:32,
				left : 248,//21
				top : 596,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img31 = new Image();
		img31.src = 'img/dientes/38-31/31.png';
		fabric.Image.fromURL(img31.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:31,
				left : 218,//21
				top : 599,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img48 = new Image();
		img48.src = 'img/dientes/48-41/48.png';
		fabric.Image.fromURL(img48.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:48,
				left : 58,//21
				top : 388,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img47 = new Image();
		img47.src = 'img/dientes/48-41/47.png';
		fabric.Image.fromURL(img47.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:47,
				left : 69,//21
				top : 433,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img46 = new Image();
		img46.src = 'img/dientes/48-41/46.png';
		fabric.Image.fromURL(img46.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:46,
				left : 78,//21
				top : 477,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		var img45 = new Image();
		img45.src = 'img/dientes/48-41/45.png';
		fabric.Image.fromURL(img45.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:45,
				left : 94,//21
				top : 523,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img44 = new Image();
		img44.src = 'img/dientes/48-41/44.png';
		fabric.Image.fromURL(img44.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:44,
				left : 109,//21
				top : 557,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img43 = new Image();
		img43.src = 'img/dientes/48-41/43.png';
		fabric.Image.fromURL(img43.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:43,
				left : 127,//21
				top : 587,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})

		var img42 = new Image();
		img42.src = 'img/dientes/48-41/42.png';
		fabric.Image.fromURL(img42.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:42,
				left : 161,//21
				top : 596,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})
		
		var img41 = new Image();
		img41.src = 'img/dientes/48-41/41.png';
		fabric.Image.fromURL(img41.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:41,
				left : 191,//21
				top : 598,//23
				// width: canvas.getWidth()-390,
				// height: canvas.getHeight()-650,
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		})




		
		canvas.on('object:selected', function(e) {
			e.target.setFill('green');
			canvas.renderAll();
		}

		)
	}
	
	
	canvas.on('mouse:over', function(e) {
	   //e.target.setFill('red');
	   // canvas.renderAll();
		//alert('im here');
		
		if(e.target.get('id')!=0){
		var filter = new fabric.Image.filters.Tint({
			  color: '#00BFFF',
			  opacity: 0.3
			});
		e.target.filters.push(filter);
		e.target.applyFilters(canvas.renderAll.bind(canvas));
			//applyFilter(e.target,filter);
		}
	  });

	  canvas.on('mouse:out', function(e) {
		  if(e.target.get('id')!=0){
			  e.target.filters = [];
			//e.target.filters.push(filter);
			e.target.applyFilters(function() { e.target.canvas.renderAll(); });
			
				//applyFilter(e.target,filter);
			}
	  });
	  
	 
		canvas.on('mouse:down', function(e){
			
			/*var pointer = canvas.getPointer(e.e);
		    var posx = pointer.x;
		    var posy = pointer.y;
		    console.log(posx+","+posy);*/
				
			if(e.target.get('id')!=0){
				if($('#pacientesCBX').val()!='0'){ //valida si se ha seleccionado paciente
				//$('#idDiente').val(e.target.get('id'));
					$.get('ManejarPeticiones', {
						class : "Common.AtencionCOM",
						method : "AjaxItemsAdd",
						idDiente: e.target.get('id')
					}, function(responseText) {
						$('#modal_atencion').html(responseText);
					});
				
				
				$("#modal_atencion").dialog({
	                width: 700,
	                height: 350,
	                show: "scale",
	                hide: "scale",
	                modal: "true"

	            });		
			}else
				alert("Seleccinar paciente");
			}
		});
}

function applyFilter(target, filter) {
    //var obj = canvas.getActiveObject();
   // target.filters[index] = filter;
	target.filters.push(filter);
    target.applyFilters(canvas.renderAll.bind(canvas));
  }


function dibujarEnElCanvas(ctx) {
	// Creo una imagen conun objeto Image de Javascript
	var img = new Image();
	// indico la URL de la imagen
	img.src = 'img/ODO2.png';
	img.id = "odo";
	// defino el evento onload del objeto imagen
	img.onload = function() {
		// incluyo la imagen en el canvas
		// ctx.drawImage(img,0,0,W,H);
		fabric.Image.fromURL(img.src, function(oImg) {
			canvas.add(oImg);
		});
	}

}

setTimeout(function() {
	inicializarCanvas();
	addEventListener("resize", inicializarCanvas);
}, 15);
