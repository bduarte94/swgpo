function simpleTablaAjax(datos, clase, metodo, resultado, carrera){
    var selectOrigen = document.getElementById(datos);// Obtengo el select que el usuario modifico
    var opcionSeleccionada=selectOrigen.options[selectOrigen.selectedIndex].value; // Obtengo la opcion que el usuario selecciono
    var valorCarrera=document.getElementById(carrera).value;
    divResultado = document.getElementById(resultado);
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'parametro': opcionSeleccionada,// <-- the $ sign in the parameter name seems unusual, I would avoid it
            'carrera':valorCarrera
        },
        dataType: "html",
        success: function(responseText){
            divResultado.innerHTML = responseText;
        }
    });
}

function agregarTipoCatalogos(clase,metodo,id_estado,id_input,tipo,carrera, resultado, id_multiple,idValidarEnMatch){
    var valorInput=document.getElementById(id_input).value;
    var valorEstado=$("input[name='"+id_estado+"']:checked").val();
    var valorMultiple = $("#"+id_multiple).val();
    var valorCarrera =document.getElementById(carrera).value;
    var valorValidarEnMatch =$("input[name='"+idValidarEnMatch+"']:checked").val();
   divResultado = document.getElementById(resultado);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'tipoCatalogo':valorInput,
            'tipo':tipo,
            'estado':valorEstado,
            'multiple': valorMultiple,
            'carrera':valorCarrera,
            'validarEnMatch': valorValidarEnMatch
        },
        dataType: "html",
        success: function(responseText){
            divResultado.innerHTML += responseText;
        }
    });
}

function editarTipoCatalogos(clase,metodo,id_estado,id_input,tipo,valueId, carrera, resultError, multiple,idvalidarEnMatch){
    var valorInput=document.getElementById(id_input).value;
    var valorEstado=$('input:radio[name='+id_estado+']:checked').val();
    var valorId =document.getElementById(valueId).value;
    var valorCarrera =document.getElementById(carrera).value;
    var valorMultiple =document.getElementById(multiple).value;
    var valorValidarEnMatch =$('input:radio[name='+idvalidarEnMatch+']:checked').val();
    divResultado = document.getElementById(tipo+valorId);
    divResultado2 = document.getElementById(resultError);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'tipoCatalogo':valorInput,
            'tipo':tipo,
            'estado':valorEstado,
            'id':valorId,
            'carrera':valorCarrera,
            'multiple': valorMultiple,
            'validarEnMatch': valorValidarEnMatch
        },
        dataType: "html",
        success: function(responseText){
            if(responseText != "Error"){
            divResultado.innerHTML = responseText;
            }
            else
                {
                divResultado2.innerHTML = "<div id=\"bad_new_catalogo\" class=\"alert alert-error\">"+
                "<a class=\"close\" data-dismiss=\"alert\">&times;</a> <strong>Ya existe "+
                valorInput+"</strong></div>";
                }
        }
    });
}

function agregarCatalogos(clase,metodo,id_estado,id_input,tipo,idTipoBeneficio, resultado, n_detalle, niveles, carrera){
    var valorInput=document.getElementById(id_input).value;
    var valorEstado=$("input[name='"+id_estado+"']:checked").val();
    var valorIdetalle = $("input[name='"+ n_detalle+"']:checked").val();
    valorIdetalle=valorIdetalle=="true"?valorIdetalle:"false";
    
    var valorNiveles = $("input[name='"+ niveles+"']:checked").val();
    valorNiveles=valorNiveles=="true"?valorNiveles:"false";
   
    var selectOrigen = document.getElementById(idTipoBeneficio);// Obtengo el select que el usuario modifico
    var idTipoBenef=selectOrigen.options[selectOrigen.selectedIndex].value;
    var valorCarrera=document.getElementById(carrera).value;
    divResultado = document.getElementById(resultado);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'catalogo':valorInput,
            'tipo':tipo,
            'idTipoCatalogo':idTipoBenef,
            'estado':valorEstado,
            'idetalle':valorIdetalle,
            'niveles': valorNiveles,
            'carrera':valorCarrera
        },
        dataType: "html",
        success: function(responseText){
            divResultado.innerHTML += responseText;
        }
    });
}

function editarCatalogos(clase,metodo,id_estado,id_input,tipo,valueId, resultError, carrera, tipoRespuesta){
    var valorInput=document.getElementById(id_input).value;
    var valorEstado=$("input[name='"+id_estado+"']:checked").val();
    var valorId =document.getElementById(valueId).value;
    var valorCarrera=document.getElementById(carrera).value;
    var valortipoRespuesta = $("#"+tipoRespuesta+"").is(':checked')?true:false;
    divResultado = document.getElementById(tipo+valorId);
    divResultado2 = document.getElementById(resultError);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'catalogo':valorInput,
            'tipo':tipo,
            'estado':valorEstado,
            'id':valorId,
            'carrera':valorCarrera,
            'tipoRespuesta':valortipoRespuesta
        },
        dataType: "html",
        success: function(responseText){
            if(responseText != "Error"){
            divResultado.innerHTML = responseText;
            }
            else
                {
                divResultado2.innerHTML = "<div id=\"bad_new_catalogo\" class=\"alert alert-error\">"+
                "<a class=\"close\" data-dismiss=\"alert\">&times;</a> <strong>Ya existe "+
                valorInput+"</strong></div>";
                }
        }
    });
}

function validacionEmpresaAjax(idparam1, idparam2,idparam3, clase, metodo, resultado){
    var valorParametro = document.getElementById(idparam1).value;
    var valorParametro2 = document.getElementById(idparam2).value;
    var valorParametro3 = document.getElementById(idparam3).value;
    var divResultado = document.getElementById(resultado);
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'parametro': valorParametro,
            'parametro2': valorParametro2,
            'parametro3': valorParametro3,
            'parametro4': 'E'
           // <-- the $ sign in the parameter name seems unusual, I would avoid it
        },
        dataType: "html",
        beforeSend: function(){
            $('#load').show();
          },
       complete: function() {
               $('#load').hide();
          },     
        success: function(responseText){
            if(responseText == "true")
                {
                 $('#emailExito').hide(); 
                 $('#emailError').hide();
                 $('#emailExito').show('slow');
                 divResultado.innerHTML = "";
                }
            else if(responseText == "false")
                {
                $('#emailError').hide();
                $('#emailExito').hide(); 
                $('#emailError').show('slow');
                }
        }
    });
}

function CreacionUsuarioContactoAjax(idparam1, idparam2,idparam3, clase, metodo, resultado){
    var valorParametro = document.getElementById(idparam1).value;
    var valorParametro2 = document.getElementById(idparam2).value;
    var valorParametro3 = document.getElementById(idparam3).value;
    var divResultado = document.getElementById(resultado);
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'parametro': valorParametro,
            'parametro2': valorParametro2,
            'parametro3': valorParametro3,
            'parametro4': 'C'
           // <-- the $ sign in the parameter name seems unusual, I would avoid it
        },
        dataType: "html",
        beforeSend: function(){
            $('#load').show();
            $('#newUser').hide();
          },
       complete: function() {
               $('#load').hide();
          },     
        success: function(responseText){
            if(responseText == "true")
                {                 
                 $('#correoOk').show('slow');
                 $('#correoNok').hide();
                 divResultado.innerHTML = "";
                }
            else if(responseText == "false")
                {    
                $('#correoNok').show('slow');
                 $('#correoOk').hide();
                }
        }
    });
}

function validarUserDisponibleAjax(idparam1, clase, metodo){
    var valorParametro = document.getElementById(idparam1).value;
      $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'parametro': valorParametro,
         
        },
        dataType: "html",
        success: function(responseText){
            $('#msjUsername').hide();
            if(responseText == "true"){
                $('#msjUsername').html("<span style=\"color:green\"><i class=\"icon-ok-sign\"></i> Usuario disponible</span>");
                    }
            else if(responseText == "false"){
                $('#msjUsername').html("<span style=\"color:red\"><i class=\" icon-remove-sign\"></i> Usuario no disponible</span>");
                }
            $('#uResult').val(responseText);
            $('#msjUsername').show();
          }
    });
}

function mostrarDatosPlantilla(clase, metodo,valueId,modal,estadoPlantilla,nombrePlantilla,asuntoPlantilla,cuerpoPlantilla,results_plantilla, uso,otrosDatos, otrosDatos2){
    //'estadoPlantilla','nombrePlantilla','asuntoPlantilla','cuerpoPlantilla','results_plantilla',uso (//indicara si se usara desde admin plantilla o ya sobreescribiendo los comodines)
    //otrosDatos: se ocupara un parametro con nombre generico para reutilizarlo con otro input en futuras implementaciones
    var resultArray = new Array();
    divResultado = document.getElementById("idPlantilla"+valueId);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'id':valueId,
            'uso':uso,
            'otrosDatos':otrosDatos,
            'otrosDatos2':otrosDatos2
        },
        dataType: "html",
        success: function(responseText){
            resultArray=responseText.split("@#");
            if(responseText != "Error"){
                $('#'+nombrePlantilla).val(resultArray[0]);
                if(estadoPlantilla!='')if( resultArray[1]=='true')$('input:radio[name='+estadoPlantilla+']')[0].checked = true;
                else $('input:radio[name='+estadoPlantilla+']')[1].checked = true;
                $('#'+asuntoPlantilla).val(resultArray[2]);
                $('#'+cuerpoPlantilla).val(resultArray[3]).blur();
                if(modal!='')$('#'+modal).modal('show');
                if(results_plantilla!='')$('#'+results_plantilla).show();
            }
            else
            {alert("Error al mostrar la plantilla seleccionada");}
        }
    });
}

function agregarPlantilla(clase,metodo,id_estado,nombrePlantilla,asuntoPlantilla, cuerpoPlantilla, resultado,modal){
    var valorNombrePlantilla=document.getElementById(nombrePlantilla).value;
    var valorAsuntoPlantilla=document.getElementById(asuntoPlantilla).value;
    var valorCuerpoPlantilla=document.getElementById(cuerpoPlantilla).value;
    var valorEstado=$("input[name='"+id_estado+"']:checked").val();
    var divResultado = document.getElementById(resultado);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'nombrePlantilla':valorNombrePlantilla,
            'estado':valorEstado,
            'asuntoPlantilla':valorAsuntoPlantilla,
            'cuerpoPlantilla':valorCuerpoPlantilla
        },
        dataType: "html",
        success: function(responseText){
            $('#myModal').modal('hide');
            $('#nombrePlantilla').val('');
            $('#asuntoPlantilla').val('');
            $('#cuerpoPlantilla').val('').blur();
            divResultado.innerHTML += responseText;
        }
    });
}

function editarPlantilla(clase,metodo,idPlantillaParam, id_estado,nombrePlantilla,asuntoPlantilla, cuerpoPlantilla, resultadoError,modal){
    var idPlantilla=document.getElementById(idPlantillaParam).value;
    var valorNombrePlantilla=document.getElementById(nombrePlantilla).value;
    var valorAsuntoPlantilla=document.getElementById(asuntoPlantilla).value;
    var valorCuerpoPlantilla=document.getElementById(cuerpoPlantilla).value;
    var valorEstado=$("input[name='"+id_estado+"']:checked").val();
    var divResultado = document.getElementById('idPlantilla'+idPlantilla);
    divResultado2 = document.getElementById(resultadoError);
    $.ajax({
        type:'GET',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'idPlantilla':idPlantilla,
            'nombrePlantilla':valorNombrePlantilla,
            'estado':valorEstado,
            'asuntoPlantilla':valorAsuntoPlantilla,
            'cuerpoPlantilla':valorCuerpoPlantilla
        },
        dataType: "html",
        success: function(responseText){

            if(responseText != "Error"){
                $('#myModal').modal('hide');
                $('#nombrePlantilla').val('');
                $('#asuntoPlantilla').val('');
                $('#cuerpoPlantilla').val('').blur();
                divResultado.innerHTML = responseText;
            }
            else
                {
                $('#myModal').modal('hide');
                $('#nombrePlantilla').val('');
                $('#asuntoPlantilla').val('');
                $('#cuerpoPlantilla').val('').blur();
                divResultado2.innerHTML = "<div id=\"bad_Plantilla\" class=\"alert alert-error\">"+
                "<a class=\"close\" data-dismiss=\"alert\">&times;</a> <strong>Ya existe la plantilla "+
                valorNombrePlantilla+"</strong></div>";
                }
        }
    });
}

function rechazarSolicitudEmpresaAjax(idparam1, idparam2, clase, metodo, resultado){
    var valorParametro = document.getElementById(idparam1).value;
    var valorParametro2 = document.getElementById(idparam2).value;
    var divResultado = document.getElementById(resultado);
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'parametro': valorParametro,
            'parametro2': valorParametro2,
        },
        dataType: "html",
        beforeSend: function(){
            $('#load').html('Eliminando');
            $('#load').show();
          },
       complete: function() {
               $('#load').hide();
          },     
        success: function(responseText){
            if(responseText == "true")
                {
                 $('#rechazadoExito').hide(); 
                 $('#rechazadoError').hide();
                 $('#rechazadoExito').show('slow');
                 divResultado.innerHTML = "";
                }
            else if(responseText == "false")
                {
                $('#rechazadoError').hide();
                $('#rechazadoExito').hide(); 
                $('#rechazadoError').show('slow');
                }
        }
    });
}

function listadoPasantiaAjax(clase, metodo, datos, resultado, carrera){
    var selectOrigen = document.getElementById(datos);// Obtengo el select que el usuario modifico
    var opcionSeleccionada=selectOrigen.options[selectOrigen.selectedIndex].value; // Obtengo la opcion que el usuario selecciono
    var valorCarrera=document.getElementById(carrera).value;  
    divResultado = document.getElementById(resultado);
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'estado': opcionSeleccionada,// <-- the $ sign in the parameter name seems unusual, I would avoid it
            'carrera':valorCarrera
            
        },
        dataType: "html",
        success: function(responseText){
            divResultado.innerHTML = responseText;
        }
    });
}

function enviarPlantillaXeMail(idPlantilla, idDestinatario, emailDestinatario, cc, asunto, cuerpo, clase, metodo, resultado,opt){
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'idDestinatario': idDestinatario,
            'idPlantilla': idPlantilla,
            'emailDestinatario': emailDestinatario,
            'cc':cc,
            'asunto': asunto,
            'cuerpo': cuerpo,
            'opt':opt
        },
        dataType: "html",
        beforeSend: function(){
            $('#load').show();
          },
       complete: function() {
               $('#load').hide();
          },     
        success: function(responseText){
            if(responseText == "true")
                {
                 $('#emailExito').hide(); 
                 $('#emailError').hide();
                 $('#results_plantilla').hide();
                 $('#emailExito').show('slow');
                }
            else if(responseText == "false")
                {
                $('#emailError').hide();
                $('#emailExito').hide(); 
                $('#emailError').show('slow');
                }
        }
    });
}

function modificarAtributosPasantia(clase,metodo,atributo, idSolicitud, idOptante,resultado,idRegistro,newSpan,divResultado, fecha){
    var textresultado = document.getElementById(resultado);
    var inhabilitar="inhabilitarCandidatoText"+idRegistro;
    var spanNuevo=newSpan+idRegistro;
    $.ajax({
        type:'post',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'idOptante':idOptante,
            'idPasantia':idSolicitud,
            'atributo':atributo,
            'valor':true,
            'fecha':fecha
            
        },
        dataType: "html",
        beforeSend: function(){
            $('#load').show();
          },
        success: function(responseText){
            if(responseText == "citadoTrue")
            {
                $('#load').hide();
                document.getElementById(inhabilitar).innerHTML = "";
                textresultado.innerHTML = "";
                document.getElementById(spanNuevo).innerHTML = "<button type=\"button\" id=\"seleccionarOptante"+idRegistro+"\" class=\"btn btn-success btn-small\""+
                "onclick=\"$('#modalSeleccionar').modal('show');$('#idOptanteSeleccion').val('"+idOptante+"');$('#idSolicitudSeleccion').val('"+idSolicitud+"'); $('#idOptanteHasPasantia').val('"+idRegistro+"'); \">"+
                // " onclick=\"modificarAtributosPasantia('ajax.PasantiaAJAX','seleccionarOptanteParaPasantia','seleccionado',"+idSolicitud+","+idOptante+",'seleccionararCandidatoText"+idRegistro+"',"+idRegistro+",'');\""+
                       // " title=\"Seleccionar optante para ocupar la pasantia\"> "+
                        " <i class=\"icon-ok-circle icon-white\"></i> Seleccionar</button>";
                $('#Ok').html('<strong>&Eacute;xito!</strong>&nbsp; El optante ha sido Citado');
                $('#Ok').show('slow');
            }
            if(responseText == "seleccionadoTrue")
            {
                optanteSolicitadosVsSeleccionados('ajax.PasantiaAJAX','optanteSolicitadosVsSeleccionados',idSolicitud, divResultado);
                $('#load').hide();
                textresultado.innerHTML = "Optante seleccionado";
                $('#Ok').html('<strong>&Eacute;xito!</strong>&nbsp; El optante ha sido Seleccionado');
                $('#Ok').show('slow');
            }
        }
    });
}

function optanteSolicitadosVsSeleccionados(clase, metodo, idSolicitud, resultado){
    divResultado = document.getElementById(resultado);
    $.ajax({
        type:'POST',
        url: '/ajax',
        data:{
            'class': clase, 
            'method': metodo,
            'idPasantia':idSolicitud
        },
        dataType: "html",
        success: function(responseText){
            $('#tablaResult').html(responseText);
        }
    });
}

function verCoincidenciasOptanteYSolicitudPasante(clase, metodo,idOptante, idSolicitud,resultado){
    divResultado = document.getElementById(resultado);
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'idOptante': idOptante,
            'idSolicitud': idSolicitud,
        },
        dataType: "html",
        success: function(responseText){
            divResultado.innerHTML = responseText;
        }
    });
}
function reenviarCorreoConfirmacionRegistro(idOptante, nombreOptante, emailDestinatario, clase, metodo, resultado){
    $.ajax({
        type: 'GET',
        url: '/ajax',
        data: { 
            'class': clase, 
            'method': metodo,
            'idOptante': idOptante,
            'nombreOptante': nombreOptante,
            'emailDestinatario': emailDestinatario
        },
        dataType: "html",
        beforeSend: function(){
            $('#load').show();
          },
       complete: function() {
               $('#load').hide();
          },     
        success: function(responseText){
            if(responseText == "true")
                {
                 $('#emailExito').hide(); 
                 $('#emailError').hide();
                 $('#emailExito').show('slow');
                }
            else if(responseText == "false")
                {
                $('#emailError').hide();
                $('#emailExito').hide(); 
                $('#emailError').show('slow');
                }
        }
    });
}
