var Total=parseFloat(0.0);

var lenguaje = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    			},
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }};


$(document).ready(function() { //*****************READY******************************************************
	$.getScript("js/OdontoFunctions.js");
	
	var tableCre=$('#regtableCre').DataTable({
		responsive: true,
		language: lenguaje
	});
	
	var tableCot=$('#regtableCot').DataTable({
		responsive: true,
		language: lenguaje
	});
	
	var table= $('#regtableAt').DataTable({
		retrieve: true,
		searching:false,
		language: lenguaje
	});
	
	//seleccionar creditos
	$('#modal_Creditos').on('dblclick','.rowcre',function (event) { 
		$('#idCredito').val($(this).data("id"));
		$("#modal_Creditos").dialog("close");
		$('#OptCredito').prop("checked", "checked");
		$('#txtidCredito').val("CRE-"+$(this).data("id"));
		$('#txtLimite').val(getLimiteCredito($(this).data("id")));
		$('#txtSaldo').val(getSaldo($(this).data("id")));
	});
	//modal de creditos
	$('#OptCredito').on('click',function(event){
		var marcado = $("#OptCredito").prop("checked") ? true : false;
		
				//$(this).prop("checked", "");
				idPaciente=$('#pacientesCBX').val();
				if($('#pacientesCBX').val()!='0'){
					//getCreditos(idPaciente);
					if(marcado){
					$.get('ManejarPeticiones', {
						class : "Common.CreditosCOM",
						method : "getCreditos",
						idPaciente:  idPaciente
					}, function(responseText) {
						$('#registroCre').hide('normal');
						//$('#registroCre').html(responseText);
						tableCre.rows().remove().draw();
					 	tableCre.rows.add($(responseText)).draw();
						$('#registroCre').show('normal');
						//return responseText;
					});
					
					$("#modal_Creditos").dialog({
		                width: 800,
		                height: 400,
		                show: "scale",
		                hide: "scale",
		                modal: "true"
		
		            });	
					$(this).prop("checked", "");
					}else{
						$('#idCredito').val(-1);
						$('#txtidCredito').val("Ningun crédito asignado");
						$('#txtLimite').val("Sin límite");
						$('#txtSaldo').val("Sin saldo");
					}
		}else{
			$(this).prop("checked", "");
			$('#idCredito').val(-1);
			$('#txtidCredito').val("Ningun crédito asignado");
			$('#txtLimite').val("Sin límite");
			$('#txtSaldo').val("Sin saldo");
			alert("Seleccionar paciente");
		}
	});
	
	//evento para registrar atencion
	$('#btnAt').on('click',function (event) { // function to process submitted table
		if($('#pacientesCBX').val()!='0'){
			if($('#descripcion').val()!=""){
					var method = "";
					if ($('#At_Active').val()==-1){ 
						method = "saveAtencion";
					}else{
						method = "UpdateAtencion";
					}
					var tableData = []; // we will store rows' data into this array
					var i=0;
			        $("#regtableAt") // select table by id
			                .find(".tableRow") // select rows by class
			                //.has(":checked") // select only rows with checked checkboxes
			                .each(function () { // for each selected row extract data
			                    var tableRow = {};
			                    var jRow = $(this);
			                    tableRow.idDiente = jRow.find('td.idDiente').text();
			                    tableRow.idServicio = jRow.find('input.idServicio').val();
			                    tableRow.idSitio = jRow.find('td.idSitio').text();
			                    tableRow.idEstado = jRow.find('td.idEstado').text(); //puedes cambiar td por input,checkbox, etc
			                    tableRow.precio = jRow.find('td.precio').text();
			                    tableRow.descuento = jRow.find('td.descuento').text();
			                    tableRow.total = jRow.find('td.total').text();
			                    tableRow.realizado = jRow.find('input.realizado').val();
			                    tableData.push(tableRow);
			                    i++;
			                });
			        		if(i>0){
			        			var guardar = false;
			        			if($('#idCredito').val()!=-1){ //valida si no se sobrepasa el limite de credito
				        			var limite = getLimiteCredito($('#idCredito').val());
				        			var saldo = getSaldo($('#idCredito').val());
				        			var resultado = parseFloat(Total)+parseFloat(saldo);
				        			if (resultado <= parseFloat(limite)){
				        				guardar=true;
				        			}
				        			else{
				        				guardar=false;
				        			}
			        			}else
			        				guardar = true
			        			
			        			if (guardar==true){
						        $.ajax({
						        	type:'POST',
							        url: 'ManejarPeticiones',
							        data:{
									'class' : 'Common.AtencionCOM',
									'method': method,
									idPaciente: $( "#pacientesCBX option:selected" ).val(),
									idOdontologo : $('#idOdontologo').val(),
									descripcion: $('#descripcion').val(),
									Total: Total,
									tableData: tableData,
									idAtencion: $('#At_Active').val(),
									idCredito: $('#idCredito').val()
							        },
							        success: function(data) {
							        	alert("Atencion Almecenada!");
							        	location.reload();
							        }
						        	
						        	
						        });
			        			}else{alert ("Sobrepasa límite de crédito");}
			        		} else alert ("No se ha registrado ningun servicio");
			        event.preventDefault(); //Prevent sending form by browser
			}else {
				alert("Ingrese Descripcion");
				$('#descripcion').focus();
			}
		
		}else {
				alert("Seleccione paciente");
				$('#pacientesCBX').focus();
				}
		
		}
	);//fin guardar atencion
	
	//evento para el boton de carga de atenciones
	$('#btnGeAt').on('click',function (event) { 
		if($('#pacientesCBX').val()!='0'){ //valida si se ha seleccionado paciente
			//$('#idDiente').val(e.target.get('id'));
				$.get('ManejarPeticiones', {
					class : "Common.AtencionCOM",
					method : "getAtenciones",
					idPaciente: $( "#pacientesCBX option:selected" ).val()
				}, function(responseText) {
					$('#modal_Cotizaciones').html(responseText);
				});
			
			
			$("#modal_Atenciones").dialog({
                width: 500,
                height: 200,
                show: "scale",
                hide: "scale",
                modal: "true"

            });		
		}else
			alert("Seleccinar paciente");
	});//fin carga de atenciones
	
	//limpiar pagina
	$('#btnNewDoc').on('click',function (event) { 
		//$("#AtencionDatos").load(location.href + " #AtencionDatos>*","");
		location.reload();
	});

	
	//evento para el boton de carga de cotizaciones
	$('#btnGetCot').on('click',function (event) { 
		if($('#pacientesCBX').val()!='0'){ //valida si se ha seleccionado paciente
			//$('#idDiente').val(e.target.get('id'));
				$.get('ManejarPeticiones', {
					class : "Common.AtencionCOM",
					method : "getCotizaciones",
					idPaciente: $( "#pacientesCBX option:selected" ).val()
				}, function(responseText) {
					tableCot.rows().remove().draw();
				 	tableCot.rows.add($(responseText)).draw();
				});
			
			
			$("#modal_Cotizaciones").dialog({
                width: 500,
                height: 200,
                show: "scale",
                hide: "scale",
                modal: "true"

            });		
		}else
			alert("Seleccinar paciente");
	});
	
		//*********evento para seleccion de cotizacion**
		$('#modal_Cotizaciones').on('dblclick','.rowcot',function (event) { 
			//alert ($(this).data("id"));
			$('#Cot_Active').val($(this).data("id")); //se le asigna el id cotizacion al input
			 $.ajax({
				  type:'GET',
			        url: 'ManejarPeticiones',
			        data:{
					'class' : 'Common.AtencionCOM',
					'method': 'AjaxAddRowCotizacion',
					idCotizacion:$(this).data("id"),
					dataType: 'json'
			        },
					success: function(data) {
						 var jsonObj = JSON.parse(data);
						 	$('#descripcion').val(jsonObj.descripcion);
							//$('#registroAt').html(jsonObj.filas);
						 	table.rows().remove().draw();
						 	table.rows.add($(jsonObj.filas)).draw();
							Total = parseFloat(jsonObj.total);
							$('#total').text("Total: "+Total);
						/*var sheet = document.createElement('style')
						sheet.innerHTML = ".outer td {max-width:10%;min-width:10%;text-align:left;}";
						document.body.appendChild(sheet);*/
						$("#modal_Cotizaciones").dialog("close");
						/*$.each(jsonObj, function(index, element) {
							$('#descripcion').val(element.descripcion);
							$('#registro').html(element.filas);
							Total = parseFloat(element.total);
							$('#total').text("Total: "+Total);
				        });*/
					
						$('td').css("width","10%");
						$('td').css("max-width","10%");
						$('td').css("min-width","10%");
						$('td').css("text-align","left");
					}
				});
			
		})
		
	
	//**********************************************************
	//evento para registrar cotizacion
	$('#btnCot').on('click',function (event) { // function to process submitted table
		if($('#pacientesCBX').val()!='0'){
			if($('#descripcion').val()!=""){
					var method = "";
					if ($('#Cot_Active').val()==-1){
						method = "saveCotizacion";
					}else{
						method = "UpdateCotizacion";
					}
					var tableData = []; // we will store rows' data into this array
					var i=0;
			        $("#regtableAt") // select table by id
			                .find(".tableRow") // select rows by class
			                //.has(":checked") // select only rows with checked checkboxes
			                .each(function () { // for each selected row extract data
			                    var tableRow = {};
			                    var jRow = $(this);
			                    tableRow.idDiente = jRow.find('td.idDiente').text();
			                    tableRow.idServicio = jRow.find('input.idServicio').val();
			                    tableRow.idSitio = jRow.find('td.idSitio').text();
			                    tableRow.idEstado = jRow.find('td.idEstado').text(); //puedes cambiar td por input,checkbox, etc
			                    tableRow.precio = jRow.find('td.precio').text();
			                    tableRow.descuento = jRow.find('td.descuento').text();
			                    tableRow.total = jRow.find('td.total').text();
			                    tableRow.realizado = jRow.find('input.realizado').val();
			                    tableData.push(tableRow);
			                    i++;
			                });
			        		if(i>0){
						        $.ajax({
						        	type:'POST',
							        url: 'ManejarPeticiones',
							        data:{
									'class' : 'Common.AtencionCOM',
									'method': method,
									idPaciente: $( "#pacientesCBX option:selected" ).val(),
									idOdontologo : $('#idOdontologo').val(),
									descripcion: $('#descripcion').val(),
									Total: Total,
									tableData: tableData,
									idCotizacion: $('#Cot_Active').val()
							        },
							        success: function(data) {
							        	alert("Cotizacion Almecenada!");
							        	location.reload();
							        }
						        	
						        	
						        });
			        		} else alert ("No se ha registrado ningun servicio");
			        event.preventDefault(); //Prevent sending form by browser
			}else {
				alert("Ingrese Descripcion");
				$('#descripcion').focus();
			}
		
		}else {
				alert("Seleccione paciente");
				$('#pacientesCBX').focus();
				}
		
		}
	);
	
	
	
	
	//SELECTORES(SERVICIOS) PARA EL MODAL QUE AGREGA ATENCIONES ALA TABLA
	//***********************************************************
	$('#regtableAt').tooltip();
	
	$('#modal_atencion').on('change','#TipoServicio', function() {
		$.get('ManejarPeticiones', {
			class : "Common.CatalogosCOM",
			method : "ListaSerPorTipo",
			idTipoServicio:  $(this).val(),
			idClinica: $('#idClinica').val()
		}, function(responseText) {
			$('#Servicio').html(responseText);
		});
	});
	
	$('#modal_atencion').on('change','#Servicio', function() {
			var idServicio=$(this).val();
			$('#modal_precio').val(getPrecioServicio(idServicio));
	});
	
	$('#modal_atencion').on('click','#button' ,function() {
		var descuento = $("#modal_descuento").val();
		if($('#Servicio').val()!=-1){
			if(descuento.match(/[-+]?([0-9]*\.[0-9]+|[0-9]+)/)){
				if(descuento!="" && parseFloat(descuento)>=0 && parseFloat(descuento)<=100){
					$("#modal_atencion").dialog('close');
					var idDiente=$('#idDiente').val();
					var idServicio=$('#Servicio').val();
					var sitio=$('input[name=Sitio]:checked').val();
					var estado=$('input[name=Estado]:checked').val();
					
					var desc = $('#modal_descuento').val();
					var precio=parseFloat($('#modal_precio').val());
					descuento= (parseFloat($('#modal_descuento').val())/100)*precio;
					
					//var PrecioDesc = descuento
					 
					 
					 
					  $('#atencion').remove();
					  
					  $.ajax({
						  type:'GET',
					        url: 'ManejarPeticiones',
					        data:{
							'class' : 'Common.AtencionCOM',
							'method': 'AjaxAddRow',
							idDiente:idDiente,
							idServicio:idServicio,
							sitio:sitio,
							estado:estado,
							desc:desc,
							total:parseFloat(precio)-parseFloat(descuento)
					        },
							success: function(data) {
								/*var sheet = document.createElement('style')
								sheet.innerHTML = ".outer td {max-width:10%;min-width:10%;text-align:left;}";
								document.body.appendChild(sheet);*/
								//$('#registroAt').append(data);
								table.row.add($(data)).draw();
								//alert(data);
								
								
								/*$('td').css("width","10%");
								$('td').css("max-width","10%");
								$('td').css("min-width","10%");
								$('td').css("text-align","left");*/
							}
						});
						//Imprime el total en los items 
					  	
					  	Total=parseFloat(Total)+(parseFloat(precio)-parseFloat(descuento));
						 $('#total').html("Total: "+Total);
						 //alert(precio+"   "+descuento);
					}else{
						alert("valores de descuento no válidos");
						$('#modal_descuento').focus();
				}
				}else{
					alert("Error en campo descuento");
					$('#modal_descuento').focus();
				}
		}else{
			alert("Selecciones Servicio");
			$('#Servicio').focus();
		}
		});
	//elimina una fila de la tabla
	$('#regtableAt tbody').on('click','.del_row' ,function() {
		var columnaTotal=0.0;
		//alert($(this).closest('tr').attr('#tdPrecio').text());
		var a = document.querySelectorAll('.del_row');
		for(var b in a){ //Como nos devuelve un array iteramos
		  var c = a[b];
		  if(typeof c == "object"){ //Solo buscamos los objetos
		     c.onclick = function (){ //Asignamos un evento onclick
		       var td = this.offsetParent; //Localizamos el TD
		       var tr = td.parentElement;  //LLegamos hasta el TR
		       var columna6 = tr.children[6]; // Buscamos la Columna NOMBRE
		       columnaTotal=parseFloat(columna6.textContent);
		       //alert("Precio: "+columna2.textContent);
		       //alert(parseFloat(columnaTotal));
				Total=parseFloat(Total)-parseFloat(columnaTotal);
				$('#total').html("Total: "+Total);
				$(this).closest('tr').remove();
		     }
		  }
		}
		
		
	});
	
	/*table.on( 'column-visibility.dt', function ( e, settings, column, state ) {
	        alert(column);
	  });*/
	
}) //cierra document ready
