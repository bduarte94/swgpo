
	$(document).ready(function() {
		//registra nuevo servicios
		$('#nuevo_servicio').on("click","#submit",function() {
			// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
			//e.preventDefault();
			//alert($( this ).text());
			$('#table').html("<div style='margin-left:40%'><img src=\"img/loading.gif\"/></div>");
			var page = $(this).attr('data');        
	        var dataString = 'page='+page;
			$.get('ManejarPeticiones', {
				servicio : $('#servicio').val(),
				precio : $('#precio').val(),
				descripcion : $('#descripcion').val(),
				tipoServicio : $('#tipoServicio').val(),
				estado: $('#estado').val(),
				moneda: $('#moneda').val(),
				class : $('#class').val(),
				method : $('#method').val(),
				idClinica : $('#idClinica').val()
			//apellido: apellidoVar,
			//edad: edadVar
			}, function(responseText) {
			    $('#servicio').val("");
			    $('#precio').val("");
			    $('#descripcion').val("");
			    $('#tipoServicio').val('0');
				$('#table').fadeIn(1000).html(responseText);
			});
		});
		//actualiza servicios
		$('#nuevo_servicio').on("click","#update",function() {
			// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
			//e.preventDefault();
			//alert($( this ).text());
			$('#table').html("<div style='margin-left:40%'><img src=\"img/loading.gif\"/></div>");
			var page = $(this).attr('data');        
	        var dataString = 'page='+page;
			$.get('ManejarPeticiones', {
				servicio : $('#servicio').val(),
				precio : $('#precio').val(),
				descripcion : $('#descripcion').val(),
				tipoServicio : $('#tipoServicio').val(),
				estado: $('#estado').val(),
				moneda: $('#moneda').val(),
				class : "Common.CatalogosCOM",
				method : "AjaxActualizarServicios",
				idServicio: $('#idServicio').val(),
				idClinica : $('#idClinica').val()
			//apellido: apellidoVar,
			//edad: edadVar
			}, function(responseText) {
				$('#table').fadeIn(1000).html(responseText);
			});
		});
		
		
		
		$('#new_ser').on('click',function(event){
			$.get('ManejarPeticiones',{
				class : "Common.CatalogosCOM",
				method : "MostrarRegServicios"
			}, function(responseText){
				$('#nuevo_servicio').html(responseText);
				$('#sernew').show('low');
			}
					
			)
		});
		
		$('#table').on('click','#update_ser',function(event){
			$.get('ManejarPeticiones',{
				class : "Common.CatalogosCOM",
				method : "MostrarUpServicios",
				idServicio: $(this).val()
			}, function(responseText){
				$('#nuevo_servicio').html(responseText);
				$('#serup').show('low');
			}
					
			)
		});
		
		
		
	});
	
    function foco(idElemento){
        document.getElementById(idElemento).focus();
       }
       