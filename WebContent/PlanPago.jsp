<%@ include file="include/cabecera.jspf" %>
<%@page import="Common.PacienteCOM"%>
<%@page import="Common.CreditosCOM"%>

<%
String idClinica ="";
String idOdontologo ="";
if(session.getAttribute("session")==null){
		response.sendRedirect("Index.jsp");
	//RequestDispatcher dd=request.getRequestDispatcher("Index.jsp");
	//dd.forward(request, response);
	}else
	{
		if(session.getAttribute("session").equals(false)){
			response.sendRedirect("Index.jsp");
		}else{
			if (session.getAttribute("idClinica") != null) {
		    	idClinica = session.getAttribute("idClinica").toString();
		    }else{
		    	//session.setAttribute("session", false);
		    	response.sendRedirect("Index.jsp");
		    }
		    
		   
		    if (session.getAttribute("idOdontologo") != null) {
		    	idOdontologo = session.getAttribute("idOdontologo").toString();
		    }
		}
	}

	
    /* if (request.getParameter("idClinica") == null) {
        if (session.getAttribute("idClinica") != null) {
            idClinica = session.getAttribute("idClinica").toString();
        }
    } else {
        idClinica = request.getParameter("idClinica").toString();
        session.setAttribute("idClinica", idClinica);
    } */
    
    
%>

<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/Bootstrap-select.css" rel="stylesheet">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/chosen.css">
<link rel="stylesheet" href="css/ImageSelect.css">
<script src="js/PlanPago.js"></script>

<style>
<!--
		#SelectPaciente{	
			max-width: 30%;
			min-width: 10%;
			margin: 0 1.5% 24px 1.5%;
		}
		header,
		section,
		aside {
		  margin: 0 1.2% 24px 0.3%;
		}
		section {
		  float: left;
		  width: 63%;
		  max-height:300px;
		  border:solid;
		  overflow-y: auto; 
		}
		aside {
		  float: right;
		  width: 35%;
		  overflow-x: auto;
		}

		.regtable{ width:100%; min-width:100%; height:100%; background-color: white;}
		.cabezera, .registro { display: block; }
.registro {
	width :100%;
	min-width:100%;
    height: 150px;       /* Just for the demo          */
    overflow-y: auto;    /* Trigger vertical scroll    */
    overflow-x: hidden;  /* Hide the horizontal scroll */
}	

.outer th, .outer td {
width : 10%;
max-width:10%;
min-width:10%; 
text-align:left;
}
.outer table caption {

width:100%;
text-align:center;
top:0;
left:0;
height:20px;
background:#2ECCFA;
border:1px solid blue;
color:black;
}
 .outer tfoot td{
max-width:19%;
text-align:right;
background:white; 
border:1px solid #000;
color:#000;
bottom:0;
left:0;
} 

.rowcre:hover{
	background-color: #D0FA58;
	cursor:pointer;
}
		
-->
</style>

<script type="text/javascript">

	
	$(document).ready(function() {
		//Evento del boton agregar nuevo credito
			$('#btnNewCred').on('click',function(event){
				if($('#pacientesCBX').val()!='0'){
					$('#modalNewcredito').dialog({
		                width: 500,
		                height: 200,
		                show: "scale",
		                hide: "scale",
		                modal: "true"
		
		          	});
				}else
					alert("Seleccionar Paciente");
		});
		/* $('#btnExitCred').on('click',function(event){
			$('#modalNewcredito').dialog({
                width: 500,
                height: 200,
                show: "scale",
                hide: "scale",
                modal: "true"

            });		
		}); */
		$("#btnRegCred").on('click',function(event){
			var idPaciente = $( "#pacientesCBX option:selected" ).val();
			$.ajax({
				  type:'GET',
			        url: 'ManejarPeticiones',
			        data:{
					'class' : 'Common.CreditosCOM',
					'method': 'AjaxSaveCredito',
					idPaciente: idPaciente,
					limite: $("#limite").val(),
					plazo: $("#plazo").val(),
					descripcion: $("#descripcion").val(),
					dataType: 'json'
			        },
					success: function(data) {
						   $('#view_creditos').html(data);
							$("#modalNewcredito").dialog("close");
						
					}
				});
			getCreditos(idPaciente);
			
		});

		
		
	});
	
	

</script>

</head>
<body style="position: absolute;">
	<%@ include file="include/menu.jspf"%>
	<br>
	<div class="container" style="position:relative; margin-top: 5%;margin-left: 5%;">
	
		 <div id="modalNewcredito" style="display: none; background-color: #A9E2F3;" class="modal" title="Nuevo cr�dito" >
					<!-- <div class="row" style="margin-left: 2%;margin-right: 2%;">	
						<div class="well well-sm">Nuevo cr�dito</div>
					</div> -->
					<div class="row" style="margin-left: 2%;">
						<div class="col-sm-3 col-md-4 col-lg-4">
							<input id="limite" name="limite" type="number" class="form-control" size="7" placeholder="Limite" required="required" pattern="[0-9]+(\\.[0-9][0-9]?)?">
						</div>
						<div class="col-sm-3 col-md-4 col-lg-4">
							<input id="plazo" name="plazo" type="number" class="form-control" size="4" placeholder="Plazo" value="0" required="required" pattern="^[0-9]+">
						</div>
					</div><hr>
					<div class="row" style="margin-left: 2%;">
						<div class="col-sm-6 col-md-8 col-lg-8">
							<textarea id="descripcion" rows="2" cols="45" class="form-control" placeholder="Ingrese Descripcion" required="required"></textarea>
						</div>
					</div><hr>
					<div class="row" style="margin-left: 2%;">
						<div class="col-sm-6 col-md-8 col-lg-8">
							<button id="btnRegCred" class="btn btn-success" title="Registrar cr�dito" >
								<i class="fa fa-pencil-square-o" aria-hidden="true">Agregar Cr�dito</i>
							</button>
						</div>
					</div>
					
			</div>
	
		<header>
			<div class="well well-sm"><h4>Plan de pagos</h4></div>
		</header>
		<div id="SelectPaciente">
			<select id="pacientesCBX" name="pacientesCBX" class="my-select" style="position:relative; margin-top: 100%;">
			  <option value='0' selected="selected">Seleccionar Paciente</option>
				<%
					out.write(new PacienteCOM().ListaPacienteCombobox(idClinica));
				%>
			</select>
		</div><hr>
		<section id="Section_creditos">					
			<div id="view_creditos">
				<div class="outer">
					<div class="innera">
						<table  id="regtableCre" class = "table table-bordered regtable">
							<caption>Creditos</caption>
							<thead id="cabezeraCre" class="cabezera">
								<!--<tr>
								
				                  <th colspan="5" style="text-align: center;">Detalle Atenci�n</th>
				            	</tr>-->
				            	<tr >
								<th >Id Cr�dito</th>
								<th >Descripcion</th>
								<th >Fecha</th>
								<th >Limite</th>
								<th >Plazo</th>
								<th >Estado</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
								
								<td id="total" colspan="1" style="text-align: right;">
									<button id="btnNewCred" class="btn btn-info" title="Agregar nuevo cr�dito">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</button>
								</td>
								</tr>
							</tfoot>
							<tbody id="registroCre" class="registro">
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</section>
		<aside class="Aside_Abonos">
			<!-- Modal registro de nuevo credito -->
			
		
		</aside>
	</div>
	
	<!-- Evento de seleccion de pacientes-->
	<script type="text/javascript" src="js/chosen.jquery.js"></script>
	<script type="text/javascript" src="js/ImageSelect.js"></script>
	<script type="text/javascript" src="js/event.simulate.js"></script>
	<script type="text/javascript">
	
		$(".my-select").chosen({
			width : "100%",
			heigth: "100%"
		});
		$(".my-select-border")
				.chosen(
					{
					width : "100%",
						html_template : '{text} <img style="border:3px solid #ff703d;padding:0px;margin-right:4px"  class="{class_name}" src="{url}" />'
						});
		$('.my-select').on('change',function(evt, params) {
			$('#infopaciente').html("<div style='margin-left:40%'><img src=\"img/loading.gif\"/></div>");
			//var page = $(this).attr('data');        
	        //var dataString = 'page='+page;
	        var idPaciente = $(this).val(); 
	        getCreditos(idPaciente);
			
		});
	</script>
	<%@ include file="include/footer2.jspf"%>
</body>
</html>