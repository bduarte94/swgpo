<%@ include file="include/cabecera.jspf"%>
<%@page import="Common.PacienteCOM"%>
<%String idOdontologo=session.getAttribute("idOdontologo").toString();
String idPaciente=request.getParameter("idPaciente").toString();%>
<script type="text/javascript" src="js/VistaPreviaImg.js"></script>

<%
out.write("<script type=\"text/javascript\">"+

"$(document).ready(function(){");
 
String reg = request.getParameter("reg");
if(reg!=null){
    if(reg.equals("ok"))
        out.write(" $('#ok').show('slow');});</script>");
      else if(reg.equals("nok"))
        out.write(" $('#not').show('slow','linear');});</script>");
}else
	 out.write("});</script>");
%>

</head>
<body>
<%@ include file="include/menu.jspf" %>
<br><br><br>
			<div id="ok" class="alert alert-info" style="display:none;">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>&Eacute;xito!</strong>&nbsp; Se ha registrado el paciente con &eacute;xito.
                    </div>
                 	<div id="not" class="alert alert-danger" style="display:none;">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>Error!</strong>&nbsp; Ocurrio un error al registrar el paciente
                    </div>	
	<div class="Container">
	<h2>Editar Foto De Paciente</h2>
		<div class="row">
		
		<div class="col-sm-3">
			<div class="panel panel-primary" align="Center">
				<div class="panel panel-heading">
					<h4>Fotografia Actual</h4>
				</div>
			</div>
			<div class="panel panel-body">
				<%out.write(new PacienteCOM().obtenImagenPaciente(idPaciente));%>
			</div>
		</div>
		
		
		<form id="formRF" class="form" action="ActualizarFoto" method="post" enctype="multipart/form-data">
		<div class="col-sm-3">
	 		<div class="panel panel-primary" align="Center">
	 		<input type="hidden" id="idodontologo" name="idOdontologo" value="<%=idOdontologo%>" />
			<input type="hidden" id="idPaciente" name="idPaciente" value="<%=idPaciente%>" />
		
	 			<div class="panel panel-heading">
	 			<h4>Foto De Paciente</h4>
	 			</div>
	 			<div class="panel panel-body">
	 			<div class="row">
	 			<img id="vistaPrevia" class="img-responsive" alt="" src="" width="128px" height="128px">
				</div>
				</div>
				<div id='botonera' class="panel panel-footer">
				<span id="infoNombre" hidden="true">[Seleccione una imagen]</span><br/>
            	<span id="infoTamaņo" hidden="true"></span>
	 			<input id="archivo" type="file" name="foto" class="file" placeholder="Subir foto" accept="image/*">
				</div>
			</div><br>
				<div class="panel panel-footer">
	 					<a class="btn btn-large btn-default" type="submit" value="cancelar" name="cancelar" href="javascript:history.go(-1)">
                  		 <span class="glyphicon glyphicon-ban-circle"></span> Cancelar
                   		</a>
	 					<button class="btn btn-large btn-primary" type="submit" value="save" name="save">
                  		 <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                   		</button>
	 				</div>
									
			</div>
			</form>
	</div>
	</div>
</body>