<%@ include file="include/cabecera.jspf"%>

<%@page import="utils.StringUtils"%>
<%@page import="Common.PacienteCOM"%>


<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

String idClinica="";
if(session.getAttribute("idClinica")!=null)
	idClinica=session.getAttribute("idClinica").toString(); 

String varSession = request.getParameter("login");
if((varSession !=null && varSession.equals("true")) &&
	    (session.getAttribute("busqueda") != null))
	session.setAttribute("busqueda", null);

%>

<style>
<!--


#content{
	padding-top: 25px;
    padding-right: 50px;
    padding-bottom: 25px;
    padding-left: 50px;
}

.img {
  display: block;
  max-width:50px;
  max-height:75px;
  width: auto;
  height: auto;
}
#busq{
	width: 1024px;
	height: 300px;
	
	overflow-y: scroll;
	overflow:auto;
	float: left;
    margin: 5px;
    padding: 15px;   
}
#result{
	background: #fff;
}

#controles{
	aling: left;
	
}

-->
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('#submit').click(function(event) {
			// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
			$.get('ManejarPeticiones', {
				busqueda : $('#busqueda').val(),
				class : $('#class').val(),
				method : $('#method').val(),
				idClinica: $('#idClinica').val()
				//apellido: apellidoVar,
				//edad: edadVar
			}, function(responseText) {
				$('#busq').html(responseText);
			});
		});
		
		$('#busqueda').keypress(function(event) {
			if (event.which == 13) {
			    $('#form').submit();
			    return false;    //<---- Add this line
			 }else{
			// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
			$.get('ManejarPeticiones', {
				busqueda : $('#busqueda').val(),
				class : $('#class').val(),
				method : $('#method').val(),
				idClinica: $('#idClinica').val()
				//apellido: apellidoVar,
				//edad: edadVar
			}, function(responseText) {
				$('#busq').html(responseText);
			});
			 }
		});
		
	});
</script>


</head>
<body onload="$('#busqueda').focus();">

<%@ include file="include/menu.jspf"%> <br><br><br><br><br>

<div class="span6">
	<Form id="form" class="navbar-form" action="ManejarPeticiones" method="get">
	<div class="col-lg-9">
		<h4><span class="glyphicon glyphicon-search"></span>B�squeda De Pacientes</h4>
	</div>
	<div class="col-lg-9">
	<div >
		<div class="input-group">
		<input id="busqueda" type="text" class="form-control" placeholder="Buscar" name="busqueda" value="">
		<span class="input-group-btn">
		<button id="submit" type="button" class="input-group-addon"><span class="glyphicon glyphicon-search"></span></button>
		</span>
		</div>
	</div>
	<input type="hidden" id="class" name="class" value="Common.PacienteCOM"/>
	<input type="hidden" id="idClinica" name="idClinica" value="<%=((!StringUtils.isBlank(idClinica)) ? idClinica : " ")%>">
    <input type="hidden" id="method" name="method" value="buscarPaciente"/>
	
	</div>
	</Form>
</div>
<div class="row" id="busq"></div>

</body>
 <%@ include file="include/footer2.jspf"%>
</html>