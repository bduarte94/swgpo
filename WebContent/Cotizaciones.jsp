<%@ include file="include/cabecera.jspf" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Common.PacienteCOM"%>
<%@page import="Common.CatalogosCOM"%>
<%@page import="Common.AtencionCOM"%>

<%
	String idOdontologo = "";
	String idClinica = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
%>

<body>

    <%@ include file="include/menu.jspf" %>
    
    <div class = "container">
    	<div id="cotizaciones">
    	 <% out.write(new AtencionCOM().getCotizaciones(idClinica))%>
    	</div>
    </div>
        
  	<%@ include file="include/footer2.jspf"%>
</body>
    	
</html>