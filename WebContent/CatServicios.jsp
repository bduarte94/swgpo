<%@page import="com.sun.beans.util.Cache"%>
<%@page import="Common.CatalogosCOM"%>
<%@page import="Common.EstadosCOM"%>
<%@ include file="include/cabecera_2.jspf"%>
<%@page import="java.util.Locale"%>
<%@page import="utils.StringUtils"%>

<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}
	String idClinica = "";
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
%>
<!-- Implementacion de AJAX -->
<script src="js/CatServicios.js"></script>


<!-- <link href="css/catalogos.css" rel="stylesheet"> -->
</head>
<body>
	<%@ include file="include/menu.jspf"%>
	<div class="container-fluid">
	 <div class="centerview">
		<div id="tittle" class="well well-sm tittle">
			<h4  class="pull-left" >
				Cat�logo de servicios
			</h4>
			
			<div class="pull-right">
			<button id="new_ser" type="button" title="Nuevo Servicio" class="btn btn-success" >
					<span class="glyphicon glyphicon-certificate"></span>
			</button>
			</div>
		</div>

			<div class="Span">
				<div class="row">
					<div class="col-md-6">
						<div id="content" class="panel panel-primary">
							<div class="panel panel-heading">Lista de Servicios</div>
							<div id="table">
								<%
									out.write(new CatalogosCOM().ServiciosMostrar(idClinica));
								%>
							</div>
						</div>
					</div>
					<div class="col-md-6">

						<form id="form_new">
							<input type="hidden" id="class" name="class"
								value="Common.CatalogosCOM" /> <input type="hidden"
								id="idClinica" name="idClinica"
								value="<%=((!StringUtils.isBlank(idClinica)) ? idClinica : " ")%>">
							<input type="hidden" id="method" name="method"
								value="AjaxRegistrarServicios" />

							<div id="nuevo_servicio"></div>


						</form>
					</div>
				</div>
			</div>


		</div>
		<%@ include file="include/footer.jspf"%>
	</div>
</body>
