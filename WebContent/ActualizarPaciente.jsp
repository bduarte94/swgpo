<%@ include file="include/cabecera.jspf"%>
<%@page import="Common.PacienteCOM"%>
<%@page import="java.io.OutputStream"%>

<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

	String idOdontologo = "";
	String idClinica = "";
	String idPaciente = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();

idPaciente=request.getParameter("idPaciente").toString();%>



<%
out.write("<script type=\"text/javascript\">"+

"$(document).ready(function(){"+

    "$(\"#myTab a\").click(function(e){"+

        "e.preventDefault();"+

        "$(this).tab('show'); });");
 
String reg = request.getParameter("reg");
if(reg!=null){
    if(reg.equals("ok"))
        out.write(" $('#ok').show('slow','linear');});</script>");
      else if(reg.equals("nok"))
        out.write(" $('#not').show('slow','linear');});</script>");
}else
	 out.write("});</script>");
%>
	<!--  <script type="text/javascript" src="js/VistaPreviaImg.js"></script> -->
<script type="text/javascript" src="js/ImgVistaPrevia2.js"></script>
<link href="css/registrar_pac.css" rel="stylesheet">
</head>
<body>
<%@ include file="include/menu.jspf" %>
	<br><br>
	<div id="contenido" class="container" align="Center">
		<br><br>	
					<div id="ok" class="alert alert-info" style="display:none;">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>&Eacute;xito!</strong>&nbsp; Se han Actualizado los datos del paciente con &eacute;xito.
                    </div>
                 	<div id="not" class="alert alert-warning" style="display:none;">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>Error!</strong>&nbsp; Ocurrio un error al actualizar el paciente
                    </div>		
                    
<div class="page-header" align="left">
    	<h3 class="text text-primary">Información del paciente</h3>
</div>

		<form id="formRP" class="form"  action="ManejarPeticionesPacientesActualizar" method="post" enctype="multipart/form-data">
				<input type="hidden" id="class" name="class" value="Common.PacienteCOM"/>
             	<input type="hidden" id="method" name="method" value="ActualizarPaciente"/> 
				<input type="hidden" id="idodontologo" name="idOdontologo" value="<%=idOdontologo%>" />
				<input type="hidden" id="idPaciente" name="idPaciente" value="<%=idPaciente%>" />
				
				
	 	<% 
	 		out.write(new PacienteCOM().DetallesPaciente(idClinica,idPaciente));
	 	// byte[] buffer = new PacienteCOM().obtenImagenPaciente(idPaciente);
	   		//out.write(new PacienteCOM().obtenImagenPaciente(idPaciente));
	   		//response.setContentType("image/jpg");
	   		//response.getOutputStream().write(buffer);
	   		//response.getOutputStream().flush();
	 	%>	 				
	 				
				
	 	<div class="row">
	 		<div class="panel panel-footer">
	 					<a class="btn btn-large btn-default" type="submit" value="cancelar" name="cancelar" href="javascript:history.go(-1)">
                  		 <span class="glyphicon glyphicon-ban-circle"></span> Cancelar
                   		</a>
	 					<button class="btn btn-large btn-primary" type="submit" value="save" name="save">
                  		 <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                   		</button>
	 				</div>
	 	</div>
		</form>
	</div>
<%@ include file="include/footer.jspf" %>
		
	<script src="js/Moment.js"></script>
	<script src="js/bootstrap-datetimepicker.js"></script>
</body>