<%@ include file="include/cabecera.jspf" %>

<%@page import="Common.PacienteCOM"%>
<%@page import="Common.OdontogramaCOM"%>
<%@page import="Common.CatalogosCOM"%>
<%@page import="Dao.OdontogramaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<script src="js/fabric/fabric.js"></script>
<script src="js/fabric/fabric.min.js"></script>
<script src="js/fabric/fabric.require.js"></script>

<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/Bootstrap-select.css" rel="stylesheet">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui.min.js"></script>
<!-- <script src="js/odontograma.js"></script>
<script src="js/OdontoFunctions.js"></script>
<script src="js/PlanPago.js"></script> -->

  <link rel="stylesheet" href="css/chosen.css">
 <link rel="stylesheet" href="css/ImageSelect.css"> 

<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}
	String idOdontologo = "";
	String idClinica = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
%>

<style>
html {
  position: relative;
  min-height: 100%;
}
body {
  Margin bottom by footer height
  margin-bottom: 60px;
  padding-top: 40px;
  overflow-y:auto;  
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  Set the fixed height of the footer here
  height: 60px;
  background-color: #f5f5f5;
}
#infopaciente{background-color: #A9E2F3;}
</style>

<script type="text/javascript">
var X, Y, W, H, r;
function inicializarCanvas() {
	// var canvas = document.querySelector("#canvas");
	var canvas = new fabric.Canvas('canvas',{ selection: false, preserveObjectStacking:true });
	var selectedObject;
	var source;
	var img = new Image();
	// indico la URL de la imagen
	img.src = 'img/ODO.png';
	img.id = "odo";
	// defino el evento onload del objeto imagen
	img.onload = function() {
		// incluyo la imagen en el canvas
		// ctx.drawImage(img,0,0,W,H);
		/* fabric.Image.fromURL(img.src, function(oImg) {
			// oImg.scale(0.5).setFlipX(true);
			oImg.set({
				id:0,
				left : 0,
				top : 0,
				// width: canvas.getWidth(),
				// height: canvas.getHeight(),
				perPixelTargetFind : true,
				targetFindTolerance : 4,
				hasControls : false,
				hasBorders : false,
				selectable : false,
				hoverCursor : 'pointer'
			})
			canvas.add(oImg);
		}); */
		
		fabric.Image.fromURL(img.src, function(o) {
			canvas.setBackgroundImage(o, canvas.renderAll.bind(canvas), {
		    top: 0,
		    left: 0
		  });
		});
		
		<% out.write(new OdontogramaCOM().getOdontograma()); %>
		
		/* canvas.on('object:selected', function(e) {
			e.target.setFill('green');
			canvas.renderAll();
		} */
		//)
	}
	
	
	canvas.on('mouse:over', function(e) { 
	   //e.target.setFill('red');
	   // canvas.renderAll();
		//alert('im here');
		
		if(e.target){
		    if(e.target.get('id')!=0 ){
		var filter = new fabric.Image.filters.Tint({
			  color: '#00BFFF',
			  opacity: 0.3
			});
		//console.log(e.target.get('id'));
		 e.target.item(0).filters.push(filter);
		e.target.item(0).applyFilters(canvas.renderAll.bind(canvas)); 
			//applyFilter(e.target,filter);
		    }
		}
	  });
	  canvas.on('mouse:out', function(e) {
		  if(e.target){
		      if(e.target.get('id')!=0 ){
		      //alert(e.target);
			  	e.target.item(0).filters = [];
				//e.target.item(0).filters.push(filter);
				e.target.item(0).applyFilters(function() { e.target.canvas.renderAll(); });
				//applyFilter(e.target,filter);
		      }
			}
	  });
	  
	 
		canvas.on('mouse:down', function(e){
			
			/*var pointer = canvas.getPointer(e.e);
		    var posx = pointer.x;
		    var posy = pointer.y;
		    console.log(posx+","+posy);*/
		    var objeto=e.target.item(0);
			if(e.target){
			   if(e.target.get('id')!=0 ){
				if($('#pacientesCBX').val()!='0'){ //valida si se ha seleccionado paciente
				//$('#idDiente').val(e.target.get('id'));
				    
					$.get('ManejarPeticiones', {
						class : "Common.OdontogramaCOM",
						method : "getEstadosDientes"
					}, function(responseText) {
						$('#dialog').html(responseText);
				});
				
				$('#dialog').dialog({
				    title: 'Seleccionar estado',
					width: 630,heigth: 310,
				    buttons: {
				      "OK": function () {
					  //if(e.target.get('propiedad')==1){
								
						    fabric.Image.fromURL($('input[name="optradio"]:checked').data("src"), function(img)
						    {         
						     img.set({ left: e.target.get('left'), top: e.target.get('top')});
						      
							     if(e.target.item(1)){
								   
		            					e.target.getObjects().forEach(function(object) {
		            						if(fabric.util.getKlass(copiedObjects[0].type).async) {
							            		objeto = object;
		            						}
		            						//canvas.remove(object);
							            });
// 		             					e.target.removeWithUpdate(e.target.item(1));
// 		 						      	canvas.renderAll();
		            					canvas.removeWithUpdate(objeto);
		            					canvas.renderAll();
								}
					        
						     	e.target.addWithUpdate(img);
						      canvas.renderAll();
						    });
						//}
				        $(this).dialog("close"); 
				      },
				      "Cancelar": function () {
					  	//dialog=false;
				        $(this).dialog("close");
				      }
				    }
				  });
				
				
				//alert(e.target.get('propiedad'));
				
				
// 					if(e.target.get('propiedad')==1){
// 					    if (e.target.item(1) !=null)
// 							e.target.removeWithUpdate(e.target.item(1))
// 					    fabric.Image.fromURL('img/dientes/estados/1.png', function(img)
// 					    {                     
					        
// 					     img.set({ left: e.target.get('left'), top: e.target.get('top')});
// 					      e.target.addWithUpdate(img);
// 					      canvas.renderAll();
// 					    });
// 					}
				
// 				e.target.add(new fabric.Rect({
				    
// 				    left: 0,
// 				    top: 0,
// 				    originX: 'center',
// 				    originY: 'center'
// 				  }));
				//canvas.renderAll().bind(canvas);
				
				/* fabric.Image.fromURL('img/dientes/estados/1.png', function(img) {
				    e.target.addWithUpdate(img);
				    e.target.setCoords();
				    canvas.setActiveObject(e.target);
				    canvas.renderAll();
				  }); */
				/* $("#modal_atencion").dialog({
	                width: 700,
	                height: 350,
	                show: "scale",
	                hide: "scale",
	                modal: "true"
	            });		 */
				
			}else
				alert("Seleccinar paciente");
			}
			}
		});
		
		
		// to json
		//var btnEl = document.getElementById('btnSaveOdonto');
		 //     canvas.getElement().parentNode.appendChild(btnEl);
		      /* btnEl.onclick = function() {
		        alert(JSON.stringify(canvas));
		      }; */
		      
		     $('#btnSaveOdonto').on('click',function(){
			 	//alert(JSON.stringify(canvas));
			 	 if($('#pacientesCBX').val()!='0'){
			 	   if($('#descripcion').val()!=""){
				 	$.post('ManejarPeticiones', {
						class : "Common.OdontogramaCOM",
						method : "saveOdonto",
						json: JSON.stringify(canvas.toJSON(["id","hasControls", "hasBorders","selectable","hoverCursor","renderOnAddRemove","propiedad"])),
						descripcion: $('#descripcion').val(),
						idPaciente: $('#pacientesCBX').val()
					}, function(responseText) {
						alert("odontograma almacenado");
				});
			 	   }else alert("Ingrese descripción")	
			 	}else alert("Seleccione paciente");
			     
		     });
		      
		      //eventos selector de pacientes
		     $(".my-select").chosen({
				width : "100%",
				heigth: "100%"
			});
			$(".my-select-border")
					.chosen(
						{
						width : "100%",
							html_template : '{text} <img style="border:3px solid #ff703d;padding:0px;margin-right:4px"  class="{class_name}" src="{url}" />'
							});
			$('.my-select').on('change',function(evt, params) {
				$('#infopaciente').html("<div style='margin-left:40%'><img src=\"img/loading.gif\"/></div>");
				//var page = $(this).attr('data');        
		        //var dataString = 'page='+page;
				$.get('ManejarPeticiones', {
					class : "Common.PacienteCOM",
					method : "ajaxinfopaciente",
					idPaciente:  $(this).val()
				}, function(responseText) {
					$('#infopaciente').fadeIn(1000).html(responseText);
				});
				
				$.get('ManejarPeticiones', {
					class : "Common.OdontogramaCOM",
					method : "getOdontogramaJSON",
					idPaciente:  $(this).val()
				}, function(responseText) {
				    if(responseText!=""){
					    canvas.clear();
					    canvas.loadFromJSON(responseText, canvas.renderAll.bind(canvas));
				    }else{
						inicializarCanvas();
						addEventListener("resize", inicializarCanvas);
				    }
				  });
				
			});
		
}
function applyFilter(target, filter) {
    //var obj = canvas.getActiveObject();
   // target.filters[index] = filter;
	target.filters.push(filter);
    target.applyFilters(canvas.renderAll.bind(canvas));
  }
function dibujarEnElCanvas(ctx) {
	// Creo una imagen conun objeto Image de Javascript
	var img = new Image();
	// indico la URL de la imagen
	img.src = 'img/ODO2.png';
	img.id = "odo";
	// defino el evento onload del objeto imagen
	img.onload = function() {
		// incluyo la imagen en el canvas
		// ctx.drawImage(img,0,0,W,H);
		fabric.Image.fromURL(img.src, function(oImg) {
			canvas.add(oImg);
		});
	}
}
setTimeout(function() {
	inicializarCanvas();
	addEventListener("resize", inicializarCanvas);
}, 15);
</script>


</head>
<body>
<%@ include file="include/menu.jspf" %>
<div class="container">
		<div id="dialog"></div>
	<div class="page-header">
    	<h3 class="text text-primary">Odontograma del paciente </h3>
	</div>
	<div class="col-lg-6">
		<canvas  width="450px" height="709px" id="canvas" >Su navegador no soporta canvas :( </canvas>
	</div>
	<div class="col-lg-6">
		<div class="row" style="margin-right: 50%;">
				<div id="SelectPaciente">
					<select id="pacientesCBX" name="pacientesCBX" class="my-select">
					  <option value='0' selected="selected">Seleccionar Paciente</option>
						<%
							out.write(new PacienteCOM().ListaPacienteCombobox(idClinica));
						%>
					</select>
				</div>
				
		</div>
		<div class="row">
			<div id="infopaciente">
			</div>
		</div>
		<div class="row">
			<div id="desc" >
				<label>Descripcion:</label>
				<textarea  class ="form-control" id="descripcion" name="descripcion" rows="2" cols="12"></textarea>
			</div>
		</div>
		<div class="row" id="btnSaveOdonto">
			<button class="btn btn-success">Guardar</button>
		</div>
	</div>
	
</div>

	<script type="text/javascript" src="js/chosen.jquery.js"></script>
	<script type="text/javascript" src="js/ImageSelect.js"></script>
	<script type="text/javascript" src="js/event.simulate.js"></script>
	<script type="text/javascript">
	
		
	</script>

<%-- <%@ include file="include/footer2.jspf" %> --%>
</body>
</html>

