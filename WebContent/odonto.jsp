<%@ include file="include/cabecera.jspf" %>

<%@page import="Common.PacienteCOM"%>
<%@page import="Common.CatalogosCOM"%>
<%@page import="Dao.OdontogramaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<script src="js/fabric/fabric.js"></script>
<script src="js/fabric/fabric.min.js"></script>
<script src="js/fabric/fabric.require.js"></script>

<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/Bootstrap-select.css" rel="stylesheet">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/odontograma.js"></script>
<script src="js/OdontoFunctions.js"></script>
<script src="js/PlanPago.js"></script>

 <link rel="stylesheet" href="css/chosen.css">
  <link rel="stylesheet" href="css/ImageSelect.css">

<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

	String idOdontologo = "";
	String idClinica = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Atención Odontológica</title>
         
    <style>
		.diente {
			border:none;
						
		}
		.diente:hover{
			background-color: blue;
			-webkit-border-radius: 5px 10px;  /* Safari  */
  			-moz-border-radius: 100px 105px;     /* Firefox */
		}
		
		 #div{ 
		margin-top: 55px;
		}
		#canvas{border:1px solid #d9d9d9; width: 100%; height: 100%;}
		
		#AtencionDatos1{
			margin-top: 55px;
			width:100%;
			max-width: 100%;
			min-width: 10%;
			min-height: 25%;
		}
		 
		#SelectPaciente{	
			max-width: 80%;
			min-width: 50%;
		}
		.w3-border{border:1px solid #ccc!important}
		.w3-padding{padding:8px 16px!important}
		.w3-margin-top{margin-top:16px!important}
		.size{widht:100%; height: 100px; max-height: 100px; overflow: auto;}
		
		#pic{margin-left:0; padding-left: 0px;}
		#cont{margin-top: -60px; margin-left:65px;}
		
		#infopaciente{background-color: #A9E2F3;}
		
		.outer{ width:100%; min-width:100%; height:100%; }
		.outer table {max-width : 100%; width: 100%;}
		
		#desc{  width: 100%;
				min-width: 100%;
				max-width: 100%;
			}
		#desc textarea{
			width: 100%;
		}

 /* .cabezera, .registro { display: block; }
.registro {
	width :100%;
	min-width:100%;
    height: 300px;       /* Just for the demo          */
   /* overflow-y: auto;    /* Trigger vertical scroll    */
   /* overflow-x: hidden;  /* Hide the horizontal scroll */
/*}	 */
/*
.outer th, .outer td {
width : 10%;
max-width:10%;
min-width:10%; 
text-align:left;
}
.outer table caption {

width:100%;
text-align:center;
top:0;
left:0;
height:20px;
background:#2ECCFA;
border:1px solid blue;
color:black;
}
 .outer tfoot td{
max-width:19%;
text-align:right;
background:white; 
border:1px solid #000;
color:#000;
bottom:0;
left:0;
}  */
.cabezera{
	background-color: #A9E2F3;
}

.rowcot:hover{
	background-color: #D0FA58;
	cursor:pointer;
}
.rowcre:hover{
	background-color: #D0FA58;
	cursor:pointer;
}

	
 </style>
 
    
 </head>
<body>

        <%@ include file="include/menu.jspf" %>
        
<div class="container" >
	<div class="row">   
            <div id="div" class="col-sm-12 col-md-6 col-lg-5">
				<canvas  width="450px" height="709px" id="canvas" >Su navegador no soporta canvas :( </canvas>
			</div>
			<!-- validar luego para clinica -->
			<input type="hidden" id="idOdontologo" name="idOdontologo" value="<%=idOdontologo%>"/>
       		<input type="hidden" id="idClinica" name="idClinica" value="<%=idClinica%>"/>
       		<input type="hidden" id="Cot_Active" name="Cot_Active" value=-1>
       		<input type="hidden" id="At_Active" name="At_Active" value=-1>
       		<input type="hidden" id="idCredito" name="idCredito" value=-1>
    <div class="col-sm-12 col-md-6 col-lg-7">    
	<div id="AtencionDatos1">
		<!-- Modal de registro de servicios para cada pieza dental -->
		<div id="modal_atencion" style="display:none;"></div>
		<!-- Modal de registro de seleccion de cotizaciones-->
		<div id="modal_Cotizaciones" style="display:none;" title="Cargar Cotizaciones">
			<table id="regtableCot" class = "table table-bordered regtable" >
							<thead class="cabezera">
				            	<tr >
								<th >id Cotización</th>
								<th >Descripcion</th>
								<th >Fecha</th>
								<th >Valor</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
								
								<td colspan="4" style="text-align: right;">
									Seleccionar con doble clic el crédito del paciente
								</td>
								</tr>
							</tfoot>
							<tbody id="registroCot" class="registro">
							</tbody>
			</table>
						
		</div>
		<!-- Modal de registro de seleccion de Atenciones-->
		<div id="modal_Atenciones" style="display:none;" title="Cargar Cotizaciones"></div>
		
		<div id="modal_Creditos" style="display:none;">
			<div class="outer">
						<table id="regtableCre" class = "table table-bordered regtable" >
							<caption>Creditos</caption>
							<thead class="cabezera">
				            	<tr >
								<th >Id Crédito</th>
								<th >Descripcion</th>
								<th >Fecha</th>
								<th >Limite</th>
								<th >Plazo</th>
								<th >Estado</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
								
								<td colspan="6" style="text-align: right;">
									Seleccionar con doble clic el crédito del paciente
								</td>
								</tr>
							</tfoot>
							<tbody id="registroCre" class="registro">
							</tbody>
						</table>
						
				</div>
		</div>
		
		<div class="row">
			<div id="SelectPaciente">
				<select id="pacientesCBX" name="pacientesCBX" class="my-select">
				  <option value='0' selected="selected">Seleccionar Paciente</option>
					<%
						out.write(new PacienteCOM().ListaPacienteCombobox(idClinica));
					%>
				</select>
			</div>
		</div>
		<br>
		<div class="row">
			<div id="infopaciente">
			</div>
		</div>
		<hr>
		<div class="row"> <!-- botones de acceso rapido -->
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<button id="btnNewDoc" type="button" class="btn btn-default" title="Nuevo Documento"><span class="glyphicon glyphicon-new-window"></span></button>
				<button id="btnGetCot" type="button" class="btn btn-info" title="Cargar cotización"><span class="glyphicon glyphicon-folder-open"></span></button>
				<button id="btnGetAt" type="button" class="btn btn-info" title="Cargar Atención"><span class="fa fa-stethoscope"></span></button>
	  		</div>	
		</div>
		<hr>
		<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		  			<label><input type="checkbox" id="OptCredito" name="OptCredito" title="Asignar atencion a un crédito"> Crédito <input class ="form-control" id="txtidCredito" value="Ningun crédito asignado" type="text" readonly="readonly" style="margin-right: 10%;">
					</label>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<label>Limite</label>
					<input class ="form-control" id="txtLimite" value="sin limite" type="text" readonly="readonly">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<label>Saldo</label>
					<input class ="form-control" id="txtSaldo" value="sin saldo" type="text" readonly="readonly" >
	  			</div>
		</div>
		<hr>
		<div class="row">
			<div id="desc" >
				<label>Descripcion:</label>
				<textarea  class ="form-control" id="descripcion" name="descripcion" rows="2" cols="12"></textarea>
			</div>
		</div>
		<hr>
		<div class="row">
			<div style="width: 100%; overflow-x:auto; background-color: #E1F3FA;">
					<table  id="regtableAt" class = "table table-bordered">
						<caption>Detalle Atención</caption>
						<thead class="cabezera">
							<!--<tr>
							
			                  <th colspan="5" style="text-align: center;">Detalle Atención</th>
			            	</tr>-->
			            	<tr >
							<th >Id Diente <i class="fa fa-fw fa-sort"></i></th>
							<th >Servicio  <i class="fa fa-fw fa-sort"></i></th>
							<th >Sitio <i class="fa fa-fw fa-sort"></i></th>
							<th >Estado <i class="fa fa-fw fa-sort"></i></th>
							<th >Precio <i class="fa fa-fw fa-sort"></i></th>
							<th >Descuento % <i class="fa fa-fw fa-sort"></i></th>
							<th >Total <i class="fa fa-fw fa-sort"></i></th>
							<th >Realizado <i class="fa fa-fw fa-sort"></i></th>
							<th >Acción</th>
							</tr>
						</thead>
						<tbody id="registroAt" class="registro">
						</tbody>
						<tfoot>
							<tr>
							
							<td id="total" colspan="9" style="text-align: right;">Total: 0.00</td>
							</tr>
						</tfoot>
					</table>
			</div>
		</div>
		
		<div class="row">
		 <button id="btnCot" type="button" class="btn btn-info" title="Guardar como cotización"><span class="glyphicon glyphicon-floppy-disk">Cotización</span></button>
		 <button id="btnAt" type="button" class="btn btn-success" title="Guardar en el historial del paciente"><span class="glyphicon glyphicon-floppy-disk">Guardar</span></button>
		</div>
	</div>
	</div>
	</div>
</div>

	<script type="text/javascript" src="js/chosen.jquery.js"></script>
	<script type="text/javascript" src="js/ImageSelect.js"></script>
	<script type="text/javascript" src="js/event.simulate.js"></script>
	<script type="text/javascript">
	
 		$(".my-select").chosen({
			width : "100%",
			heigth: "100%"
		});
 		$(".my-select-border")
				.chosen(
 				{
 					width : "100%",
 						html_template : '{text} <img style="border:3px solid #ff703d;padding:0px;margin-right:4px"  class="{class_name}" src="{url}" />'
 						});
 		$('.my-select').on('change',function(evt, params) {
			$('#infopaciente').html("<div style='margin-left:40%'><img src=\"img/loading.gif\"/></div>");
 			//var page = $(this).attr('data');        
	        //var dataString = 'page='+page;
			$.get('ManejarPeticiones', {
				class : "Common.PacienteCOM",
				method : "ajaxinfopaciente",
 				idPaciente:  $(this).val()
 			}, function(responseText) {
				$('#infopaciente').fadeIn(1000).html(responseText);
 			});
		});
	</script>
		<%@ include file="include/footer.jspf"%>
    </body>
    	
</html>
