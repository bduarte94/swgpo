<%@include file="include/cabecera.jspf"%>
<link href="css/registro.css" rel="stylesheet">

<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

	String idOdontologo = "";
	String idClinica = "";
	String rol = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
	if (session.getAttribute("tipoUser") != null)
		rol = session.getAttribute("tipoUser").toString();

%>

<% 
String log;
if(request.getParameter("ok")!=null){
		log=request.getParameter("ok").toString();
		if(log.compareTo("0")==0)
			out.write("<script type=\"text/javascript\">"+
		"$(function(){"+
			"$('#noreg').show('slow');"+
			
					"})</script>");
		else
			out.write("<script type=\"text/javascript\">"+
					"$(function(){"+
						"$('#sireg').toggle('slow');"+
								"})</script>");
}			
%>

<script type="text/javascript">
	$(document).ready(function()
		{
		$("#boton").click(function () {	 
			$('#dialog').toggle("slow");
			});
		$("#boton1").click(function () {	 
			$('#target').toggle("fast");
			});
		$("#boton2").click(function () {	 
			$('#target').toggle(5000);
			});
		
		
		$("#form").submit(function( event ) {
		    event.preventDefault();
		});
		
		$("#submit").click(function( event ) {
		    dataString = $("#form").serialize();    
		
		    $.ajax({
		          type: 'POST',
		          url: 'ManejarPeticiones',
		          dataType: "json",
		          data:dataString,
		          success: function(resp) {
		              if(!resp.existe){
				        	  if(resp.ok==1){
				        	  	$('#sireg').toggle('slow');
				        	  	return false;
				              }else{
				        	  	$('#noreg').show('slow');
				        	  	return false;
				              }
		   				}else{
						    $('#user_nok').toggle('slow');
						    $('#txtUsr').focus();
						    return false;
						}
		              
		             
		              
		          } 
		         
		    });
		    return false;
		  });
		
		
		 });
	
	/* function validar_usuario(){
	    var result;
	    $.ajax({
	          dataType: 'json',
	          type: 'POST',
	          url: 'ManejarPeticiones',
	          'method': 'usuarioExiste',
				'class': 'UsuarioCOM',
				'login':$('#txtUsr').val(),
	          success: function(resp) {
	              if(responseText==='0'){
	        	  		$('form').submit();
	                  	return true;
					}else{
					    $('#user_nok').toggle('slow');
					    $('#txtUsr').focus();
					    return false;
					}
	          }
	    });
	} */
</script>

</head>


<body>
	<header>
	<%@include file="include/menu.jspf"%>
	</header>
	<div class="container">
	
							<div id="noreg" class="mensaje alert alert-danger" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Error!</strong>&nbsp; No se ha creado el usuario
                    		</div>
                 			<div id="sireg" class="mensaje alert alert-warning" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Aviso!</strong>&nbsp; Se ha enviado un correo al usuario con las credenciales de acceso
                    		</div>
                    		<div id="user_nok" class="mensaje alert alert-warning" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Aviso!</strong>&nbsp;El usuario ya existe
                    		</div>	
	
 		
		<form role="form" id="form" action="ManejarPeticiones" method="get" >
				<input type="hidden" name="idClinica" value="<%=idClinica%>">
				<input type="hidden" name="method" value="registrarOdontologo">
				<input type="hidden" name="class" value="Common.OdontologoCOM">
				
				
				<div id="form1" class="panel panel-primary" style="margin-top: 60px;">
					<div class="panel panel-heading">
						<h2>Nuevo Usuario</h2>
					</div>
					<div class="panel panel-body">
						<div class="Container-wrapper">
                 			 <div class="row">
                   				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Nombre:</div>
                  				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input id="nom" type="text" class="form-control" name="txtnom" placeholder="Ingrese Nombre" required="required"></div>
                    			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">2do Nombre:</div>
                  				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input id="nom2" type="text" class="form-control" name="txtnom2" placeholder="Ingrese Nombre"></div>
                    	  	</div>
                        	<div class="row">
                        		<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Apellido:</div>
                  				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input id="apel" type="text" class="form-control" name="txtapel" placeholder="Ingrese Apellido" required="required"></div>                 
                        		<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">2do Apellido:</div>
                  				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input id="apel2" type="text" class="form-control" name="txtapel2" placeholder="Ingrese Apellido"></div>
                        	</div>
                        	<div class="row">
                   				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Sexo:</div>
           		   				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
	           		   				<select class="form-control" name="sexo">
	 								 <option>Femenino</option>
	 								 <option>Masculino</option>
									</select>
								</div> 
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Tel�fono:</div>
                  				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input id="tel" type="number" class="form-control" name="txtTel" placeholder="Ingrese Numero"></div>
                        	
							</div>
                        	 <div class="row">
                        	 	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Direcci�n:</div>
           		   				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input id="dir" type="text" class="form-control" name="txtdir" placeholder="Ingrese direcci�n"> </div>
                       		 
                   				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Email:</div>
           		   				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input type="email" class="form-control" name="txtemail" placeholder="Ingrese Correo Electronico"> </div>
                       		 </div><hr>
                        	 <div class="row">
                   				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Usuario:</div>
           		   				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input type="text" class="form-control" id="txtUsr" name="txtUsr" placeholder="Ingrese Usuario" required="required"> </div>
                       
                   				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Contrase�a:</div>
           		   				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><input type="password" class="form-control" name="txtPass" placeholder="Ingrese Contrase�a" required="required"> </div>
                       		 
                       		 </div><br>
                       		  
                       		 
                        	 
					</div>
					<div class="panel-footer">
						<button class="btn btn-large btn-primary" id="submit" type="submit" value="Login" name="Login">
                  		 <span class="glyphicon glyphicon-log-in"></span> Registrar Usuario
                   		</button>
					</div>
				
				</div>
			</div>
			</form>
	</div>
	
	<%@include file="include/footer.jspf" %>
</body>