<%-- 
    Document   : editarOdonto
    Created on : 24-jul-2015, 17:06:43
    Author     : Jimmy
--%>

<%@page import="Entidades.Atencion"%>
<%@page import="Entidades.Padecimiento"%>
<%@page import="Entidades.EstadoDiente"%>
<%@page import="Entidades.Servicio"%>
<%@page import="Entidades.Tratamiento"%>
<%@page import="Entidades.Diente"%>
<%@page import="Dao.OdontogramaDAO"%>
<%@page import="java.util.LinkedList"%>
<%@page import="Entidades.Paciente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
    <%@ include file="include/cabecera.jspf" %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
             <%@ include file="include/menu.jspf" %>
       
        
        <div class="panel-primary">
            <div class="page-header" align="center">
                <br><br>
                <%
                String codigo = request.getParameter("seleccion");
                %>
                <h3 class="text text-primary">Editar los datos del odontograma: <%=codigo%> </h3> 
            </div>

            <img src="img/ODO.png" style="position: absolute;top: 140px;left: 240px" id="11"/>

            <%--sector izquierda--%>
            <img src="img/3.png" style="position: absolute;top: 421px;left: 292px;opacity: 0.7" id="18" />
            <img src="img/1.png" style="position: absolute;top: 375px;left: 299px;opacity: 0.7" id="17"/>
            <img src="img/2.png" style="position: absolute;top: 330px;left: 310px;opacity: 0.7" id="16"/>
            <img src="img/4.png" style="position: absolute;top: 291px;left: 333px;opacity: 0.7" id="15"/>
            <img src="img/5.png" style="position: absolute;top: 256px;left: 341px;opacity: 0.7" id="14"/>
            <img src="img/6.png" style="position: absolute;top: 220px;left: 348px" id="13"/>
            <img src="img/7.png" style="position: absolute;top: 206px;left: 382px" id="12"/>
            <img src="img/8.png" style="position: absolute;top: 210px;left: 420px" id="11"/>

            <%--sector derecho--%>
            <img src="img/33.png" style="position: absolute;top: 419px;left: 573px;opacity: 0.7" id="28" />
            <img src="img/11.png" style="position: absolute;top: 374px;left: 566px;opacity: 0.7" id="27"/>
            <img src="img/33.png" style="position: absolute;top: 328px;left: 556px;opacity: 0.7" id="26"/>
            <img src="img/44.png" style="position: absolute;top: 291px;left: 551px;opacity: 0.7" id="25"/>
            <img src="img/44.png" style="position: absolute;top: 258px;left: 539px" id="24"/>
            <img src="img/66.png" style="position: absolute;top: 220px;left: 530px" id="23"/>
            <img src="img/77.png" style="position: absolute;top: 210px;left: 497px" id="22"/>
            <img src="img/88.png" style="position: absolute;top: 211px;left: 459px" id="21"/>

            <%--sector izquierda-abajo --%>
            <img src="img/33.png" style="position: absolute;top: 525px;left: 293px;opacity: 0.7" id="48" />
            <img src="img/47.png" style="position: absolute;top: 574px;left: 309px;opacity: 0.7" id="47"/>
            <img src="img/46.png" style="position: absolute;top: 618px;left: 318px;opacity: 0.7" id="46" />
            <img src="img/444.png" style="position: absolute;top: 664px;left: 335px;opacity: 0.7" id="45" />
            <img src="img/777.png" style="position: absolute;top: 697px;left: 350px;opacity: 0.7" id="44" />
            <img src="img/555.png" style="position: absolute;top: 726px;left: 367px;opacity: 0.7" id="43" />
            <img src="img/43.png" style="position: absolute;top: 736px;left: 400px;opacity: 0.7" id="42" />
            <img src="img/43.png" style="position: absolute;top: 740px;left: 429px;opacity: 0.7" id="41" />

            <%--sector derecho-abajo --%>
            <img src="img/333.png" style="position: absolute;top: 523px;left: 565px;opacity: 0.7" id="38" />
            <img src="img/37.png" style="position: absolute;top: 571px;left: 558px;opacity: 0.7" id="37"/>
            <img src="img/36.png" style="position: absolute;top: 615px;left: 551px;opacity: 0.7" id="36" />
            <img src="img/35.png" style="position: absolute;top: 660px;left: 543px;opacity: 0.7" id="35" />
            <img src="img/34.png" style="position: absolute;top: 694px;left: 532px;opacity: 0.7" id="34" />
            <img src="img/332.png" style="position: absolute;top: 724px;left: 510px;opacity: 0.7" id="33" />
            <img src="img/43.png" style="position: absolute;top: 736px;left: 486px;opacity: 0.7" id="32" />
            <img src="img/43.png" style="position: absolute;top: 740px;left: 458px;opacity: 0.7" id="31" />

        </div>
            
            <form name="formulario" id="formulario" method="get" class="form" action="EdicionOdontograma.jsp" style="position:absolute;left: 1000px;top: 100px">

            <h2 class="text text-primary">Datos a editar</h2>
           
            <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Odontograma
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idPaciente"name="txtCodigo">
                        <option value="<%=codigo%>"><%=codigo%></option>
                    </select>
                    </div>

            </div>
            
            
            <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Nombre del Paciente
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idPaciente"name="selecPaciente">
                        


                        <%
                            LinkedList<Paciente> pac = OdontogramaDAO.getidPaciente();
                            LinkedList<Paciente> pacnombbre = OdontogramaDAO.getNombrePaciente();
                            for (int i = 0; i < pac.size(); i++) {
                                out.println("<option value=" + pac.get(i).getidPacienten()+">" + pacnombbre.get(i).getNombre1() + " " + pacnombbre.get(i).getApellido1() +"</option>");

                            }
                        %>

                    </select>



                </div>

            </div>


            <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Id del Diente
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="iddiente"name="selecDiente">


                        <%
                            LinkedList<Diente> diente = OdontogramaDAO.getidDiente();
                            for (int i = 0; i < diente.size(); i++) {
                                out.println("<option>" + diente.get(i).getIdDiente() + "</option>");

                            }
                        %>

                    </select>



                </div>

            </div>

            <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Tratamiento
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idTratamiento"name="selecTratamiento">


                        <%
                            LinkedList<Tratamiento> trata = OdontogramaDAO.getTratamiento();
                            LinkedList<Tratamiento> trataDesc = OdontogramaDAO.getDescripcionTratamiento();
                            for (int i = 0; i < trata.size(); i++) {
                                out.println("<option value=" + trata.get(i).getIdtratamiento() + ">" + trataDesc.get(i).getDescripcion()+ "</option>");

                            }
                        %>

                    </select>



                </div>

            </div>
                        
                <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Servicio
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idServicio" name="selecServicio">


                        <%
                            LinkedList<Servicio> ser = OdontogramaDAO.getServicio();
                            LinkedList<Servicio> serNombre = OdontogramaDAO.getNombreServicio();
                            for (int i = 0; i < ser.size(); i++) {
                                out.println("<option value=" + ser.get(i).getIdservicio() + ">" + serNombre.get(i).getNombre()+ "</option>");

                            }
                        %>

                    </select>



                </div>

            </div>
                        
                        <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Atencion
                    </h4>
                </div>
                        <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idAtencion"name="selectAtencion">


                        <%
                            LinkedList<Atencion> atencion = OdontogramaDAO.getAtencion();

                            for (int i = 0; i < atencion.size(); i++) {
                                out.println("<option value =" + atencion.get(i).getidAtencion()+ ">" + atencion.get(i).getidAtencion()+ "</option>");

                            }
                        %>

                    </select>



                </div>
                        
                        </div>

              <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Estado Diente
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idEstadoDiente"name="selectEstadoDiente">


                        <%
                            LinkedList<EstadoDiente> eDiente = OdontogramaDAO.getEstadoDiente();
                            LinkedList<EstadoDiente> eNombreDiente = OdontogramaDAO.getNombreEstadoDiente();
                            for (int i = 0; i < eDiente.size(); i++) {
                                out.println("<option value=" + eDiente.get(i).getIdEstadoDiente() +">" + eNombreDiente.get(i).getDescripcion()+ "</option>");

                            }
                        %>

                    </select>



                </div>

            </div>
                        
             <div class="row">  

                <div class="navbar-inner" style="padding: 5px;">
                    <h4 id="h4" style="color: #317EAC;">
                        Padecimiento
                    </h4>
                </div>
                <div class="col-xs-offset-4 col-centered center-block">
                    <select class="form-control" id="idPadecimiento"name="selectPadecimiento">


                        <%
                            LinkedList<Padecimiento> padeciemiento = OdontogramaDAO.getPadecimiento();
                            LinkedList<Padecimiento> padeciemientoDesc = OdontogramaDAO.getDescrPadecimiento();
                            for (int i = 0; i < padeciemiento.size(); i++) {
                                out.println("<option value =" + padeciemiento.get(i).getPadecimiento() + ">" + padeciemientoDesc.get(i).getDescripcion()+ "</option>");

                            }
                        %>

                    </select>



                </div>

            </div>
                        
                         <div class="row">  
   
                             <div class="col-xs-offset-4 col-centered center-block">
                                 <br>
                                 <button class="btn btn-large btn-primary center-block" type="submit" value="Guardar" name="guardar">
                  		 <span class="glyphicon glyphicon-floppy-disk"></span> Editar
                   		</button>
                             </div>
                             
                         </div>
                        
                        
        </form>
    </body>
</html>
