<%@ include file="include/cabecera.jspf"%>

<%@page import="Common.PacienteCOM"%>
<%@page import="Common.OdontogramaCOM"%>
<%@page import="Common.CatalogosCOM"%>
<%@page import="Dao.OdontogramaDAO"%>
        
<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

	String idOdontologo = "";
	String idClinica = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
%>
</head>
    <body>
         <%@ include file="include/menu.jspf" %>
        <div id="contenido" class="container" align="Center">
            
        <h3 class="text text-primary" style="margin-top:60px; text-align: left;">Reportes del sistema</h3>
        
        
        <nav class="navbar navbar-default">
			  <div class="container-fluid">
			     <ul class="nav navbar-nav">
			      <li class="active"><a href="ReportePacientes.jsp" title="Pacientes">Pacientes</a></li>
			      <li class="dropdown">
			      	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Odontologos
			      	<span class="caret"></span></a>
				      <ul class="dropdown-menu">
		                <li ><a href="ReporOdontologo.jsp" title="Odontologos">Reporte odontologo</a></li>
		                <li ><a href="ReporOdontologo.jsp" title="Servicios">Catalogos de Servicios</a></li>
	            	  </ul>
            	  </li>
			      <li><a href="ReporteOdontograma.jsp" title="Texto">Odontograma</a></li>
			      <li><a href="Reportes/RUsuarios.jsp" title="Texto">Usuarios</a></li>
            	<li><a href="ReporteAtencion.jsp" title="Texto">Citas</a></li>
			    </ul>
			  </div>
		</nav>
        
        <div style="">
        	<div class="row" style="margin-right: 50%;">
				<div id="SelectPaciente">
					<select id="pacientesCBX" name="pacientesCBX" class="my-select">
					  <option value='0' selected="selected">Seleccionar Paciente</option>
						<%
							out.write(new PacienteCOM().ListaPacienteCombobox(idClinica));
						%>
					</select>
				</div>
				
			</div>
			<div class="row" style="text-align: left;">
				<a href="#">Historial Medico</a>
			</div>
        </div>
        
        
        
         </div>
         
         <script type="text/javascript">
	
 		$(".my-select").chosen({
 			width : "100%",
 			heigth: "100%"
 		});
 		$(".my-select-border")
 				.chosen(
					{
					width : "100%",
 						html_template : '{text} <img style="border:3px solid #ff703d;padding:0px;margin-right:4px"  class="{class_name}" src="{url}" />'
 						});
		$('.my-select').on('change',function(evt, params) {
			$('#infopaciente').html("<div style='margin-left:40%'><img src=\"img/loading.gif\"/></div>");
			//var page = $(this).attr('data');        
 	        //var dataString = 'page='+page;
 			$.get('ManejarPeticiones', {
				class : "Common.PacienteCOM",
				method : "ajaxinfopaciente",
				idPaciente:  $(this).val()
			}, function(responseText) {
				$('#infopaciente').fadeIn(1000).html(responseText);
			});
 		});
	</script>
         
         
          <%@ include file="include/footer2.jspf"%>
       
    </body>
</html>
