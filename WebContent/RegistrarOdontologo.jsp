<%@page import="Dao.Email"%>
<%@ include file="include/cabecera.jspf"%>
<%@ include file="include/menu.jspf"%>


    
				<script type="text/javascript">
						function validarDatosPost(){
							 var verificar = true;
							 
							 if (!document.frmRegistroOdontologo.txtNom1.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtNom1.focus();
									verificar=false;
									
							 }
							 else if(!document.frmRegistroOdontologo.txtNom2.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtNom2.focus();
									verificar=false;
							 															 
						}
						
						else if(!document.frmRegistroOdontologo.txtApellido.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtApellido.focus();
									verificar=false;
							 }
							 
							 else if(!document.frmRegistroOdontologo.txtApellido2.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtApellido2.focus();
									verificar=false;
							 }
							 
							  else if(!document.frmRegistroOdontologo.txtDireccion.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtDireccion.focus();
									verificar=false;
							 }
							 
							 else if(!document.frmRegistroOdontologo.txtMovil.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtMovil.focus();
									verificar=false;
							 }
							 
							  else if(!document.frmRegistroOdontologo.txtCodigoMinsa.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtCodigoMinsa.focus();
									verificar=false;
							 }
							 
							 	  else if(!document.frmRegistroOdontologo.fecha.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.fecha.focus();
									verificar=false;
							 }
							 
							 else if(!document.frmRegistroOdontologo.txtlogin.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtlogin.focus();
									verificar=false;
							 }
							 
							  else if(!document.frmRegistroOdontologo.txtpass.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtpass.focus();
									verificar=false;
							 }
							 
							  else if(!document.frmRegistroOdontologo.txtcorreo.value){
									alert("Este campo es requerido");
									document.frmRegistroOdontologo.txtcorreo.focus();
									verificar=false;
							 }
							 
							 if(verificar){
									document.frmRegistroOdontologo.submit();
							 }
	 }
						
						window.onload=function(){
							 document.getElementById("btnEnviar").onclick=validarDatosPost;
						}
						
				</script>
				
	<style>
<!--
#content{
	padding: 35px 280px;
}

-->
</style>	

<% 
String log;
if(request.getParameter("ok")!=null){
		log=request.getParameter("ok").toString();
		if(log.compareTo("0")==0)
			out.write("<script type=\"text/javascript\">"+
		"$(function(){"+
			"$('#noreg').show('slow');"+
			
					"})</script>");
		else
			out.write("<script type=\"text/javascript\">"+
					"$(function(){"+
						"$('#sireg').toggle('slow');"+
								"})</script>");
}			
%>
	
    </head>
		
    <body>
<div id="content" class="Container" align="center">			 
	<div class="page-header" align="center">
		 <br>
    	<h1 class="text text-primary">Nuevo Registro</h1>
	</div>
    
    
    						<div id="noreg" class="alert alert-danger" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Error!</strong>&nbsp; No se ha creado el usuario
                    		</div>
                 			<div id="sireg" class="alert alert-warning" style="display:none;">
                        	<button class="close" data-dismiss="alert" type="button">&times;</button>
                        	<strong>Aviso!</strong>&nbsp; Se ha enviado un mensaje de confirmación a su correo
                    		</div>	
    
    	<div class="row" align="center">
			 <form id="frmRegistroOdontologo" name="frmRegistroOdontologo" class="form" action="AgregarOdontologo" 
						 method="post" enctype="application/x-www-form-urlencoded">
					
	 		<div class="span6">
				 	
			<div class="panel panel-primary" >
						 
						 <div class="panel panel-heading">
						 <h2>Ingrese Datos del Médico Odóntologo</h2>
						 </div>
					
			<div class="panel panel-body" align="left">				
					<div class="row">
	 		
						<div class="col-sm-2">
						<label>Nombre:</label>
						</div>
	 		
						<div class="col-sm-3">
						<input type="text"class="form-control" name="txtNom1" id="txtNom1" placeholder="Ingrese Nombre del Odóntologo" value="">
				</div>
	 		
						<div class="col-sm-1">
						<label>2do Nombre:</label>
						</div>
	 		
						<div class="col-sm-3">
						<input type="text"class="form-control" name="txtNom2" id="txtNom2"placeholder="Ingrese 2do Nombre Odóntologo">
						</div>
						
						<div class="col-sm-1">
						<label>Email:</label>
			 			</div>	
					
			<div class="col-sm-3">
				 <input type="email" class="form-control" name="txtcorreo" id="txtcorreo"placeholder="Ingrese la Dirección de Correo Electrónico">
			</div>
				
				 </div><br>
			
			<div class="row">
				 <div class="col-sm-2">
						<label>Apellido:</label>
				 </div>	
					
				 <div class="col-sm-3">
						<input type="text" class="form-control" name="txtApellido1" id="txtApellido" placeholder="Ingrese el Apellido del Odóntologo">
				 </div>
				 
				 <div class="col-sm-1">
				 	<label>2do Apellido:</label>
				 </div>	
					
				 <div class="col-sm-3">
						<input type="text" class="form-control" name="txtApellido2" id="txtApellido2" placeholder="Ingrese el 2 Apellido del Odóntologo">
				</div>
				 
				 
				 
			</div>
				 <br>
				 
				 
			<div class="row">
				
				<div class="col-sm-2">
				<label>Contraseña</label>
			 </div>	
					
			<div class="col-sm-3">
				 <input type="password" class="form-control" name="txtpass" id="txtpass" placeholder="Ingrese la contraseña para el usuario">
			</div>		 
				 
			
			<div class="row">
			<div class="col-sm-1">
				<label>Usuario</label>
			 </div>	
					
			<div class="col-sm-3">
				 <input type="text" class="form-control" name="txtlogin" id="txtlogin"placeholder="Ingrese el nombre de Usuario">
			</div>
						 
			
				 
			 <br>
			</div> <br>
			
				
			<div class="panel panel-footer">
				 <button class="btn btn-large btn-primary" type="submit" value="Guardar" name="btnEnviar" id="btnEnviar" >
				 	<span class="glyphicon glyphicon-floppy-disk"></span> Enviar
				 </button>
			<br>
			<a href="Index.jsp">Ya tienes una cuenta? Inicia Sesion Aqui</a> 
			</div>	 
		</div>
		</div>
		</div>
		</div>
			 </form>
			 </div>
			 </div>
					
    </body>
		
		
			
</html>
