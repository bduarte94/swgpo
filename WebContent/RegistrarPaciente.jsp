<%@ include file="include/cabecera_2.jspf"%>

<%String idClinica=session.getAttribute("idClinica").toString();
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}
%>




<script type="text/javascript">

$(document).ready(function(){

    $("#myTab").click(function(e){

        e.preventDefault();

        $(this).tab('show');
     });
<%
String reg = request.getParameter("reg");
if(reg!=null){
    if(reg.equals("ok"))
        out.write(" $('#ok').show('slow');");
      else if(reg.equals("nok"))
        out.write(" $('#not').show('slow','linear');");
}else
	 
%>	 
	
				$('#fechaNac').on('focusout',function() {
					//alert ("im here");
					//$('#txtEdad').val("valor");
					if($('#fechaNac').val()!=""){
						var DOB = new Date($('#fechaNac').val());
		                var today = new Date();
		                var age = today.getTime() - DOB.getTime();
		                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
		                //alert (age);
		                $('#txtEdad').val(age);
					}else {
						$('#txtEdad').val("invalido");
					}
				})
				

	
	 });
</script>


<script type="text/javascript" src="js/VistaPreviaImg.js"></script>
<link href="css/registrar_pac.css" rel="stylesheet">

<style>
#formPac{
margin-left: 2em;
margin-right: 2em;	

}
#mensajes{
	margin-left:25%;
	margin-right: 25%;	
}
</style>



</head>
<body>
<%@ include file="include/menu.jspf" %>
	<br><br>
	<div id="contenido" class="container-wrapper" align="Center">
		<br><br>	
					<div id="mensajes">
					<div id="ok" class="alert alert-info" style="display:none;">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>&Eacute;xito!</strong>&nbsp; Se ha registrado el paciente.
                    </div>
                 	<div id="not" class="alert alert-danger" style="display:none;">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>Error!</strong>&nbsp; Ocurrio un error al registrar el paciente
                    </div>		
                    </div>
                    
<div class="page-header" align="left" style="margin-left: 2%">
    	<h3 class="text text-primary">Registro de paciente</h3>
</div>

		
 
		
		<form id="formRP" class="form" enctype="multipart/form-data" action="ManejarPeticionesPacientes" method="post">
	
	 	<div id="formPac">
	 	  <div class="row"> 	
	 		<div class="col-sm-8">
	 		
	 		<div class="panel panel-primary" align="Center">
	 			<div class="panel panel-heading">
	 				<h4>Ingrese Datos Generales Del Paciente</h4>
	 			</div>
	 			
	 				<div class="row">
	 					<div class="col-sm-2">
	 						<label>Nombre:</label>
	 					</div>
	 					<div class="col-sm-3">
	 						<input type="text"class="form-control" name="txtNomPac" placeholder="Ingrese Nombre Paciente" required="required" pattern="^[a-zA-z]+">
	 					</div>
	 					<div class="col-sm-2">
	 						<label>2do Nombre:</label>
	 					</div>
	 					<div class="col-sm-3">
	 						<input type="text"class="form-control" name="txtNomPac2" placeholder="Ingrese 2do Nombre Paciente">
	 					</div>
	 					
	 						
	 				</div><br>
	 				<div class="row">
	 					<div class="col-sm-2">
	 						<label>Apellido</label>
	 					</div>
	 					<div class="col-sm-3">
	 						<input type="text"class="form-control" name="txtApelPac" placeholder="Ingrese Apellido Paciente" required="required" pattern="^[a-zA-z]+">
	 					</div>
	 					<div class="col-sm-2">
	 						<label>2do Apellido:</label>
	 					</div>
	 					<div class="col-sm-3">
	 						<input type="text"class="form-control" name="txtApelPac2" placeholder="Ingrese 2do Apellido Paciente">
	 					</div>
	 					
	 				</div><br>
	 				<div class="row">
	 					<div class="col-xs-2 col-centered">Sexo:</div>
           		   			<div class="col-xs-3 col-centered"> 
           		   				<select class="form-control" name="sexo">
 								 <option value="Femenino">Femenino</option>
 								 <option value="Masculino">Masculino</option>
	 							</select>
	 						</div>
	 						
	 						<div class="col-sm-2">
	 						<label>Fecha de Nacimiento:</label>
	 						</div>
	 					<div class='col-sm-3'>
												
                				<div class="form-group">
                			<div class='input-group date' id='fechaNac'>
                   				 <input id="fechaNac" name="fechaNac" type="text" class="form-control" required="required" pattern="^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$">
                    			<span class="input-group-addon">
                        		<span class="glyphicon glyphicon-calendar"></span>
                    			</span>
                			</div>
            			</div>
            				
        					</div>
        				</div>
        				<br>
        				
        				<div class="row">
	 					<!--  <div class="col-sm-2">
	 						<label>N Tarjeta</label>
	 						</div>
	 						<div class="col-sm-3">
	 						<input type="number" class="form-control" name="txtTarjeta" placeholder="Ingrese tarjeta">
	 						</div> -->
	 						<div class="col-sm-2">
	 						<label>Email</label>
	 						</div>
	 						<div class="col-sm-8">
	 						<input type="email" class="form-control" name="txtEmail" placeholder="Ingrese Email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
	 						</div>
	 						
	 						<!-- <div class="col-sm-2">
	 							<label>Edad</label>
	 						</div>
	 						<div class="col-sm-2">
	 							<input id= "txtEdad" type="text" class="form-control" name="txtEdad" readonly="readonly"/>
	 						</div> -->
	 						
	 				  </div><br>
        				
        				<div class="row">
        					<div class="col-sm-2">
	 						<label>Telefono</label>
	 						</div>
	 						<div class="col-sm-3">
	 						<input type="number" class="form-control" name="txtTel" placeholder="Ingrese telefono">
	 						</div>
	 					   
	 					   <div class="col-sm-2">
	 						<label>Movil</label>
	 						</div>
	 						<div class="col-sm-3">
	 						<input type="number" class="form-control" name="txtMov" placeholder="Ingrese telefono">
	 						</div>
	 					 
	 					    				
        				</div><br>
        				
        				<div class="row">
        					<div class="col-sm-2">
	 						<label>Compania Seguro</label>
	 						</div>
	 						<div class="col-sm-3">
	 						<input type="text" class="form-control" name="txtseguro" placeholder="Ingrese Seguro">
	 						</div>
	 						<div class="col-sm-2">
	 						<label>N Seguro</label>
	 						</div>
	 						<div class="col-sm-3">
	 						<input type="number" class="form-control" name="txtNseguro" placeholder="Ingrese numero de seguro">
	 						</div>
	 											   
        				
        				</div>
	 					
	 				<br>
	 				
	 				<div class="row">
	 					<div class="col-sm-2">
	 						<label>Direccion</label>
	 						</div>
	 						<div class="col-sm-5">
	 						<input type="text" class="form-control" name="txtDireccion" placeholder="Ingrese Direccion">
	 						</div>
	 						<div class="col-sm-1">
	 						<label>Ciudad</label>
	 						</div>
	 						<div class="col-sm-2">
	 						<select class="form-control" name="ciudad">
 								 <option value="Managua">Managua</option>
 								 <option value="Masaya">Masaya</option>
 								 <option value="Leon">Leon</option>
 								 <option value="Chinandega">Chinandega</option>
 								 <option value="Esteli">Esteli</option>
 								 <option value="Carazo">Carazo</option>
 								 <option value="Rivas">Rivas</option>
 								 <option value="Rio San Juan">Rio San Juan</option>
 								 <option value="Chontales">Chontales</option>
 								 <option value="Boaco">Boaco</option>
 								 <option value="Jinotega">Jinotega</option>
	 							</select>
	 						</div>
	 				</div><br>
	 				<div class="row">
	 					   <div class="col-sm-2">
	 						<label>Grupo Sanguineo</label>
	 					</div>
	 					<div class="col-sm-2">
	 						<select class="form-control" name="txtTipoSangre">
 								 <option value="A">A</option>
 								 <option value="B">B</option>
 								 <option value="AB">AB</option>
 								 <option value="O">O</option>
	 							</select>
	 					</div>
	 					
	 				</div><br>	 				
	 				<div class="row">
	 						<div class="col-sm-2">
	 						<label>Observaciones</label>
	 						</div>
	 						<div class="col-sm-8">
	 						<input type="text" class="form-control" name="txtObservacion" placeholder="Ingrese Observacion">
	 						</div>
	 				</div><br>
	 				<script type="text/javascript">
       					     $(function () {
                				$('#fechaNac').datetimepicker({
       					    	format: 'YYYY-MM-DD',
                		 		viewMode: 'years'
                		 		
           				 	});
                
           					});
           					
           				
           			</script>
           			
           			
           			
	 				
			</div><!--Final primer panel "datos generales del paciente"-->
			
			
			<div class="panel panel-primary">
				<div class="panel panel-heading">
					<h4>Historial M�dico</h4>
				</div>			
			
				<ul id="mytab" class="nav nav-tabs nav-justified">
  								<li class="active"><a data-toggle="tab" href="#antecPer">Ant. Personales</a></li>
  								<li><a data-toggle="tab" href="#antecFam">Ant. Familiares</a></li>
  								<li><a data-toggle="tab" href="#alergia">Alergias</a></li>
  								<li><a data-toggle="tab" href="#vac">Vacunas</a></li>
  								<li><a data-toggle="tab" href="#trat">Trat. Habitual</a></li>
  								<li><a data-toggle="tab" href="#cir">Cirugias</a></li>
  								<li><a data-toggle="tab" href="#enfer">Enfer. Cr�nicas</a></li>
							</ul>
							<div class="tab-content">

    							<div id="antecPer" class="tab-pane fade in active">
    							<textarea name="antPer" placeholder="Ingrese descripcion de antecedentes personales" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>

    							<div id="antecFam" class="tab-pane fade">
								<textarea name="antFam" placeholder="Ingrese descripcion de antecedentes Familiares" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>
    							
    							<div id="alergia" class="tab-pane fade">
								<textarea name="ale" placeholder="Ingrese descripcion de alergias" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>
    							
    						<!--  	<div id="vac" class="tab-pane fade">
								<textarea name="vac" placeholder="Ingrese vacunas" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>
    							-->
    							<div id="trat" class="tab-pane fade">
								<textarea name="trat" placeholder="Ingrese descripcion de tratamientos habituales" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>
    							<div id="cir" class="tab-pane fade">
								<textarea name="cir" placeholder="Ingrese descripcion de cirugias" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>
    						<!--	<div id="enfer" class="tab-pane fade">
							 	<textarea name="ENFER" placeholder="Ingrese enfermedades cr�nicas" class="form-control" rows="3" maxlength="100" cols="50"></textarea>
    							</div>  -->
    						
    						</div>
							
			
			</div>
			
			</div>	<!-- Columna 1 -->
			<div class="col-sm-4">
	 		<div class="panel panel-primary" align="Center">
	 			<div class="panel panel-heading">
	 			<h4>Foto De Paciente</h4>
	 			</div>
	 			<div class="panel panel-body">
	 			<div class="row">
	 			<img id="vistaPrevia" class="img-responsive" alt="" src="" width="128px" height="128px">
				</div>
				</div>
				<div id='botonera' class="panel panel-footer">
				<span id="infoNombre" hidden="true">[Seleccione una imagen]</span><br/>
            	<span id="infoTama�o" hidden="true"></span>
	 			<input id="archivo" type="file" name="foto" class="file" placeholder="Subir foto" accept="image/*">
				</div>
			</div><br>
			
	 		
			</div>
			
			</div>
			
			<div class="row">
	 		<div class="panel panel-footer">
	 					<a class="btn btn-large btn-default" type="submit" value="cancelar" name="cancelar" href="javascript:history.go(-1)">
                  		 <span class="glyphicon glyphicon-ban-circle"></span> Cancelar
                   		</a>
	 					<button class="btn btn-large btn-primary" type="submit" value="save" name="save">
                  		 <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                   		</button>
	 				</div>
	 	
		</div>
			
			
		</div>
		
		
	 	
		<!--  <input type="hidden" id="class" name="class" value="Common.PacienteCOM"/>
             	<input type="hidden" id="method" name="method" value="registrarPaciente"/> -->
				<input type="hidden" id="idClinica" name="idClinica" value="<%=idClinica%>" />
		</form>
		
	</div>
<%@ include file="include/footer.jspf" %>
	
	<script src="js/Moment.js"></script>
	<script src="js/bootstrap-datetimepicker.js"></script>
</body>