
<%@ include file="include/cabecera_2.jspf"%>
<%@page import="com.dhtmlx.planner.controls.DHXLocalization"%>
<%@page import="org.apache.catalina.connector.Request"%>
<%@page import="java.util.Calendar"%>
<%@page import="Common.ClinicaCOM"%>
<%@page import="Common.UsuarioCOM"%>
<%@page import="com.dhtmlx.planner.*,com.dhtmlx.planner.data.*"%>



<%
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}

	String idOdontologo = "";
	String idClinica = "";
	String rol = "";
	if (session.getAttribute("idOdontologo") != null)
		idOdontologo = session.getAttribute("idOdontologo").toString();
	if (session.getAttribute("idClinica") != null)
		idClinica = session.getAttribute("idClinica").toString();
	if (session.getAttribute("tipoUser") != null)
		rol = session.getAttribute("tipoUser").toString();

%>
<%@page import="utils.StringUtils"%>
<%@page import="Common.PacienteCOM"%>
<%@page import="Common.OdontologoCOM"%>

<link href="css/inicio.css" rel="stylesheet">
<link href="css/scrolling-nav.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/Bootstrap-select.css" rel="stylesheet">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui.min.js"></script>

<script src="codebase/locale/locale_es.js" charset="utf-8"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#regcitasbutton").click(function () {
            $("#regcitas").dialog({
                width: 700,
                height: 350,
                show: "scale",
                hide: "scale",
                modal: "true"

            });
        });
        $("#verAgenda").click(function () {
        	document.getElementById("iframe").contentDocument.location.reload(true);
            $("#Calendario").show('slow')
            $("#schema").hide('slow')
        });
        $("#OcultarAgenda").click(function () {
            $("#Calendario").hide('slow')
            $("#schema").show('slow')
        });



    });


</script>	

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'

        });

        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD hh:mm'
         

        });
        
        $('#datetimepicker3').datetimepicker({
            format: 'YYYY-MM-DD hh:mm'
        });
    });
</script>
</head>
<body >

    <%@ include file="include/menu.jspf"%>
        <!-- Ventanas emergentes -->

        <%@ include file = "include/RegistrarCita.jspf" %>

		
		
        <!-- Contenido principal -->
        <div class="container-fluid" >	
        	<div class="row" style="margin-left: 3%; margin-top: 3%;">
        		<h2><%out.write(new ClinicaCOM().getNombreclinica(idClinica)); %></h2> <h4> ROL: <%out.write(new UsuarioCOM().getRolUser(rol));%></h4>
        	</div>
        <div id="schema" class="row">
			
            <!-- Navigation Buttons -->
            <div id="menu1" class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
             <div id="myScrollspy" >
                <ul id="myTabs">
                <li class="page-scroll"><a class="menu FXlight" type="button" href="#ini"  style="font-size: x-large;"><span class="fa fa-desktop"> Presentación</span></a></li>
                    <li class="page-scroll"><a class="menu FXlight" type="button" href="#paciente"  style="font-size: x-large;"><span class="fa fa-stethoscope"> Pacientes</span></a></li>
                    <li class="page-scroll"><a class="menu FXlight" id="boton" type="button" href="#cita"  style="font-size: x-large;"><span class="fa fa-calendar"> Agenda</span></a></li>
                    <li class="page-scroll"><a class="menu FXlight" type="button" href="#tratamientos" style="font-size: x-large;"><span class="fa fa-user-md"> Atención</span></a></li>
                    <li class="page-scroll"><a class="menu FXlight" type="button" href="#catalogos"  style="font-size: x-large;"><span class="fa fa-cog"> Catálogos</span></a></li>
                    <!--  <li><a type="button" href="#notificaciones">Plantillas de notificaciones</a></li>-->
                    <li class="page-scroll"><a class="menu FXlight" type="button" href="#reportes"  style="font-size: x-large;"><span class="fa fa-pie-chart"> Reportes</span></a></li>
                </ul>
            </div>
			</div>
            <!-- Content -->
            <div id="contenido"  class="col-xs-12 col-sm-12 col-md-9 col-lg-8">
            <div id="content" class="container-fluid">
                <div class="tab-content">
                    <div class="tab-pane active" id="ini">
                        <section>
                        	<!-- <div class="title row">
                            	<div class="well well-sm" >
                                <h4> Sistema Web De Gestión De Paciente Odontologicos </h4>
                            	</div>
                            </div> -->
                            <div class="row">
                        	<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
								  <ol class="carousel-indicators">
								    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								    <li data-target="#myCarousel" data-slide-to="1"></li>
								    
								  </ol>
								
								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								    <div class="item active">
								      <img src="img/ini.jpg" alt="">
								    </div>
								
								    <div class="item">
								      <img src="img/ini2.jpg" alt="">
								    </div>
								
								  
								  </div>
								
								  <!-- Left and right controls -->
								  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
								    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
								    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								    <span class="sr-only">Next</span>
								  </a>
								</div>
							</div>
                        </section>
                    </div>
					
					<section class="tab-pane " id="paciente"> 
					<div class="span9">
                            <div class="title row">
                            	<div class="well well-sm" >
                                <h4> Gestion De Pacientes </h4>
                            	</div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="RegistrarPaciente.jsp" type="button" class="botones btn btn-primary">
                                        <h4>Registrar Paciente</h4>
                                        <img class="img-responsive" alt="" src="img/reg2.png">
                                        
                                    </a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a href="BuscarPaciente.jsp" type="button" class="botones btn btn-success">
                                        <h4>Buscar Paciente</h4>
                                        <img class="img-responsive" alt="" src="img/lupa.gif">
                                    </a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a href="ListaPacientes.jsp" type="button" class="botones btn btn-info">
                                    	<h4>Listar pacientes</h4>
                                        <img class="img-responsive" alt="" src="img/Lista.png">
                                    </a>
                                </div>

                                <!--  <div class="col-sm-3">
                                        <a href="#" type="button" class="btn btn-primary">
                                        <img class="img-responsive" alt="" src="img/notif.png">
                                        <h4>Notificar paciente</h4>
                                        </a>
                                </div>-->

                            </div>
						</div>
                        </section>
					
                    <section class="tab-pane" id="tratamientos">
                            <div class="title row">
                            	<div class="well well-sm" >
                                <h4> Gestion De Atenciones </h4>
                            	</div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="odonto.jsp" type="button" class="botones btn btn-primary">
                                         <h4>Registrar Atención</h4>
                                        <img class="img-responsive" alt="" src="img/regOdonto.png">
                                       
                                    </a>
                                </div>

                               <!--  <div class="col-sm-4">
                                    <a id="button1" href="ConsultarOdonto.jsp" type="button" class="btn btn-primary">
                                        <img class="img-responsive" alt="" src="img/upOdonto.png">
                                        <h4>Actualizar<br>Odontograma</h4>
                                    </a>
                                </div> -->
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="odontograma.jsp" type="button" class="botones btn btn-success">
                                        <h4>Odontograma</h4>
                                        <img class="img-responsive" alt="" src="img/upOdonto.png">
                                        
                                    </a>
                                </div>
                                
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="PlanPago.jsp" type="button" class="botones btn btn-info">
                                        <h4>Plan de pagos</h4>
                                        <img class="img-responsive" alt="" src="img/credito.png">
                                        
                                    </a>
                                </div>
                                

                             <!--     <div class="col-sm-3">
                                    <a href="#" type="button" class="btn btn-primary">
                                        <img class="img-responsive" alt="" src="img/Atencion.png">
                                        <h4>Nuevo<br>Ingreso</h4>
                                    </a>
                                </div>-->


                            </div>
                    </section>


                    <section class="tab-pane" id="cita">
                        <div class="navbar-inner">
                            <div > 
                                
                            	<div class="well well-sm row" >
                                <h4> Gestion De Citas </h4>
                            	</div>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <a id="regcitasbutton" class="botones btn btn-primary">
                                        <h4>Programar Cita</h4>
                                            <img class="img-responsive" alt="" src="img/regCita.png">
                                            
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <a id="verAgenda2" href="Calendar.jsp?idClinica=<%=idClinica%>&idOdontologo=<%=idOdontologo%>" type="button" class="botones btn btn-success">
                                            <h4>Calendario citas</h4>
                                            <img class="img-responsive" alt="" src="img/calendar.png">
                                            
                                        </a>
                                    </div>


                                </div>
                        </div>
                    </section>
                    
                     <section class="tab-pane" id="catalogos">
                        <div > 
                            
                            	<div class="well well-sm row" >
                                <h4> Catálogos </h4>
                            	</div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="CatServicios.jsp" type="button" class="botones btn btn-primary">
                                    <h4>Servicios</h4>
                                        <img class="img-responsive" alt="" src="img/medicos.png">
                                        
                                    </a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="" type="button" class="botones btn btn-success">
                                    <h4>Horario Atención</h4>
                                        <img class="img-responsive" alt="" src="img/schedule.png">
                                        
                                    </a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="Registro.jsp" type="button" class="botones btn btn-info">
                                    <h4>Usuarios</h4>
                                        <img class="img-responsive" alt="" src="img/user.png">
                                        
                                    </a>
                                </div>
                                

                              </div>
                        </div>
                    </section>
                    
                        <section class="tab-pane" id="reportes">
                        <div > 
                            
                            	<div class="well well-sm row" >
                                <h4> Reportes </h4>
                            	</div>
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a id="button1" href="UReportes.jsp" type="button" class="botones btn btn-primary">
                                    <h4>Reportes<br>ODONTOWEB</h4>
                                        <img class="img-responsive" alt="" src="img/regOdonto.png">
                                        
                                    </a>
                                </div>

                              </div>
                        </div>
                    </section>
                </div>
            </div>
		</div> <!-- comienza calendario -->
		
		 				
        </div>
	
		
		<div id="Calendario" style="display: none;" class="container-fluid">
			<div class="row">
				<div class="well well-sm col-xs-12 col-sm-12 col-md-12 col-lg-12">Calendario De Citas <button id="OcultarAgenda" type="button" class="close" data-dismiss="modal" title="Cerrar Agenda">&times;</button></div>	
			</div>
			<div class = "row">
				<iframe class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="iframe" src="Calendar.jsp?idClinica=<%=idClinica%>&idOdontologo=<%=idOdontologo%>" >	
				</iframe>
			</div>
		</div>
		</div>
        <!-- Enable the tabs -->
        <script type="text/javascript">
            $('#myTabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show','fade');
            });

            jQuery(".FXlight").hover(function () {
                if (jQuery(this).attr("FXlight") == undefined) {
                    jQuery(this).attr("FXlight", true).animate({
                        opacity: 0.4
                    }, 250).animate({
                        opacity: 1.0
                    }, 250, function () {
                        jQuery(this).removeAttr("FXlight");
                    });
                }
            }, function () {
            });
        </script>
 
    
    <%@ include file="include/footer2.jspf"%>
     <%//@ include file="include/footer.jspf"%>
</body>
	
</html>