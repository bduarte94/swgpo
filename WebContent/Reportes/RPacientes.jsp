<%-- 
    Document   : ROdontologos
    Created on : 27-jul-2015, 21:02:25
    Author     : Jimmy
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="net.sf.jasperreports.engine.*"%>
<%@page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.util.*"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generación de Reportes de Odontologos</title>
    </head>
    <body>  
        <%
            try {
                Class.forName("org.gjt.mm.mysql.Driver");
                System.setProperty ("java.awt.headless", "true");
                Connection conexion = DriverManager.getConnection(
                        "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");

                String idOdontologo = request.getParameter("txtCodigo");
                if (!idOdontologo.isEmpty()) {

                    File reportfile = new File(application.getRealPath("Reportes/Pacientes.jasper"));
                    
                    Map parameters = new HashMap();

                    parameters.put("CodOdonto", idOdontologo);

                    byte[] bytes = JasperRunManager.runReportToPdf(reportfile.getPath(), parameters, conexion);

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    ServletOutputStream outputstream = response.getOutputStream();
                    outputstream.write(bytes, 0, bytes.length);
                    outputstream.close();
                } else {
                    File reportfile = new File(application.getRealPath("Reportes/PacientesGen.jasper"));

                    Map parameters = new HashMap();

                    parameters.put("", "");

                    byte[] bytes = JasperRunManager.runReportToPdf(reportfile.getPath(), parameters, conexion);

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    ServletOutputStream outputstream = response.getOutputStream();
                    outputstream.write(bytes, 0, bytes.length);
                    outputstream.close();

                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error" + ex.toString());
            }

        %>
    </body>
</html>
