

<%@page import="java.sql.Timestamp"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="net.sf.jasperreports.engine.*"%>
<%@page import="net.sf.jasperreports.engine.*"%>
<%@page import="java.util.*"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generación de Reportes de Odontologos</title>
    </head>
    <body>  
        <%
            try {
                Class.forName("org.gjt.mm.mysql.Driver");
                Connection conexion = DriverManager.getConnection(
                        "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");

                String idOdontologo = request.getParameter("txtCodigoOdontologo");
                String idPaciente = request.getParameter("txtCodigoPaciente");
                /*String FechaI = request.getParameter("fechaI");
                String FechaF = request.getParameter("fechaF");*/

                SimpleDateFormat formatoDeFecha = new SimpleDateFormat("MM/dd/yyyy H:mm");
                
                String FechaI = request.getParameter("fechaI");
                String FechaF = request.getParameter("fechaF");
                
                String arrayFechaI [] = FechaI.split("-");
                String anioI = arrayFechaI[0];
                String mesI = arrayFechaI[1];
                String diaI = arrayFechaI[2];
                
                String fechaDesde = anioI + "-" + mesI + "-" + diaI;
                //Date di =formatoDeFecha.parse(fechaDesde);
                /*Date dtt = formatoDeFecha.parse(FechaI);
                Date dtt2 = formatoDeFecha.parse(FechaF);*/
                
                
                String arrayFechaF [] = FechaF.split("-");
                String anioF = arrayFechaF[0];
                String mesF = arrayFechaF[1];
                String diaF = arrayFechaF[2];
                
                String fechaHasta = anioF + "-" + mesF + "-" + diaF ;
               // Date df =formatoDeFecha.parse(fechaHasta);
                
                
                
              /* java.sql.Date dii = new java.sql.Date(dtt.getDate());
               java.sql.Date dii2 = new java.sql.Date(dtt2.getDate());*/
                             
                if (!idOdontologo.isEmpty() || !idPaciente.isEmpty() || !FechaI.isEmpty() || !FechaF.isEmpty()) {

                    File reportfile = new File(application.getRealPath("Reportes/Cita.jasper"));

                     
                    Map parameters = new HashMap();

                    parameters.put("IdOdontologo", idOdontologo);
                    parameters.put("IdPaciente", idPaciente);
                    parameters.put("FechaI",fechaDesde);
                    parameters.put("FechaF",fechaHasta);
                                      

                     byte[] bytes = JasperRunManager.runReportToPdf(reportfile.getPath(), parameters, conexion);

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    ServletOutputStream outputstream = response.getOutputStream();
                    outputstream.write(bytes, 0, bytes.length);
                    outputstream.close();
                } else {

                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error" + ex.toString());
            }

        %>
    </body>
</html>
