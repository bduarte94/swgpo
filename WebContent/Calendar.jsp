<%@ include file="include/cabecera_2.jspf"%>
<%@page import="com.dhtmlx.planner.controls.DHXLocalization"%>
<%@page import="com.dhtmlx.planner.extensions.DHXExtension"%>
<%@page import="com.dhtmlx.planner.controls.DHXLightboxSelect"%>
<%@page import="com.dhtmlx.planner.controls.DHXLightboxSelectOption"%>
<%@page import="com.dhtmlx.planner.api.DHXEventBox" %>
<%@page import="org.apache.catalina.connector.Request"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dhtmlx.planner.*,com.dhtmlx.planner.data.*"%>
<%@page import="com.dhtmlx.planner.controls.DHXExternalLightboxForm" %>


	<script src="./codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
	<script src="./codebase/ext/dhtmlxscheduler_container_autoresize.js" type="text/javascript" charset="utf-8"></script>
	<script src="./codebase/locale/locale_es.js" type="text/javascript" charset="utf-8"></script>
	<script src="./codebase/locale/recurring/locale_recurring_es.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="./codebase/dhtmlxscheduler.css" type="text/css">

</head>



	<%
	if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
		response.sendRedirect("Index.jsp");
	}
	
	String idOdontologo ="";
	String idClinica = "";
	if (request.getParameter("idOdontologo")!=null)
	 	idOdontologo=request.getParameter("idOdontologo");
		
	if (request.getParameter("idClinica")!=null) 
		idClinica=request.getParameter("idClinica");
	else
		response.sendRedirect("Index.jsp");
	%>
	<!-- <script src="codebase/locale/locale_es.js" charset="utf-8"></script>
	<script src="codebase/ext/dhtmlxscheduler_quick_info.js" type="text/javascript" charset="utf-8"></script>
	<script src="codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
 -->
	
	
<style>
<!--


.dhx_cal_navline,
.dhx_cal_header {
  	background-color: white;
}
.dhx_cal_navline{
	top: 18px !important;
	margin-left: -8px;  
}  
 .dhx_cal_header {
	top: 78px !important;  
	margin-left: -1px;  
}
		.planner  {
			position:relative;
			margin-top: 5%;
				
		}
	
-->
</style>


<%-- <%! 
		String getPlanner(HttpServletRequest request,String idOdontologo,String idClinica) throws Exception{
		DHXPlanner planner= new DHXPlanner("./codebase/",DHXSkin.TERRACE);
		DHXLocalization locale=null;
		//s.setWidth(1200);
		//s.setHeight(550);
		//DHXExtension ext;
		
		
		planner.extensions.add(DHXExtension.CONTAINER_AUTORESIZE);
		
		
		//DHXExternalLightboxForm box = planner.lightbox.setExternalLightboxForm("./codebase/custom_editor.jsp");
		
		DHXLightboxSelect paciente = new DHXLightboxSelect("idPaciente", "Pacientes");
		paciente.setServerList("paciente");
		paciente.setHeight(40);
		planner.lightbox.add(paciente);
		
		if(idOdontologo.compareTo("")==0 || idOdontologo==null){
		DHXLightboxSelect odontologos = new DHXLightboxSelect("idOdontologo1", "Odontologos");
		odontologos.setServerList("odontologo");
		odontologos.setHeight(40);
		planner.lightbox.add(odontologos);	
		}
		//select.addOption(new DHXLightboxSelectOption("1", "Juan"));
		//select.addOption(new DHXLightboxSelectOption("2", "Pedro"));
		//select.addOption(new DHXLightboxSelectOption("3", "Luis"));
		
		//DHXEvent sel=new DHXEvent();
		
		
		//configura un nuevo box(cajs de texto para mostrar info de citas)
		//DHXEventBox ev = new DHXEventBox();
		//ev.setTemplate("<div class='custom_event' style='background-color:#1796B0; opacity:0.7; padding:10px;'>"+
		//          "<span class='event_date'>{start_date:date(%H:%i)}</span> - "+
		//          "<span class='event_date'>{end_date:date(%H:%i)}</span><br>"+
		//          "<span><b>{text}</b></span><br>"+
		//          "<span><select><option id=1>Value</option></select></span><br>"+	  
		//          "</div>"+
		//          "</div>");
		//ev.setCssClass("custom_event");
		//planner.templates.setEventBox(ev);
		
		planner.config.setDetailsOnCreate(true);
		planner.config.setDblClickCreate(true);
		planner.config.setDetailsOnDblClick(true);
		
		
		Calendar now = Calendar.getInstance();
		planner.setInitialDate(now);
		planner.localizations.set(locale.Spanish);
		planner.load("events.jsp?idClinica="+idClinica+"&idOdontologo="+idOdontologo, DHXDataFormat.JSON);
		planner.data.dataprocessor.setURL("events.jsp?idClinica="+idClinica+"&idOdontologo="+idOdontologo);
		return planner.render();
	}
	
	%> --%>




<body>
	 <%@ include file="include/menu.jspf"%>
	<%-- <div id="planner" class="planner"><%//=getPlanner(request,idOdontologo,idClinica) %></div> --%>
	
	<div id="planner" class="container-fluid planner">
	
	<div id='scheduler_here' class='dhx_cal_container' style='width:100%;height:100%;'>
		<div class='dhx_cal_navline'>
			<div class='dhx_cal_prev_button'>&nbsp;</div>
			<div class='dhx_cal_next_button'>&nbsp;</div>
			<div class='dhx_cal_today_button'></div>
			<div class='dhx_cal_date'></div>
			<div class='dhx_cal_tab' name='month_tab' style='left:76px;'></div>
			<div class='dhx_cal_tab' name='week_tab' style='left:140px;'></div>
			<div class='dhx_cal_tab' name='day_tab' style='left:204px;'></div>
		</div>
		<div class='dhx_cal_header'></div>
		<div class='dhx_cal_data'></div>
	</div>
	<script> 
	scheduler.config.serverLists = {};
	scheduler.locale.labels.section_description = 'Descripción';
	scheduler.locale.labels.section_time = 'Periodo De Tiempo';
	<%if(idOdontologo.compareTo("")==0 || idOdontologo==null){%>scheduler.locale.labels.section_idOdontologo1 = 'Odontologos';<%}%>
	scheduler.locale.labels.section_pacientes = 'Pacientes';
	scheduler.config.lightbox.sections = [
		{"map_to":"text", "name":"description", "type":"textarea", "height":200},
		{"map_to":"auto", "name":"time", "type":"time", "height":72},
		<%if(idOdontologo.compareTo("")==0 || idOdontologo==null){%>{"map_to":"idOdontologo1", "name":"idOdontologo1", "type":"select", "height":40, "options":scheduler.serverList("odontologo"),filtering:true},<%}%>
		{"map_to":"idPaciente", "name":"idPaciente", "type":"select", "height":40, "options":scheduler.serverList("paciente"),filtering:true}
		
	];

	
	
	scheduler.config.details_on_create = true;
	scheduler.config.details_on_dblclick = true;
	scheduler.config.skin = 'terrace';
	scheduler.config.dblclick_create = true;
	scheduler.init('scheduler_here',new Date());
	scheduler.config.prevent_cache = true;
	scheduler.load("events.jsp?idClinica=<%=idClinica%>&idOdontologo=<%=idOdontologo%>", "json");
	var dp = new dataProcessor("events.jsp?idClinica=<%=idClinica%>&idOdontologo=<%=idOdontologo%>");
	dp.init(scheduler);
	dp.setTransactionMode("POST", false); 
	
	scheduler.attachEvent("onEventSave",function(id,ev){
		if (!ev.text) {
			dhtmlx.alert("Descripcion no puede estar vacia");
			return false;
		}
		/* if (ev.text.length<20) {
			dhtmlx.alert("Text too small");
			return false;
		} */
		return true;
	}); 

	
	</script>
	  </div>  
</body>
	
	
	
</body>
</html>