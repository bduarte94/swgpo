<%@ include file="include/cabecera.jspf"%>
<%@page import="Common.PacienteCOM"%>
<%//String idOdontologo=session.getAttribute("idOdontologo").toString(); 
if(session.getAttribute("session")==null || session.getAttribute("session").equals(false)){
	response.sendRedirect("Index.jsp");
}
%>
<style>
<!--
#content{
	padding-top: 25px;
    padding-right: 50px;
    padding-bottom: 25px;
    padding-left: 50px;
}

-->
</style>

</head>
<body>
<%@ include file="include/menu.jspf"%><br><br><br><br><br><br>
 <div id="content" class="container-fluid" align="center">
 	
 		<div class="row" align="center">
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <img src="img/people.png" >
      <div class="caption">
        <h3>Gestion de Usuarios</h3>
        <p align="center">
          <a href="ListaUsuario.jsp" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-arrow-right">Acceder</span></a>
        </p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <img src="img/aceptar.png" >
      <div class="caption">
        <h3>Ver solicitudes de usuarios</h3>
        <p align="center">
          <a href="ListaSolicitudesCuenta.jsp" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-arrow-right">Acceder</span></a>
        </p>
      </div>
    </div>
  </div>
</div>
 	
 </div>
  <%@ include file="include/footer.jspf"%>
</body>