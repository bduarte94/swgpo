<%@page import="java.util.ArrayList"%>
<%@ include file="include/cabecera.jspf"%>



<head>
    <link href="css/registrar_pac.css" rel="stylesheet">

    <style type="text/css">
        input{
            border-radius:10px;
            box-shadow:0px 0px 25px rgba(30,144,255,0.3) inset;
            width: 200px;
            height: 30px;
            padding: 5px;
            margin: 5px;

        }
        table{
            width: 700px;
            border: 1px solid rgba(255,255,255,0.4);
            padding: 20px;
            margin: 50px;
            border-radius: 25px 25px 25px 25px;
            box-shadow:0px 10px 25px #0066cc;
            border-color: #ffffff;
            overflow: scroll;

        }
        td{
            text-align:  center;
            font-size: 18px italic;


        }

        th{
            text-align:  center;
            font-size: 18px italic;


        }
        .titulo{
            font:bold 25px;

        }

    </style>


</head>
<body>
    <%@ include file="include/menu.jspf" %>
    <br><br>
    <div id="contenido" class="container-wrapper" align="Center">	


        <div class="page-header" align="center">
            <h3 class="text text-primary">Editar Odontrograma</h3>
        </div>

        <form id="formRP" class="form" method="GET" action="ConsultarOdonto.jsp">
            <div class="row">

                <div class="col-sm-9">
                    <div class="panel panel-primary" align="Center" style="width: 80em" >
                        <div class="panel panel-heading">
                            <h4>Editar Odontograma</h4>
                        </div>

                        <div class="row" align="center">
                            <div>
                                <label>Ingrese el c�digo del paciente</label>
                            </div>
                            <div class="col-sm-3" style="width:20%; left: 40%">
                                <input type="text" class="form-control" name="txtCodigo">
                                <br>

                            </div>




                        </div>
                        <input type="submit" class="btn-success" id="btnEnviar" name="btnEnviar" value="Consultar">


                    </div>

                </div>
            </div>




        </form>

        <form action="editarOdonto.jsp" method="get" name="frmEditar">
            <div class="row">
                <div class="col-sm-9">

                    <div class="panel-primary" align="center" style="width: 90em">
                        <div class="row" align="center">
                            <div>
                                <table border=1 width="160%">
                                    <tr>
                                        <th>Codigo Odontograma</th>
                                        <th>Codigo Paciente</th>
                                        <th>Id del Diente</th>
                                        <th>Codigo del Tratamiento</th>
                                        <th>Codigo de la Atenci�n</th>
                                        <th>Editar</th>
                                    </tr>
                                    <%@ page import="Entidades.Odontograma" %>
                                    <jsp:useBean id="empControlador" class="com.controlador.verificarOdonto.verificarCodigoOdonto" />


                                    <%
                                        int codPaciente = 0;
                                        
                                        try{
                                        if (request.getParameter("txtCodigo") != null) {
                                            codPaciente = Integer.parseInt(request.getParameter("txtCodigo"));
                                        }
                                        else{
                                            if (request.getParameter("txtCodigo") == null || request.getParameter("txtCodigo").isEmpty()
                                               || request.getParameter("txtCodigo").equals("")
                                                    )
                                            codPaciente = 0;
                                        }

                                        ArrayList arrayListEmpleado = empControlador.getEmpleados(codPaciente);
                                        for (int i = 0; i < arrayListEmpleado.size(); i++) {
                                            Odontograma emp = (Odontograma) arrayListEmpleado.get(i);
                                    %>
                                    <tr>
                                        <td><%=emp.getIdOdonto()%></td>
                                        <td><%=emp.getIdPacienta()%></td>
                                        <td><%=emp.getIdDiente()%></td>
                                        <td><%=emp.getIdTratamiento()%></td>
                                        <td><%=emp.getAtencion()%></td>
                                        <td><input type="radio" value="<%=emp.getIdOdonto()%>" name="seleccion" checked></td>
                                    </tr>
                                    <%
                                        }

                                        if (empControlador.retornarEstado() > 0) {%>
                                    <input type="submit" class="btn-info" id="btnEnviar" name="btnEditar" value="Editar">
                                    <%
                                        }
                                        
                                       
                                        }
                                        catch(Exception exce){
                                            javax.swing.JOptionPane.showMessageDialog(null, "Error al procesar la solicitud","Error en la consulta",0);
                                        }

                                    %>



                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </form>


    </div>

    <%@ include file="include/footer.jspf" %>
</body>
