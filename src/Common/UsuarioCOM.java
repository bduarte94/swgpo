package Common;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONObject;

import com.mysql.jdbc.PreparedStatement;

import Dao.Email;
import Peticion.BasePeticiones;
import conexion.Conexion;

public class UsuarioCOM extends BasePeticiones{
	private java.sql.Connection con=null;
	public Conexion conexion=new Conexion();
	public String ListUsers(){
		StringBuilder PacienteResultado= new StringBuilder();
		try{
				
				con=conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("SELECT * FROM odontologo,usuario where idOdontologo=usuario.OdontologoU_idOdontologo and EstadoU_idEstado=1;");
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				PacienteResultado.append("<table class=\"table table-striped\"><th></th>");
				PacienteResultado.append("<thead>");
				PacienteResultado.append("<th>Nombre</th>");
				PacienteResultado.append("<th>Accion</th>");
				PacienteResultado.append("</thead>");
				PacienteResultado.append("<tbody>");
				while(re.next()){
					//PacienteResultado.append("<form action=\"ManejarPeticiones\" method=\"get\">");
					
				PacienteResultado.append("<tr>");
						PacienteResultado.append("<td>"+re.getString("Onombre1")+" "+re.getString("Onombre2")+" "+re.getString("Oapellido1")+" "+re.getString("Oapellido2")+"</td>");
						PacienteResultado.append("<td>");
						PacienteResultado.append("<button class=\"btn btn-primary   rel=\"tooltip\" title=\"Ver informacion\" id=\"buttonValUser\"");
						PacienteResultado.append(" onclick=\"$(location).attr('href','DetalleOdontologo.jsp?list=1&idOdontologo="+re.getInt("idOdontologo")+"');\" style=\"margin-bottom: 0px;\">");
						PacienteResultado.append("<i class=\"glyphicon glyphicon-list-alt\"></i> </button>");
						
						PacienteResultado.append("<button class=\"btn btn-success   rel=\"tooltip\" title=\"Entrar a gestion de "+re.getString("Onombre1")+"\" id=\"buttonValUser\"");
						PacienteResultado.append(" onclick=\"$(location).attr('href','Inicio.jsp?idOdontologo="+re.getInt("idOdontologo")+"');\" style=\"margin-bottom: 0px;\">");
						PacienteResultado.append("<i class=\"glyphicon glyphicon-th\"></i> </button>");
						
						PacienteResultado.append("</td>");
						
				PacienteResultado.append("</tr>");
								
				}
				PacienteResultado.append("</tbody>");
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		return PacienteResultado.toString();
	}
	
	public String getUser(String idOdontologo){
		StringBuilder PacienteResultado= new StringBuilder();
		try{
				
				con=conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("SELECT * FROM odontologo,usuario where idOdontologo=usuario.OdontologoU_idOdontologo and EstadoU_idEstado=1 and idOdontologo="+idOdontologo);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				while(re.next()){
					
					PacienteResultado.append("<input type=\"hidden\" id=\"class\" name=\"class\" value=\"Common.UsuarioCOM\"/>");
					PacienteResultado.append("<input type=\"hidden\" id=\"method\" name=\"method\" value=\"cambiarEstadoUsuario\"/>"); 
					PacienteResultado.append("<input type=\"hidden\" id=\"idodontologo\" name=\"idOdontologo\" value=\""+re.getInt("idOdontologo")+"\" />");
					PacienteResultado.append("<input type=\"hidden\" id=\"idUser\" name=\"idUser\" value=\""+re.getInt("idUsuario")+"\" />");
					PacienteResultado.append("<input type=\"hidden\" id=\"boton\" name=\"boton\" value=\"\" />");
					
				 PacienteResultado.append("<div class=\"panel panel-info\">");
					PacienteResultado.append("<div class=\"panel panel-heading\">");
						PacienteResultado.append("<h4>Nombre:"+re.getString("Onombre1")+" "+re.getString("Onombre2")+" "+re.getString("Oapellido1")+" "+re.getString("Oapellido2")+"</h4>");
					PacienteResultado.append("</div>");
					PacienteResultado.append("<div class=\"panel panel-body\">");
						PacienteResultado.append("<h5>Nombre De Usuario:"+re.getString("Login")+"</h5><br>");
						PacienteResultado.append("<h5>Telefono:"+re.getString("Telefono")+"</h5><br>");
						PacienteResultado.append("<h5>Codigo Medico:"+re.getString("CodigoDoc")+"</h5><br>");
						PacienteResultado.append("<h5>Direccion:"+re.getString("Direccion")+"</h5><br>");
						PacienteResultado.append("<h5>Fecha De Registro:"+re.getString("FechaRegistro")+"</h5><br>");
						PacienteResultado.append("<h5>Correo:"+re.getString("Email")+"</h5>");
					PacienteResultado.append("</div>");
					
					
				 
					
				}
				
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		return PacienteResultado.toString();
	}
	
	
	public boolean usuarioExiste(String login){
		
		
		boolean result=false;
		
		try {
			
			con=conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re;
			re = s.executeQuery("SELECT Login FROM usuario where Login='"+login+"'");
			if(re.next()){
				result=true;
			}else{
				result=false;
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
		//myObj.put("existe", result);
			
		//pr.print(myObj.toString());
		return result;
	}
	
	public String getRolUser(String idrol){
		StringBuilder Resultado= new StringBuilder();
		
		try{
				
				con=conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("SELECT * FROM tipousuario where idTipoUsuario="+idrol);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				while(re.next()){
					Resultado.append(re.getString("Descripcion"));
				}
				
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		return Resultado.toString();
	}
	
	public String getUsersPendientes(String idOdontologo){
		StringBuilder PacienteResultado= new StringBuilder();
		try{
				
				con=java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/swgpo","swgpo","swgpo");
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("SELECT * FROM odontologo,usuario where idOdontologo=usuario.OdontologoU_idOdontologo and EstadoU_idEstado=2 and idOdontologo="+idOdontologo);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				while(re.next()){
					PacienteResultado.append("<form id=\"validarUser\" action=\"ManejarPeticiones\" method=\"get\">");
					PacienteResultado.append("<input type=\"hidden\" id=\"class\" name=\"class\" value=\"Common.UsuarioCOM\"/>");
					PacienteResultado.append("<input type=\"hidden\" id=\"method\" name=\"method\" value=\"cambiarEstadoUsuario\"/>"); 
					PacienteResultado.append("<input type=\"hidden\" id=\"idodontologo\" name=\"idOdontologo\" value=\""+re.getInt("idOdontologo")+"\" />");
					PacienteResultado.append("<input type=\"hidden\" id=\"idUser\" name=\"idUser\" value=\""+re.getInt("idUsuario")+"\" />");
					PacienteResultado.append("<input type=\"hidden\" id=\"boton\" name=\"boton\" value=\"\" />");
					
				 PacienteResultado.append("<div class=\"panel panel-info\">");
					PacienteResultado.append("<div class=\"panel panel-heading\">");
						PacienteResultado.append("<h4>Nombre:"+re.getString("Onombre1")+" "+re.getString("Onombre2")+" "+re.getString("Oapellido1")+" "+re.getString("Oapellido2")+"</h4>");
					PacienteResultado.append("</div>");
					PacienteResultado.append("<div class=\"panel panel-body\">");
						PacienteResultado.append("<h5>Nombre De Usuario:"+re.getString("Login")+"</h5><br>");
						PacienteResultado.append("<h5>Telefono:"+re.getString("Telefono")+"</h5><br>");
						PacienteResultado.append("<h5>Codigo Medico:"+re.getString("CodigoDoc")+"</h5><br>");
						PacienteResultado.append("<h5>Direccion:"+re.getString("Direccion")+"</h5><br>");
						PacienteResultado.append("<h5>Fecha De Registro:"+re.getString("FechaRegistro")+"</h5><br>");
						PacienteResultado.append("<h5>Correo:"+re.getString("Email")+"</h5>");
					PacienteResultado.append("</div>");
					
					PacienteResultado.append("<div class=\"panel panel-footer\" align=\"right\">"+
 					"<button class=\"btn btn-large btn-danger\" name=\"boton1\" type=\"submit\" value=\"rechazar\" >"+
              		 "<span class=\"glyphicon glyphicon-ban-circle\"></span> Rechazar"+
               		"</button>"+
 					"<button class=\"btn btn-large btn-success\" name=\"boton2\" type=\"submit\" value=\"aceptar\" >"+
              		 "<span class=\"glyphicon glyphicon-ok\"></span> Aceptar"+
               		"</button>"+
							"</div>");	
				PacienteResultado.append("</div>");
				 PacienteResultado.append("</form>");
					
				}
				
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		return PacienteResultado.toString();
	}
	
	public String ListUsersPendientes(){
		StringBuilder PacienteResultado= new StringBuilder();
		try{
				
				con=java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/swgpo","swgpo","swgpo");
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("SELECT * FROM odontologo,usuario where idOdontologo=usuario.OdontologoU_idOdontologo and EstadoU_idEstado=2;");
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				PacienteResultado.append("<table class=\"table table-striped\"><th></th>");
				PacienteResultado.append("<thead>");
				PacienteResultado.append("<th>Nombre</th>");
				PacienteResultado.append("<th>Accion</th>");
				PacienteResultado.append("</thead>");
				PacienteResultado.append("<tbody>");
				while(re.next()){
					//PacienteResultado.append("<form action=\"ManejarPeticiones\" method=\"get\">");
					
				PacienteResultado.append("<tr>");
						PacienteResultado.append("<td>"+re.getString("Onombre1")+" "+re.getString("Onombre2")+" "+re.getString("Oapellido1")+" "+re.getString("Oapellido2")+"</td>");
						PacienteResultado.append("<td>");
						PacienteResultado.append("<button class=\"btn btn-primary   rel=\"tooltip\" title=\"Validar solicitud\" id=\"buttonValUser\"");
						PacienteResultado.append(" onclick=\"$(location).attr('href','DetalleOdontologo.jsp?list=2&idOdontologo="+re.getInt("idOdontologo")+"');\" style=\"margin-bottom: 0px;\">");
						PacienteResultado.append("<i class=\"glyphicon glyphicon-ok\"></i> </button>");
						PacienteResultado.append("</td>");
						
				PacienteResultado.append("</tr>");
								
				}
				PacienteResultado.append("</tbody>");
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		return PacienteResultado.toString();
	}
	public void cambiarEstadoUsuario() throws IOException{
		StringBuilder PacienteResultado= new StringBuilder();
		java.sql.PreparedStatement s=null;
		ResultSet re=null;
		Statement st=null;
		Boolean band=false;
		Email em=new Email();
		try{
				
				con=java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/swgpo","swgpo","swgpo");
				String query="update usuario set EstadoU_idEstado=1 where idUsuario=?";
				String query2="update usuario set EstadoU_idEstado=12 where idUsuario=?";
				
				if(super.getRequest().getParameter("boton1")!=null){
					s=con.prepareStatement(query2);
					band=true;
				}else
					s=con.prepareStatement(query);
				
				s.setInt(1, Integer.parseInt(super.getRequest().getParameter("idUser")));
				s.executeUpdate();
				
				
				query="SELECT Email,Login FROM odontologo,usuario where idOdontologo=usuario.OdontologoU_idOdontologo and idOdontologo="+super.getRequest().getParameter("idOdontologo")+";";
				st=con.createStatement();
				re=st.executeQuery(query);
				if(re.next())
					if(em.enviarCorreoActivacionCuenta(re.getString("Email"), re.getString("Login")))
						System.out.println("Correo enviado");
				con.close();	
					if(band)
						super.getResponse().sendRedirect("DetalleOdontologo.jsp?idOdontologo="+super.getRequest().getParameter("idOdontologo")+"&inval=ok");	
					else	
						super.getResponse().sendRedirect("DetalleOdontologo.jsp?idOdontologo="+super.getRequest().getParameter("idOdontologo")+"&val=ok");
				
		}catch(SQLException es){
					es.printStackTrace();
					super.getResponse().sendRedirect("DetalleOdontologo.jsp?idOdontologo="+super.getRequest().getParameter("idOdontologo")+"&val=nok");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
				
		}
	}
	
	public boolean ValidaUser(String id) throws ClassNotFoundException{
		boolean band=true;
		java.sql.PreparedStatement s=null;
		try{
			
			con=conexion.getconexion();
			String query="update usuario set EstadoU_idEstado=1 where idUsuario=?";
			//String query2="update usuario set EstadoU_idEstado=12 where idUsuario=?";
			s=con.prepareStatement(query);
			
			s.setInt(1, Integer.parseInt(id));
			s.executeUpdate();
		}catch(SQLException e){
			band=false;
			e.printStackTrace();
			
		}
		return band;
	}
	
}
