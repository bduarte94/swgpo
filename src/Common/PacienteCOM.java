package Common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.Blob;
//import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.oreilly.servlet.MultipartRequest;

//import org.apache.catalina.connector.Request;
import Peticion.BasePeticiones;
import conexion.Conexion;
import utils.dates;
public class PacienteCOM extends BasePeticiones {
	Conexion Conexion=new Conexion();
	private java.sql.Connection con=null;
		
	public String pacientebyid(String idPaciente){
		StringBuilder PacienteResultado= new StringBuilder();
		try{
			
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select concat(paciente.Pnombre1,\" \",paciente.Papellido1) as nombre from paciente where IdPaciente="+idPaciente);
			if(re.next()){
				PacienteResultado.append(re.getString("nombre"));
			}
		}catch(SQLException es){
			es.printStackTrace();
		}	
		return PacienteResultado.toString();
	}
	
		
	public void buscarPaciente(){
		String busqueda=(String) super.getRequest().getParameter("busqueda");
		String idClinica=(String)super.getRequest().getParameter("idClinica");
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				StringBuilder PacienteResultado= new StringBuilder();
				
		try{
			//System.out.println("idClinica:"+idClinica+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select * from paciente where (Pnombre1 like '"+busqueda+"%' or Papellido1 like '"+busqueda+"%') and idClinica="+idClinica);
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re!=null){
			PacienteResultado.append("<div id=\"result\">");
			PacienteResultado.append("<table id=\"tabla\"class=\"table table-bordered\"><th></th>");
			PacienteResultado.append("<thead>");
			PacienteResultado.append("<th>Foto</th><th>Nombre</th>");
			PacienteResultado.append("<th>Accion</th>");
			PacienteResultado.append("</thead>");
			PacienteResultado.append("<tbody>");
			while(re.next()){
				PacienteResultado.append("<tr>");
				PacienteResultado.append("<td><img class=\"img\" src=\"VerImg?idPac="+re.getInt("IdPaciente")+"\"></td>");

				PacienteResultado.append("<td>"+re.getString("Pnombre1"));
				PacienteResultado.append(" "+re.getString("Pnombre2"));
				PacienteResultado.append(" "+re.getString("Papellido1"));
				PacienteResultado.append(" "+re.getString("Papellido2")+"</td>");
				PacienteResultado.append("<td>");
				PacienteResultado.append("<button class=\"btn btn-primary   rel=\"tooltip\" title=\"Editar Paciente\" id=\"buttonEditPaciente\"");
				PacienteResultado.append(" onclick=\"$(location).attr('href','ActualizarPaciente.jsp?idPaciente="+re.getInt("idPaciente")+"');\" style=\"margin-bottom: 0px;\">");
				PacienteResultado.append("<i class=\"glyphicon glyphicon-edit\"></i> </button>");
				PacienteResultado.append("<button class=\"btn btn-success   rel=\"tooltip\" title=\"Editar Fotografia\" id=\"buttonEditFoto\"");
				PacienteResultado.append(" onclick=\"$(location).attr('href','EditarFoto.jsp?idPaciente="+re.getInt("idPaciente")+"');\" style=\"margin-bottom: 0px;\">");
				PacienteResultado.append("<i class=\"glyphicon glyphicon-picture\"></i> </button>");
				PacienteResultado.append("<button class=\"btn btn-info   rel=\"tooltip\" title=\"Ver citas Programadas\" id=\"buttonCitas\"");
				PacienteResultado.append(" onclick=\"$(location).attr('href','CitasPaciente.jsp?idPaciente="+re.getInt("idPaciente")+"');\" style=\"margin-bottom: 0px;\">");
				PacienteResultado.append("<i class=\"glyphicon glyphicon-calendar\"></i> </button>");
				
				PacienteResultado.append("<button class=\"btn btn-info   rel=\"tooltip\" title=\"Odontograma\" id=\"buttonOdontograma\"");
				PacienteResultado.append(" onclick=\"$(location).attr('href','OdontogramaPaciente.jsp?idPaciente="+re.getInt("idPaciente")+"');\" style=\"margin-bottom: 0px;\">");
				PacienteResultado.append("<i class=\"fa fa-stethoscope\"></i> </button>");
				
				
				PacienteResultado.append("</td>");
				PacienteResultado.append("</tr>");
			}
			PacienteResultado.append("</tbody>");
			con.close();
			}else
				PacienteResultado.append("<div class=\"alert alert-info\"><strong>Atencion!</strong> No hay informacion que mostrar.</div>");
		}catch(SQLException es){
			es.printStackTrace();
		}
		PacienteResultado.append("</table>");
		System.out.println(PacienteResultado.toString()+"*******************");
		pr.write(PacienteResultado.toString());
		//return PacienteResultado.toString();
		
	}
	
	public String buscarPacienteForCita(String idClinica){
		StringBuilder PacienteResultado= new StringBuilder();
		try{
				
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("select * from paciente where idClinica="+idClinica);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				PacienteResultado.append("<select name=\"idPaciente\" id=\"combobox\" class=\"chosen-select\" >");
				PacienteResultado.append("<option value=\"\" >Seleccionar...</option>");
				while(re.next()){
					PacienteResultado.append("<option value=\""+re.getInt("idPaciente")+"\">");
					PacienteResultado.append(re.getString("Pnombre1"));
					PacienteResultado.append(" "+re.getString("Pnombre2"));
					PacienteResultado.append(" "+re.getString("Papellido1"));
					PacienteResultado.append(" "+re.getString("Papellido2")+"</option>");
										
				}
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		PacienteResultado.append("</select>");
		return PacienteResultado.toString();
}

	
	public void ListaPaciente(){
		String idClinica = super.getRequest().getParameter("idClinica");
		StringBuilder PacienteResultado= new StringBuilder();
		PrintWriter pr=null;
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try{
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("select IdPaciente,Pnombre1,Pnombre2,Papellido1,Papellido2,estado.Nombre from estado,paciente where idClinica="+idClinica+" and idEstado=Estado_idEstado");
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				PacienteResultado.append("<table class=\"table table-bordered\"><th></th>");
				PacienteResultado.append("<thead>");
				PacienteResultado.append("<th>Foto</th><th>Nombre</th>");
				PacienteResultado.append("<th>Estado</th>");
				PacienteResultado.append("<th>Accion</th>");
				PacienteResultado.append("</thead>");
				PacienteResultado.append("<tbody>");
				//HttpSession sesion= getRequest().getSession(true);
				while(re.next()){
					//sesion.setAttribute("idPac", re.getInt("IdPaciente"));
					//PacienteResultado.append("<%>session.setAttribute(\"idPac\", "+re.getInt("IdPaciente")+");<%>");
					PacienteResultado.append("<tr>");
					PacienteResultado.append("<td><img class=\"img\" src=\"VerImg?idPac="+re.getInt("IdPaciente")+"\"></td>");
					PacienteResultado.append("<td>"+re.getString("Pnombre1"));
					PacienteResultado.append(" "+re.getString("Pnombre2"));
					PacienteResultado.append(" "+re.getString("Papellido1"));
					PacienteResultado.append(" "+re.getString("Papellido2")+"</td>");
					PacienteResultado.append("<td>"+re.getString("Nombre")+"</td>");
					PacienteResultado.append("<td>");
					PacienteResultado.append("<button class=\"btn btn-primary   rel=\"tooltip\" title=\"Editar Paciente\" id=\"buttonEditPaciente\"");
					PacienteResultado.append(" onclick=\"$(location).attr('href','ActualizarPaciente.jsp?idPaciente="+re.getInt("IdPaciente")+"');\" style=\"margin-bottom: 0px;\">");
					PacienteResultado.append("<i class=\"glyphicon glyphicon-edit\"></i> </button>");
					PacienteResultado.append("<button class=\"btn btn-success   rel=\"tooltip\" title=\"Editar Fotografia\" id=\"buttonEditFoto\"");
					PacienteResultado.append(" onclick=\"$(location).attr('href','EditarFoto.jsp?idPaciente="+re.getInt("IdPaciente")+"');\" style=\"margin-bottom: 0px;\">");
					PacienteResultado.append("<i class=\"glyphicon glyphicon-picture\"></i> </button>");
					PacienteResultado.append("<button class=\"btn btn-info   rel=\"tooltip\" title=\"Ver citas Programadas\" id=\"buttonCitas\"");
					PacienteResultado.append(" onclick=\"$(location).attr('href','CitasPaciente.jsp?idPaciente="+re.getInt("IdPaciente")+"');\" style=\"margin-bottom: 0px;\">");
					PacienteResultado.append("<i class=\"glyphicon glyphicon-calendar\"></i> </button>");

					PacienteResultado.append("<button class=\"btn btn-info   rel=\"tooltip\" title=\"Odontograma\" id=\"buttonOdontograma\"");
					PacienteResultado.append(" onclick=\"$(location).attr('href','OdontogramaPaciente.jsp?idPaciente="+re.getInt("idPaciente")+"');\" style=\"margin-bottom: 0px;\">");
					PacienteResultado.append("<i class=\"fa fa-stethoscope\"></i> </button>");
					
					PacienteResultado.append("</td>");
					PacienteResultado.append("</tr>");
				}
				PacienteResultado.append("</tbody>");
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		PacienteResultado.append("</select>");
		pr.write( PacienteResultado.toString());
		
	}
	
	
	
	public String ListaPacienteCombobox(String idClinica){
		StringBuilder PacienteResultado= new StringBuilder();
		
		try{
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				//System.out.println("idClinica"+idClinica);
				ResultSet re=s.executeQuery("select IdPaciente,Pnombre1,Pnombre2,Papellido1,Papellido2 from paciente where idClinica="+idClinica+" and Estado_idEstado=1");
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				//HttpSession sesion= getRequest().getSession(true);
				while(re.next()){
					//sesion.setAttribute("idPac", re.getInt("IdPaciente"));
					//PacienteResultado.append("<%>session.setAttribute(\"idPac\", "+re.getInt("IdPaciente")+");<%>");
					
					PacienteResultado.append("<option data-img-src=\"VerImg?idPac="+re.getInt("IdPaciente")+"\" value='"+re.getInt("IdPaciente")+"'> ");
					PacienteResultado.append("<img src=\"VerImg?idPac="+re.getInt("IdPaciente")+"\" ></img>");
					PacienteResultado.append(re.getString("Pnombre1"));
					PacienteResultado.append(" "+re.getString("Pnombre2"));
					PacienteResultado.append(" "+re.getString("Papellido1"));
					PacienteResultado.append(" "+re.getString("Papellido2"));
					PacienteResultado.append("</option>");
					
				}
				
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}

		return PacienteResultado.toString();
		
	}
	
	public void ajaxinfopaciente(){
		
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		StringBuilder PacienteResultado= new StringBuilder();
		PrintWriter pr=null;
		dates dates=new dates();
		
		try{
				pr=super.getResponse().getWriter();
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("select * from paciente where IdPaciente="+idPaciente);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				//HttpSession sesion= getRequest().getSession(true);
				
				while(re.next()){
					
					PacienteResultado.append("<div class=\"w3-border w3-padding w3-margin-top\"><div class=\"media\">");
					PacienteResultado.append("<div id=\"pic\" class=\"media-left media-middle\">");
					PacienteResultado.append("<img src=\"VerImg?idPac="+re.getInt("IdPaciente")+"\" class=\"media-object\" style=\"width:60px;margin-top:8px\">");
					PacienteResultado.append("</div><div id=\"cont\" class=\"media-body\">");					
					PacienteResultado.append("<h4 class=\"media-heading\">");
					PacienteResultado.append(re.getString("Pnombre1"));
					PacienteResultado.append(" "+re.getString("Pnombre2"));
					PacienteResultado.append(" "+re.getString("Papellido1"));
					PacienteResultado.append(" "+re.getString("Papellido2"));
					PacienteResultado.append("</h4>");
					PacienteResultado.append("<p>Direccion:"+re.getString("Direccion")
							+ "<br>Movil:"+re.getString("movil")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sexo:"+re.getString("Sexo")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Grupo Sanguineo:"+re.getString("GrupoSanguineo")
							+ "<br>Fecha Nacimiento:"+re.getString("FechaNac")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edad:"+dates.CalculateAge(re.getDate("FechaNac"))
							+ "</p>");
					PacienteResultado.append("</div></div></div>");
					
				}				
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print(PacienteResultado.toString());
		 pr.write(PacienteResultado.toString());
		
	}
	
		
	public byte[] obtenImagenPac(int idpac)  {
		ResultSet re=null;
        byte[] buffer = null;
        try {
        	
        	con=Conexion.getconexion();
        	//Statement s=con.createStatement();
        	PreparedStatement stmt = con.prepareStatement("select foto from foto where PacienteF_idPaciente='"+idpac+"'");
        	re=stmt.executeQuery();
			
           // String sql = "select ImgCedula from Item_Radicado where Cod_Item_Radicado= '"+Item+"'";
            
            
            while (re.next()){
                Blob bin = re.getBlob("foto");
               
                if (bin != null) {
                	 InputStream inStream = bin.getBinaryStream();
                    int size = (int) bin.length();
                    buffer = new byte[size];
                    //int length = -1;
                    //int k = 0;
                    System.out.print("Dentro de bin!=null "+bin.length());
                    try {
                        inStream.read(buffer, 0, size);
                        System.out.print("Dentro del metodo "+buffer.length);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        finally {
            
            re = null;
            
        }
        //System.out.print("Dentro del metodo "+buffer.length);
        return buffer;
    }
 
	public void registrarCita(){
		//StringBuilder PacienteResultado= new StringBuilder();
		//String time=super.getRequest().getParameter("hora");
		//String time2=super.getRequest().getParameter("hora2");
		//Date date = null;
		//Date date2 = null;
		String sal="";
		try{
			con=Conexion.getconexion();
				Statement s=con.createStatement();
				sal="insert into cita(PacienteC_idPaciente,OdontologoC_idOdontologo,start_date,text,EstadoC_idEstado,end_date)"
						+ " values("+super.getRequest().getParameter("idPaciente")+","+super.getRequest().getParameter("idOdontologo")
								+ ",'"+super.getRequest().getParameter("hora")+"','"+super.getRequest().getParameter("descripcion")+"',4,'"+super.getRequest().getParameter("hora2")+"')";
				
				//System.out.print(sal);
				s.executeUpdate(sal);
				con.close();
				super.getResponse().sendRedirect("Inicio.jsp");
		}catch(SQLException es){
			/*try {
				super.getResponse().sendRedirect("Inicio.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			es.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	public String Listacitas(String idPaciente,String idClinica){
		StringBuilder PacienteResultado= new StringBuilder();
		//String time=super.getRequest().getParameter("hora");
		//Date date = null;
		ResultSet re=null;
		try{
			//try {
				//date = new SimpleDateFormat("HH:mm").parse(time);
			//} catch (ParseException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			//}
				
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				re=s.executeQuery("SELECT idCita,Fecha,start_date,text,estado.Nombre,concat(paciente.Pnombre1,\" \",paciente.Papellido1) as paciente FROM cita,estado,paciente "+ 
						"where paciente.IdPaciente=PacienteC_idPaciente and PacienteC_idPaciente="+idPaciente+" and estado.idEstado=EstadoC_idEstado order by Fecha desc");
				PacienteResultado.append("<h2>Citas del paciente "+pacientebyid(idPaciente)+"</h2>");
				
				if(re.next()){
				PacienteResultado.append("<table class=\"table table-striped\"><th></th>");
				PacienteResultado.append("<thead>");
				PacienteResultado.append("<th>FechaReg</th>");
				PacienteResultado.append("<th>Fecha</th>");
				PacienteResultado.append("<th>Descripcion</th>");
				PacienteResultado.append("<th>Estado</th>");
				PacienteResultado.append("<th>Configurar</th>");
				PacienteResultado.append("</thead>");
				PacienteResultado.append("<tbody>");
				re.beforeFirst();
				while(re.next()){
					PacienteResultado.append("<tr>");
					PacienteResultado.append("<td>"+re.getString("Fecha")+"</td>");
					PacienteResultado.append("<td>"+re.getString("start_date")+"</td>");
					PacienteResultado.append("<td>"+re.getString("text")+"</td>");
					PacienteResultado.append("<td>"+re.getString("Nombre")+"</td>");
					PacienteResultado.append("<td></td>");
					PacienteResultado.append("</tr>");
				}PacienteResultado.append("</tbody>");
				}else{
					PacienteResultado.append("<div class=\"alert alert-info\"> no hay citas registradas<div>");
					
					
				}
				
				con.close();
					//super.getResponse().sendRedirect("Inicio.jsp");
		}catch(SQLException es){
			es.printStackTrace();
		}
		

		return PacienteResultado.toString();
	}
	
	public String DetallesPaciente(String idClinica,String idPaciente){
		StringBuilder PacienteResultado= new StringBuilder();
		int idPac=0,Estado=0;
		String nom1 = "",nom2 = "",ape1 = "",ape2="",tel="",mov="",companySeg="",NumSeg="",grupoSang="",FechaNac="",nTar="",numPol="",obs="",email="",ciudad="",sexo="",dir="";
		try{
				
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("select * from paciente where idClinica="+idClinica+" and idPaciente="+idPaciente);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				while(re.next()){
					idPac=re.getInt("idPaciente");
					nom1=re.getString("Pnombre1");
					nom2=re.getString("Pnombre2");
					ape1=re.getString("Papellido1");
					ape2=re.getString("Papellido2");
					tel=re.getString("telefono");
					mov=re.getString("movil");
					companySeg=re.getString("companySeguro");
					NumSeg=re.getString("NumeroSeguro");
					grupoSang=re.getString("GrupoSanguineo");
					FechaNac=re.getString("FechaNac");
					nTar=re.getString("NTarjeta");
					numPol=re.getString("Npoliza");
					obs=re.getString("Observacion");
					email=re.getString("Email");
					ciudad=re.getString("Ciudad");
					sexo=re.getString("Sexo");
					dir=re.getString("Direccion");
					Estado=re.getInt("Estado_idEstado");
				}
				con.close();
				PacienteResultado.append("<div class=\"row\">"+
	 		
	 		"<div class=\"col-sm-9\">"+
	 		"<div class=\"panel panel-primary\" align=\"Center\">"+
	 			"<div class=\"panel panel-heading\">"+
	 				"<h4>Datos Generales Del Paciente</h4>"+
	 			"</div>"+
	 			
	 				"<div class=\"row\">"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>Nombre:</label>"+
	 					"</div>"+
	 					"<div class=\"col-sm-3\">"+
	 						"<input type=\"text\" class=\"form-control\" name=\"txtNomPac\" value=\""+nom1+"\" >"+
	 					"</div>"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>2do Nombre:</label>"+
	 					"</div>"+
	 					"<div class=\"col-sm-3\">"+
	 						"<input type=\"text\" class=\"form-control\" name=\"txtNomPac2\" value=\""+nom2+"\">"+
	 					"</div>"+
	 					
	 						
	 				"</div><br>"+
	 				"<div class=\"row\">"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>Apellido</label>"+
	 					"</div>"+
	 					"<div class=\"col-sm-3\">"+
	 					"<input type=\"text\"class=\"form-control\" name=\"txtApelPac\" value=\""+ape1+"\" >"+
	 					"</div>"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>2do Apellido:</label>"+
	 					"</div>"+
	 					"<div class=\"col-sm-3\">"+
	 						"<input type=\"text\"class=\"form-control\" name=\"txtApelPac2\" value=\""+ape2+"\">"+
	 					"</div>"+
	 					
	 				"</div><br>"+
	 				"<div class=\"row\">"+
	 					"<div class=\"col-xs-2 col-centered\">Sexo:</div>"+
           		   			"<div class=\"col-xs-3 col-centered\">"+ 
           		   				"<select class=\"form-control\" name=\"sexo\">"+
 								 "<option "+(sexo.compareTo("Femenino")==0?"selected":"")+" value=\"Femenino\">Femenino</option>"+
 								 "<option "+(sexo.compareTo("Masculino")==0?"selected":"")+" value=\"Masculino\">Masculino</option>"+
	 							"</select>"+
	 						"</div>"+
	 						
	 						"<div class=\"col-sm-2\">"+
	 						"<label>Fecha de Nacimiento:</label>"+
	 						"</div>"+
	 					"<div class='col-sm-3'>"+
												
                				"<div class=\"form-group\">"+
                			"<div class='input-group date' id='fechaNac'>"+
                   				 "<input name=\"fechaNac\" type='text' class=\"form-control\" value=\""+FechaNac+"\"/>"+
                    			"<span class=\"input-group-addon\">"+
                        		"<span class=\"glyphicon glyphicon-calendar\"></span>"+
                    			"</span>"+
                			"</div>"+
            			"</div>"+
            				
        					"</div>"+
        				"</div>"+
        				"<br>"+
        				"<div class=\"row\">"+
        					"<div class=\"col-sm-2\">"+
	 						"<label>Telefono</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"number\" class=\"form-control\" name=\"txtTel\" value=\""+tel+"\">"+
	 						"</div>"+
	 					   
	 					   "<div class=\"col-sm-2\">"+
	 						"<label>Movil</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"number\" class=\"form-control\" name=\"txtMov\" value=\""+mov+"\">"+
	 						"</div>"+
	 					 
	 					    				
        				"</div><br>"+
        				"<div class=\"row\">"+
        					"<div class=\"col-sm-2\">"+
	 						"<label>Compania Seguro</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"text\" class=\"form-control\" name=\"txtseguro\" value=\""+companySeg+"\">"+
	 						"</div>"+
	 						"<div class=\"col-sm-2\">"+
	 						"<label>N Seguro</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"number\" class=\"form-control\" name=\"txtNseguro\" value=\""+NumSeg+"\">"+
	 						"</div>"+
	 											   
        				
        				"</div>"+
	 					
	 				"<br>"+
	 				"<div class=\"row\">"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>N Tarjeta</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"number\" class=\"form-control\" name=\"txtTarjeta\" value=\""+nTar+"\">"+
	 						"</div>"+
	 						"<div class=\"col-sm-2\">"+
	 						"<label>Email</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"email\" class=\"form-control\" name=\"txtEmail\" value=\""+email+"\">"+
	 						"</div>"+
	 				"</div><br>"+
	 				"<div class=\"row\">"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>Direccion</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-5\">"+
	 						"<input type=\"text\" class=\"form-control\" name=\"txtDireccion\" value=\""+dir+"\">"+
	 						"</div>"+
	 						"<div class=\"col-sm-1\">"+
	 						"<label>Ciudad</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-2\">"+
	 						"<select class=\"form-control\" name=\"ciudad\">"+
 								 "<option "+(ciudad.compareTo("Managua")==0?"selected":"")+" value=\"Managua\">Managua</option>"+
 								 "<option "+(ciudad.compareTo("Masaya")==0?"selected":"")+" value=\"Masaya\">Masaya</option>"+
 								 "<option "+(ciudad.compareTo("Leon")==0?"selected":"")+" value=\"Leon\">Leon</option>"+
 								 "<option "+(ciudad.compareTo("Chinandega")==0?"selected":"")+" value=\"Chinandega\">Chinandega</option>"+
 								 "<option "+(ciudad.compareTo("Esteli")==0?"selected":"")+" value=\"Esteli\">Esteli</option>"+
 								 "<option "+(ciudad.compareTo("Carazo")==0?"selected":"")+" value=\"Carazo\">Carazo</option>"+
 								 "<option "+(ciudad.compareTo("Rivas")==0?"selected":"")+" value=\"Rivas\">Rivas</option>"+
 								 "<option "+(ciudad.compareTo("Rio San Juan")==0?"selected":"")+" value=\"Rio San Juan\">Rio San Juan</option>"+
 								 "<option "+(ciudad.compareTo("Chontales")==0?"selected":"")+" value=\"Chontales\">Chontales</option>"+
 								 "<option "+(ciudad.compareTo("Boaco")==0?"selected":"")+" value=\"Boaco\">Boaco</option>"+
 								 "<option "+(ciudad.compareTo("Jinotega")==0?"selected":"")+" value=\"Jinotega\">Jinotega</option>"+
	 							"</select>"+
	 						"</div>"+
	 				"</div><br>"+
	 				"<div class=\"row\">"+
	 					   "<div class=\"col-sm-2\">"+
	 						"<label>Grupo Sanguineo</label>"+
	 					"</div>"+
	 					"<div class=\"col-sm-2\">"+
	 						"<select class=\"form-control\" name=\"txtTipoSangre\">"+
 								 "<option "+(grupoSang.compareTo("A")==0?"selected":"")+" value=\"A\">A</option>"+
 								 "<option "+(grupoSang.compareTo("B")==0?"selected":"")+" value=\"B\">B</option>"+
 								 "<option "+(grupoSang.compareTo("AB")==0?"selected":"")+" value=\"AB\">AB</option>"+
 								 "<option "+(grupoSang.compareTo("O")==0?"selected":"")+" value=\"O\">O</option>"+
	 							"</select>"+
	 					"</div>"+
	 					"<div class=\"col-sm-2\">"+
	 						"<label>N Poliza</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-3\">"+
	 						"<input type=\"number\" class=\"form-control\" name=\"txtPoliza\" value=\""+numPol+"\">"+
	 					"</div>"+
	 				"</div><br>"+	 				
	 				"<div class=\"row\">"+
	 						"<div class=\"col-sm-2\">"+
	 						"<label>Observaciones</label>"+
	 						"</div>"+
	 						"<div class=\"col-sm-8\">"+
	 						"<input type=\"text\" class=\"form-control\" name=\"txtObservacion\" value=\""+obs+"\">"+
	 						"</div>"+
	 						
	 				"</div><br>"+
	 				"<div class=\"col-sm-2\">"+
						"<label>Estado</label>"+
					"</div>"+
	 				"<div class=\"row\">"+
	 				"<div class=\"col-sm-3\">"+
						"<select class=\"form-control\" name=\"txtEstado\">"+
							 "<option "+(Estado==1?"selected":"")+" value=\"1\">Activo</option>"+
							 "<option "+(Estado==3?"selected":"")+" value=\"3\">Inactivo</option>"+
						"</select>"+
					"</div>"+
					"</div><br>"+
	 				"<div class=\"row\">"+
	 					"<div class=\"col-sm-12\" >"+
	 						"<ul id=\"mytab\" class=\"nav nav-tabs nav-justified\">"+
  								"<li class=\"active\">"+"<a data-toggle=\"tab\" href=\"#antecPer\">Ant. Personales</a></li>"+
  								"<li>"+"<a data-toggle=\"tab\" href=\"#antecFam\">Ant. Familiares</a></li>"+
  								"<li>"+"<a data-toggle=\"tab\" href=\"#alergia\">Alergias</a></li>"+
  								"<li>"+"<a data-toggle=\"tab\" href=\"#vac\">Vacunas</a></li>"+
  								"<li>"+"<a data-toggle=\"tab\" href=\"#trat\">Trat. Habitual</a></li>"+
  								"<li>"+"<a data-toggle=\"tab\" href=\"#cir\">Cirujias</a></li>"+
  								"<li>"+"<a data-toggle=\"tab\" href=\"#enfer\">Enfer. Cronicas</a></li>"+
							"</ul>"+
							"<div class=\"tab-content\">"+

    							"<div id=\"antecPer\" class=\"tab-pane fade in active\">"+
    							"<textarea name=\"antPer\" placeholder=\"Ingrese antecedentes personales\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+

    							"<div id=\"antecFam\" class=\"tab-pane fade\">"+
								"<textarea name=\"antFam\" placeholder=\"Ingrese antecedentes Familiares\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+
    							
    							"<div id=\"alergia\" class=\"tab-pane fade\">"+
								"<textarea name=\"Ale\" placeholder=\"Ingrese alergias\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+
    							
    							"<div id=\"vac\" class=\"tab-pane fade\">"+
								"<textarea name=\"VAC\" placeholder=\"Ingrese vacunas\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+
    							
    							"<div id=\"trat\" class=\"tab-pane fade\">"+
								"<textarea name=\"TRAT\" placeholder=\"Ingrese tratamientos habituales\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+
    							"<div id=\"cir\" class=\"tab-pane fade\">"+
								"<textarea name=\"CIR\" placeholder=\"Ingrese cirugias\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+
    							"<div id=\"enfer\" class=\"tab-pane fade\">"+
								"<textarea name=\"ENFER\" placeholder=\"Ingrese enfermedades cr�nicas\" class=\"form-control\" rows=\"3\" maxlength=\"100\" cols=\"50\"></textarea>"+
    							"</div>"+
    						
    						"</div>"+
							
	 					"</div>"+
	 						
	 				"</div><br>");
				PacienteResultado.append("<script type=\"text/javascript\">"+
       					     "$(function () {"+
                				"$('#fechaNac').datetimepicker({"+
       					     	"defaultDate: '"+FechaNac+"',"+
                		 		"format: 'YYYY-MM-DD',"+
                		 		"viewMode: 'years'"+
               				 	"});"+
                
           					 "});"+
           					 "</script>	");
				PacienteResultado.append("</div><!--Final primer panel \"datos generales del paciente\"-->"+
						"</div>"+	
						"<div class=\"col-sm-3\">"+
				 		"<div class=\"panel panel-primary\" align=\"Center\">"+
				 			"<div class=\"panel panel-heading\">"+
				 			"<h4>Foto De Paciente</h4>"+
				 			"</div>"+
				 			"<div class=\"panel panel-body\">"+
				 			"<div class=\"row\">"+
				 			"<img id=\"vistaPrevia\" class=\"img-responsive\" alt=\"\" width=\"128px\" height=\"128px\" src=\"VerImg?idPac="+idPac+"\">"+
							"</div>"+
							"</div>"+
							"<div id='botonera' class=\"panel panel-footer\">"+
							"<span id=\"infoNombre\" hidden=\"true\">[Seleccione una imagen]</span><br/>"+
			            	"<span id=\"infoTamano\" hidden=\"true\"></span>"+
				 			" <input id=\"foto\" type=\"file\" name=\"foto\" class=\"file\" placeholder=\"Subir foto\" accept=\"image/*\">"+
							"</div>"+
						"</div><br>"+
							
												
						"</div>"+
						
					"</div>");
				
		}catch(SQLException es){
			es.printStackTrace();
		}
		return PacienteResultado.toString();
	}

	public String obtenImagenPaciente(String idPaciente)  {      
        
        ResultSet re;
        String sal="";
        Blob bin=null;
        try {
           
        	
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			String Query="select * from foto where PacienteF_idPaciente="+idPaciente;
			System.out.print("**********"+Query); 
			re=s.executeQuery(Query);
			
            while (re.next()){
                bin = re.getBlob(3);
                sal=re.getString("FotoName");
                System.out.print("**********"+bin.length()+sal); 
            }
            InputStream inStream=null;
            FileOutputStream fileOut=null;
            if (bin != null){
                inStream = bin.getBinaryStream();
                fileOut = new FileOutputStream ("c:\\prueba\\"+sal);
                   
		         byte bytes[] = new byte[1024];
		         int leidos = inStream.read(bytes);
		         while (leidos > 0) {
		            fileOut.write(bytes);
		            leidos = inStream.read(bytes);
		         }
		         inStream.close();
		         fileOut.close();
            }         
                 } catch (IOException ex) {
            ex.printStackTrace();
        		
	}catch(SQLException es){
		es.printStackTrace();
	}
	
        
        finally {
        	con=null;
            re = null;
            
        }
         
        
        
        return "<img src=\"c:\\prueba\\"+sal+"\">";
    }


}
