package Common;

import static java.lang.System.out;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import org.json.simple.JSONObject;

import Dao.Email;
import Peticion.BasePeticiones;
import conexion.Conexion;

public class OdontologoCOM extends BasePeticiones {
	Conexion conexion=new Conexion();
	private java.sql.Connection con=null;
	
	public String ListOdontologo(String idClinica){
		StringBuilder odo= new StringBuilder();
		try{
				
				con=conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("SELECT * FROM odontologo,usuario where idOdontologo=usuario.OdontologoU_idOdontologo and EstadoU_idEstado=1 and idClinica="+idClinica);
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				odo.append("<select id=\"idOdontologo\" name=\"idOdontologo\">");
				while(re.next()){
					//odo.append("<form action=\"ManejarPeticiones\" method=\"get\">");
						odo.append("<option value='"+re.getInt("idOdontologo")+"'>");
						odo.append(re.getString("Onombre1")+re.getString("Oapellido1"));					
						odo.append("</option>");								
				}
				odo.append("</select>");
				con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		return odo.toString();
	}
	
	@SuppressWarnings("unchecked")
	public void registrarOdontologo(){
			super.getResponse().setContentType("application/json");
		 	String idClinica=super.getRequest().getParameter("idClinica");
		 	String nombre=super.getRequest().getParameter("txtnom");
			String nombre2=super.getRequest().getParameter("txtnom2");
			String apellido=super.getRequest().getParameter("txtapel");
			String apellido2=super.getRequest().getParameter("txtapel2");
			String direccion=super.getRequest().getParameter("txtdir");
			String tel=super.getRequest().getParameter("txtTel");
			//String CodigoMinsa=request.getParameter("txtCodigoMinsa");
			//String fecha=request.getParameter("fecha");
			String login=super.getRequest().getParameter("txtUsr");
			String pass=super.getRequest().getParameter("txtPass");
			String correo=super.getRequest().getParameter("txtemail");
			String query="";
			int iduser=0;
			java.sql.PreparedStatement stmt1=null;
			java.sql.PreparedStatement stmt2=null;
			PrintWriter pr=null;
			try {
				pr=super.getResponse().getWriter();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			JSONObject myObj = new JSONObject();
			UsuarioCOM us=new UsuarioCOM();
			boolean result=us.usuarioExiste(login);
			if(!result){
				myObj.put("existe", result);
			 try{
				 	//conex=(Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/swgpo","swgpo","swgpo");
						con=conexion.getconexion();
						 //sql=con.createStatement();
						 
						 con.setAutoCommit(false);
						Statement s=con.createStatement();
						query="INSERT INTO usuario(Login,Password,TipoU_idTipoUser,EstadoU_idEstado,Email) values(?,?,?,?,?)";
						stmt1 =con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
						stmt1.setString(1, login);
						stmt1.setString(2, pass);
						stmt1.setInt(3, 2);
						stmt1.setInt(4, 1);//estado activo
						stmt1.setString(5, correo);
						iduser=stmt1.executeUpdate();
						
						query="INSERT INTO odontologo(Onombre1,Onombre2,Oapellido1,Oapellido2,Direccion,"+
									 "Telefono,idEstado,idUsuario,idClinica) VALUES (?,?,?,?,?,?,?,?,?)";
						stmt2 =con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
						
						stmt2.setString(1, nombre);
						stmt2.setString(2, nombre2);
						stmt2.setString(3, apellido);
						stmt2.setString(4, apellido2);
						stmt2.setString(5, direccion);
						stmt2.setString(6, tel);
						stmt2.setInt(7, 1);
						stmt2.setInt(8, iduser);
						stmt2.setString(9, idClinica);
						stmt2.executeUpdate();
						
	
						//se envia correo electronico al usuario
						Email email = new Email();
						String de = correo;
						//String clave = request.getParameter("txtContrasena");
						ClinicaCOM cl=new ClinicaCOM();
						boolean resultado =email.enviarCorreoCredencialesOdontologo(de,nombre+" "+apellido,login,pass,cl.getNombreclinica(idClinica));
						
						if (resultado){
							 con.commit();
							 //super.getResponse().sendRedirect("Registro.jsp?ok=1");
							 myObj.put("ok", 1);
						}
						else{
							con.rollback();
							//super.getResponse().sendRedirect("Registro.jsp?ok=0");
							 myObj.put("ok", 0);
						}
						
						
			 }catch(Exception e){
					e.printStackTrace();//JOptionPane.showMessageDialog(null, "Error");
					try {
						con.rollback();
						myObj.put("ok", 0);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			 }
	}else{
		myObj.put("existe", result);
	}
			
				
			pr.print(myObj.toString());

	}
	
}
