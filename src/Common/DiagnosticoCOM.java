package Common;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Peticion.BasePeticiones;
import conexion.Conexion;

public class DiagnosticoCOM extends BasePeticiones {
	Conexion Conexion=new Conexion();
	private java.sql.Connection con=null;
	
	public String GetDiagnosticoSitio(String idSitio){
		//String idSitio=(String)super.getRequest().getParameter("idSitio");
		//String idEstado=(String)super.getRequest().getParameter("idEstado");
		String Diagnostico="";
		/*PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		try{
			//System.out.println("idodontologo:"+idOdontologo+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			String query1="SELECT * FROM diagnostico_sitio where idDiagnostico_Sitio="+idSitio;
			//String query2="SELECT * FROM Diagnostico_Estado where idDiagnostico_Estado="+idEstado;			
			ResultSet re=s.executeQuery(query1);
			if(re!=null){
				if(re.next()){
					Diagnostico="Sitio: "+re.getString("Descripcion");
				}
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			}
		
		//System.out.println(Servicios.toString()+"*******************");
		return Diagnostico;
		//return Servicios.toString();
	}
	
	public String GetDiagnosticoEstado(String idEstado){
		//String idSitio=(String)super.getRequest().getParameter("idSitio");
		//String idEstado=(String)super.getRequest().getParameter("idEstado");
		String Diagnostico="";
		/*PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		try{
			//System.out.println("idodontologo:"+idOdontologo+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			//String query1="SELECT * FROM Diagnostico_Sitio where idDiagnostico_Sitio="+idSitio;
			String query2="SELECT * FROM diagnostico_estado where idDiagnostico_Estado="+idEstado;			
			ResultSet re=s.executeQuery(query2);
			if(re!=null){
				if(re.next()){
					Diagnostico+="Estado: "+re.getString("Descripcion");
				}
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			}
		
		//System.out.println(Servicios.toString()+"*******************");
		return Diagnostico;
		//return Servicios.toString();
	}
	
	

}
