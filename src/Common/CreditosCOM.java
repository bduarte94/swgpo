package Common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import Peticion.BasePeticiones;
import conexion.Conexion;

public class CreditosCOM extends BasePeticiones {
	Conexion Conexion = new Conexion();
	private java.sql.Connection con = null;
	
	public void getCreditos(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				StringBuilder CreditosResultado= new StringBuilder();
				
		try{
			//System.out.println("idClinica:"+idClinica+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT idcredito,concat(idTipoDocumento,'-',idcredito) as codCred,Descripcion,FechaRegistro,Limite,Plazo,Nombre "+ 
										"FROM credito "+ 
										"INNER JOIN estado ON credito.idEstado_cre=estado.idEstado "+ 
										"WHERE idPaciente_cre="+idPaciente+" "
										+ "order by FechaRegistro desc;");
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re.first()){
				re.beforeFirst();
				while(re.next()){
					CreditosResultado.append("<tr class='rowcre' data-id='"+re.getInt("idcredito")+"' title=\"Ver detalle crédito.\">");
					CreditosResultado.append("<td>"+re.getString("codCred")+"</td>");
					CreditosResultado.append("<td>"+re.getString("Descripcion")+"</td>");
					CreditosResultado.append("<td>"+re.getString("FechaRegistro")+"</td>");
					CreditosResultado.append("<td>"+re.getFloat("Limite")+"</td>");
					CreditosResultado.append("<td>"+re.getInt("Plazo")+"</td>");
					CreditosResultado.append("<td>"+re.getString("Nombre")+"</td>");
					CreditosResultado.append("</tr>");
				}
				
			con.close();
			}else
				CreditosResultado.append("<div class=\"alert alert-info\"><strong>Atencion!</strong> No hay informacion que mostrar.</div>");
		}catch(SQLException es){
			es.printStackTrace();
		}
		//System.out.println(CreditosResultado.toString()+"*******************");
		pr.write(CreditosResultado.toString());
		//return PacienteResultado.toString();
	}
	
	
	public void AjaxSaveCredito(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		String limite=(String)super.getRequest().getParameter("limite");
		String plazo=(String)super.getRequest().getParameter("plazo");
		String descripcion=(String)super.getRequest().getParameter("descripcion");
			super.getResponse().setContentType("text/html;charset=UTF-8");
		    try {
				super.getRequest().setCharacterEncoding("UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			java.sql.PreparedStatement stmt1=null;
			con=Conexion.getconexion();
			
			
			try {
				con.setAutoCommit(false);
				//Statement s=con.createStatement();
				String sql1 ="Insert into credito (idPaciente_cre,Limite,Plazo,idEstado_cre,idTipoDocumento,Descripcion)"
						+ " values(?,?,?,?,?,?)";
				
					stmt1 =con.prepareStatement(sql1);
					
				
					stmt1.setInt(1, Integer.parseInt(idPaciente)); 
					stmt1.setFloat(2, Float.parseFloat(limite));
					stmt1.setInt(3, Integer.parseInt(plazo));
					stmt1.setInt(4, 15);
					stmt1.setString(5, "CRE");//codigo tipo documento es credito
					stmt1.setString(6, descripcion);
				
					stmt1.executeUpdate();
		            con.commit();
		            //getCreditos();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				/*try {
					con.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				e.printStackTrace();
			}			
	}
	
	public void AjaxSaldoByCredito() {
		String idCredito= (String) super.getRequest().getParameter("idCredito");
		Double Saldo=0.0;
		PrintWriter pr=null;
	
		try{
			pr=super.getResponse().getWriter();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select "+
							"round(IFNULL((select sum(ate.valor)  as atenciones "+ 
							"from credito cre "+
							"inner join atencion ate "+
							"on cre.idcredito = ate.idCredito_ate "+
							"where cre.idcredito = credito1.idcredito),0),2) "+
							"-"+
							"round(IFNULL((select sum(abo.valor)  as abonos "+ 
							"from credito cre "+
							"inner join abono abo "+
							"on cre.idcredito = abo.idCredito_abo "+
							"where cre.idcredito = credito1.idcredito),0),2) as saldo "+
							"from credito credito1 "+ 
							"where credito1.idcredito="+idCredito+" and credito1.idEstado_cre = 15" );
			
			if(re.next()){
				re.beforeFirst();
				if(re.next()){
					Saldo=re.getDouble("saldo");
				}
			}else{
				Saldo=0.0;
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		pr.print(Saldo);
		
	}
	
	public void AjaxgetLimiteCredito() {
		String idCredito= (String) super.getRequest().getParameter("idCredito");
		Double Limite=0.0;
		PrintWriter pr=null;
	
		try{
			pr=super.getResponse().getWriter();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select Limite from credito where idCredito="+idCredito+";");
			
			if(re.next()){
				re.beforeFirst();
				if(re.next()){
					Limite=re.getDouble("Limite");
				}
			}else{
				Limite = 0.0;
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		pr.print(Limite);
		
	}
	
}
