package Common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;

import utils.dates;
import conexion.Conexion;
import Peticion.BasePeticiones;

public class AtencionCOM extends BasePeticiones {
	Conexion Conexion = new Conexion();
	private java.sql.Connection con = null;

	public void AjaxItemsAdd(){
		String idDiente=(String)super.getRequest().getParameter("idDiente");
		CatalogosCOM cat=new CatalogosCOM();
		StringBuilder items= new StringBuilder();
		PrintWriter pr=null;
		
		
		
		try {
			pr=super.getResponse().getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				items.append("<div id=\"atencion\" class=\"panel panel-primary\" title=\"Registrar Servicio\" >");
				items.append("<div class=\"panel panel-body\">");
				items.append("<div class=\"row\">");
					items.append("<label style=\"margin-left:35%\">Codigo pieza dental</label>");
					items.append("<input type=\"text\" id=\"idDiente\" name=\"idDiente\" value=\""+idDiente+"\" readonly=\"readonly\" size=\"2\" style=\"text-align: center;\">");
					items.append("</div><hr><br>");
					items.append("<div class=\"row\"> ");
					items.append("<select id=\"TipoServicio\">");
					items.append("<option value=-1>Seleccionar tipo servicio.... </option>"
							+ cat.ListaTipoServicios());
						
								//<%
								//	out.write(new CatalogosCOM().ListaTipoServicios());
								//%>
					items.append("</select> ");
					items.append("<select id=\"Servicio\">");
					items.append("<option value=-1>Seleccionar servicio.... </option>");
					items.append("</select>");
					items.append("</div>");
					items.append("<br><div><label>Precio:</label><input type='text' id=\"modal_precio\" name=\"modal_precio\" readonly=\"readonly\" size=\"5\" value=''>"
							+ "<label>Descuento %:</label><input type=\"number\" size=\"5\" id='modal_descuento' value=0></div>");
					items.append("<hr>");
					items.append("<label>Control de diagnostico por el modelo Sitio/Estado</label>");	
					items.append("<div class=\"row\">");
					items.append("<div class=\"col-sm-4\">");
					items.append("<div class=\"radio\">");
					items.append("<label><input type=\"radio\" id=\"Sitio\" name=\"Sitio\" value=1>Sitio 1</label>");
					items.append("</div>");
					items.append("<div class=\"radio\">");
					items.append("<label><input type=\"radio\" id=\"Sitio\" name=\"Sitio\" value=2>Sitio 2</label>");
					items.append("	</div>");
					items.append("<div class=\"radio\">");
					items.append("<label><input type=\"radio\" id=\"Sitio\" name=\"Sitio\" value=3>Sitio 3</label>");
					items.append("</div>");
					items.append("</div>");
					items.append("	<div class=\"col-sm-4\">");
					items.append("<div class=\"radio\">");
					items.append(" <label><input type=\"radio\" id=\"Estado\" name=\"Estado\" value=0>Estado 0</label>");
					items.append("</div>");
				    items.append("<div class=\"radio\">");
				    items.append("<label><input type=\"radio\" id=\"Estado\" name=\"Estado\" value=1>Estado 1</label>");
				    items.append("		</div>");
				    items.append("		<div class=\"radio\">");
				    items.append("		  <label><input type=\"radio\" id=\"Estado\" name=\"Estado\" value=2>Estado 2</label>");
				    items.append("		</div>");
				    items.append("		<div class=\"radio\">");
				    items.append("		  <label><input type=\"radio\" id=\"Estado\" name=\"Estado\" value=3>Estado 3</label>");
				    items.append("		</div>");
				    items.append("		<div class=\"radio\">");
				    items.append("		  <label><input type=\"radio\" id=\"Estado\" name=\"Estado\" value=4>Estado 4</label>");
				    items.append("		</div>");
				    items.append("	</div>");
				    items.append("</div>");
				    items.append("<div class=\"panel panel-footer\">");
				    items.append("	<button id=\"button\" type=\"button\" class=\"btn btn-success\">");
				    items.append("		<span class=\"glyphicon glyphicon-plus\">Añadir</span>");
				    items.append("	</button>");
				    items.append("</div>");
				    items.append("</div>");
				    items.append("</div>");
				    
				    
				    super.getResponse().setContentType("text/html;charset=UTF-8");
				    try {
						super.getRequest().setCharacterEncoding("UTF-8");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		pr.write(items.toString());		    
	}
	
	
	public void AjaxAddRow() {
		String idDiente = (String) super.getRequest().getParameter("idDiente");
		String idServicio = (String) super.getRequest().getParameter("idServicio");
		String sitio = (String) super.getRequest().getParameter("sitio");
		String estado = (String) super.getRequest().getParameter("estado");
		String desc = (String) super.getRequest().getParameter("desc");
		String total = (String) super.getRequest().getParameter("total");
		CatalogosCOM cat = new CatalogosCOM();
		DiagnosticoCOM di = new DiagnosticoCOM();
		String row = "";
		PrintWriter pr = null;

		try {
			pr = super.getResponse().getWriter();

			row = "<tr class='tableRow' data-total="+total+">" 
					+ "<td class='idDiente' name='idDiente' name='idDiente' value='" + idDiente + "'>"+ idDiente + "</td>" 
					+ "<td> <input type='hidden' class='idServicio' name='idServicio' value='"+ idServicio + "'>" + cat.GetServicioById(idServicio)+ "</td>" 
					+"<td class='idSitio' name='idSitio' value='" + sitio + "'><span title=\""+di.GetDiagnosticoSitio(sitio)+"\" class='glyphicon glyphicon-info-sign'>"+sitio + "</span></td>" 
					+ "<td class='idEstado' name='idEstado' value='" + estado+ "'><span title=\""+di.GetDiagnosticoEstado(estado)+"\" class='glyphicon glyphicon-info-sign'>"+estado + "</span> </td>" 
					+"<td class='precio' value='"+cat.GetPrecioServicioById(idServicio)+"'>"+cat.GetPrecioServicioById(idServicio)+"</td>"
					+"<td class='descuento' value'"+desc+"'>"+desc+"</td>"
					+"<td class='total' value='"+total+"'>"+total+"</td>"
					+"<td ><input class = 'realizado' type=\"checkbox\"></td>"
					+"<td><button type=\"button\" tittle=\"Eliminar fila\" class=\"del_row btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span></button></td>"
				+ "</tr>";
			//row += "["+idDiente+","+idServicio+","+sitio+","+estado+","+cat.GetPrecioServicioById(idServicio)+","+desc+","+total+",<input class = 'realizado' type=\"checkbox\">,<button id=\"del_row\" type=\"button\" tittle=\"Eliminar fila\" class=\"btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span></button></td>]";

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		pr.write(row);
		
	}
	
	public void saveAtencion(){
		String idCredito=super.getRequest().getParameter("idCredito");
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		java.sql.PreparedStatement stmt1=null;
		java.sql.PreparedStatement stmt2=null;
		con=Conexion.getconexion();
		Map<String, String[]> tableData = super.getRequest().getParameterMap();
		
		try {
			con.setAutoCommit(false);
			Statement s=con.createStatement();
			String sql1 ="Insert into atencion (idOdontologo,idPaciente,Descripcion,idTipoDocumento,Valor,idEstado,idCredito_ate)"
					+ " values(?,?,?,?,?,?,?)";
			String sql2="Insert into detalle_atencion (idAtencion,idDiente,idServicio,idDiagnostico_Sitio,idDiagnostico_Estado,Precio,Descuento,Total)"
					+ " values (?,?,?,?,?,?,?,?)";
			int codigoAtencion=0;
			java.sql.ResultSet result = null;
			stmt1 =con.prepareStatement(sql1,Statement.RETURN_GENERATED_KEYS);
			stmt2=con.prepareStatement(sql2);
			
			stmt1.setString(4, "ATE"); //codigo tipo documento es cotizacion
			stmt1.setInt(6, 1);
			stmt1.setInt(7, Integer.parseInt(idCredito));
			
			Set set = tableData.entrySet();
	        Iterator it = set.iterator();
	        int i =1;
	            while(it.hasNext()){
	            	//System.out.println("**"+i2+"**");
	                Map.Entry<String,String[]> entry = (Map.Entry<String,String[]>)it.next();
	 
	                String key             = entry.getKey();
	                String[] value         = entry.getValue();
	 
	              /*  System.out.println("Key is "+key+"<br>");
	 
	                    if(value.length>1){    
	                        for (int i = 0; i < value.length; i++) {
	                        	System.out.println("<li>aloha" + value[i].toString() + "</li><br>");
	                        }
	                    }else
	                    	System.out.println("Value is "+value[0].toString()+"<br>");
	 
	                    System.out.println("-------------------<br>");*/
	                	if(i>0 && i<7){
	                		//System.out.println(key);
	                			if (key.compareTo("idPaciente")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setInt(2, Integer.parseInt(value[0].toString()));	                				
	                			}else if (key.compareTo("idOdontologo")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setInt(1, Integer.parseInt(value[0].toString()));
	                			}else if (key.compareTo("descripcion")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setString(3, value[0].toString());
	                			}else if (key.compareTo("Total")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setFloat(5, Float.parseFloat(value[0].toString()));
	                				stmt1.executeUpdate();
	                				if(stmt1!=null){
	                					result = stmt1.getGeneratedKeys();
	                					 if (result.next()) {
	                						 codigoAtencion=result.getInt(1);
                                         }
	                				}
	                			}
	                	}else if (i>6){
	                		
	                		if(key.indexOf("idDiente")!=-1)	{
	                			stmt2.setInt(2, Integer.parseInt(value[0].toString().trim()));
	                			stmt2.setInt(1, codigoAtencion);
	                		}
	                		else if(key.indexOf("idServicio")!=-1)
	                			stmt2.setInt(3, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("idSitio")!=-1)
	                			stmt2.setInt(4, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("idEstado")!=-1)
	                			stmt2.setInt(5, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("precio")!=-1)
	                			stmt2.setFloat(6, Float.parseFloat(value[0].toString().trim()));
	                		else if(key.indexOf("descuento")!=-1){
	                			stmt2.setFloat(7, Float.parseFloat(value[0].toString().trim()));
	                		}else if(key.indexOf("total")!=-1){
	                			stmt2.setFloat(8, Float.parseFloat(value[0].toString().trim()));
	                			stmt2.addBatch(); //funciona para el registro de multiples insert's sql
	                		}
	                	}
	                    i++;
	            }
			
	            stmt2.executeBatch();
	            con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
   
    
		
		
	}
	
	public void getAtenciones(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				StringBuilder CotizacionesResultado= new StringBuilder();
				
		try{
			//System.out.println("idClinica:"+idClinica+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT idAtencion,concat(idTipoDocumento,'-',idcotizacion) as codigoCot,FechaCotizacion,Descripcion,Valor "
										+"FROM cotizacion "
										+"where idPaciente="+idPaciente+" and idEstado=1");
			
			
			
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re!=null){
				CotizacionesResultado.append("<div id=\"cot\" class='panel panel-primary'>");
				//CotizacionesResultado.append("<div id=\"cot\" class='panel panel-heading'><h4>Cargar Cotizaciones</div>");
				CotizacionesResultado.append("<table class='table table-hover'>");
				CotizacionesResultado.append("<thead style='background-color: #A9E2F3'>");
				CotizacionesResultado.append("<th>Codigo Cot</th>");
				CotizacionesResultado.append("<th>Descripcion</th>");
				CotizacionesResultado.append("<th>Fecha Cotizacion</th>");
				CotizacionesResultado.append("<th>Valor</th>");
				CotizacionesResultado.append("</thead>");
				CotizacionesResultado.append("<tbody>");
			while(re.next()){
				CotizacionesResultado.append("<tr class='rowcot' data-id='"+re.getInt("idcotizacion")+"'>");
				CotizacionesResultado.append("<td>"+re.getString("codigoCot")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("Descripcion")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("FechaCotizacion")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("Valor")+"</td>");
				CotizacionesResultado.append("</tr>");
			}
				CotizacionesResultado.append("</tbody>");
				
			con.close();
			}else
				CotizacionesResultado.append("<div class=\"alert alert-info\"><strong>Atencion!</strong> No hay informacion que mostrar.</div>");
		}catch(SQLException es){
			es.printStackTrace();
		}
		CotizacionesResultado.append("</table>");
		CotizacionesResultado.append("</div>");
		//System.out.println(CotizacionesResultado.toString()+"*******************");
		pr.write(CotizacionesResultado.toString());
		//return PacienteResultado.toString();
		
	}
	
	
	//devuelve las filas para atencion a partir de una cotizacion
	@SuppressWarnings("unchecked")
	public void AjaxAddRowCotizacion() {
		super.getResponse().setContentType("application/json");
		String idCotizacion = (String) super.getRequest().getParameter("idCotizacion");
		
		CatalogosCOM cat = new CatalogosCOM();
		DiagnosticoCOM di = new DiagnosticoCOM();
		String row = "";
		String descripcion = "";
		int total=0;
		PrintWriter pr = null;
		JSONObject json = new JSONObject();
		
		try {
			pr = super.getResponse().getWriter();
			con=Conexion.getconexion();			
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT * FROM cotizacion cot "+
										"left outer join detalle_cotizacion dcot "+
										"on cot.idCotizacion=dcot.idCotizacion "+
										"where cot.idCotizacion = '"+idCotizacion+"'");
	
			if(!re.equals(null)){

			while(re.next()){
				descripcion = re.getString("Descripcion");
				total=re.getInt("Valor");
				row += "<tr class='tableRow'>" 
						+ "<td class='idDiente' name='idDiente' value='" + re.getInt("idDiente") + "'>"+ re.getInt("idDiente") + "</td>" 
						+ "<td> <input type='hidden' class='idServicio' name='idServicio' value='"+ re.getInt("idServicio") + "'>" + cat.GetServicioById(""+re.getInt("idServicio"))+ "</td>" 
						+"<td class='idSitio' name='idSitio' value='" + re.getInt("idDiagnostico_Sitio") + "'><span title=\""+di.GetDiagnosticoSitio(""+re.getInt("idDiagnostico_Sitio"))+"\" class='glyphicon glyphicon-info-sign'>"+re.getInt("idDiagnostico_Sitio") + "</span></td>" 
						+ "<td class='idEstado' name='idEstado' value='" + re.getInt("idDiagnostico_Estado")+ "'><span title=\""+di.GetDiagnosticoEstado(""+re.getInt("idDiagnostico_Estado"))+"\" class='glyphicon glyphicon-info-sign'>"+re.getInt("idDiagnostico_Estado") + "</span> </td>" 
						+"<td class='precio' value='"+re.getFloat("Precio")+"'>"+re.getFloat("Precio")+"</td>"
						+"<td class='descuento' value'"+re.getFloat("Descuento")+"'>"+re.getFloat("Descuento")+"</td>"
						+"<td class='total' value='"+re.getFloat("total")+"'>"+re.getFloat("total")+"</td>"
						+"<td ><input class = 'realizado' type=\"checkbox\"></td>"
						+"<td><button id=\"del_row\" type=\"button\" tittle=\"Eliminar fila\" class=\"btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span></button></td>"
					+ "</tr>";
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    json.put("descripcion",descripcion);
		json.put("filas",row.toString());
		json.put("total",total);
	    
		pr.write(json.toString());
		
	}
	
	
	//metodo que registra las cotizaciones en la base de datos
	public void saveCotizacion(){
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		java.sql.PreparedStatement stmt1=null;
		java.sql.PreparedStatement stmt2=null;
		con=Conexion.getconexion();
		Map<String, String[]> tableData = super.getRequest().getParameterMap();
		
		try {
			con.setAutoCommit(false);
			Statement s=con.createStatement();
			String sql1 ="Insert into cotizacion (idOdontologo,idPaciente,Descripcion,idTipoDocumento,Valor,idEstado)"
					+ " values(?,?,?,?,?,?)";
			String sql2="Insert into detalle_cotizacion (idCotizacion,idDiente,idServicio,idDiagnostico_Sitio,idDiagnostico_Estado,Precio,Descuento,total)"
					+ " values (?,?,?,?,?,?,?,?)";
			int codigoCotizacion=0;
			java.sql.ResultSet result = null;
			stmt1 =con.prepareStatement(sql1,Statement.RETURN_GENERATED_KEYS);
			stmt2=con.prepareStatement(sql2);
			
			stmt1.setString(4, "COT"); //codigo tipo documento es cotizacion
			stmt1.setInt(6, 1);
			
			
			Set set = tableData.entrySet();
	        Iterator it = set.iterator();
	        int i =1;
	            while(it.hasNext()){
	            	//System.out.println("**"+i2+"**");
	                Map.Entry<String,String[]> entry = (Map.Entry<String,String[]>)it.next();
	 
	                String key             = entry.getKey();
	                String[] value         = entry.getValue();
	 
	              /*  System.out.println("Key is "+key+"<br>");
	 
	                    if(value.length>1){    
	                        for (int i = 0; i < value.length; i++) {
	                        	System.out.println("<li>aloha" + value[i].toString() + "</li><br>");
	                        }
	                    }else
	                    	System.out.println("Value is "+value[0].toString()+"<br>");
	 
	                    System.out.println("-------------------<br>");*/
	                	if(i>0 && i<7){
	                		//System.out.println(key);
	                			if (key.compareTo("idPaciente")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setInt(2, Integer.parseInt(value[0].toString()));	                				
	                			}else if (key.compareTo("idOdontologo")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setInt(1, Integer.parseInt(value[0].toString()));
	                			}else if (key.compareTo("descripcion")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setString(3, value[0].toString());
	                			}else if (key.compareTo("Total")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setFloat(5, Float.parseFloat(value[0].toString()));
	                				stmt1.executeUpdate();
	                				if(stmt1!=null){
	                					result = stmt1.getGeneratedKeys();
	                					 if (result.next()) {
                                         	codigoCotizacion=result.getInt(1);
                                         }
	                				}
	                			}
	                	}else if (i>6){
	                		
	                		if(key.indexOf("idDiente")!=-1)	{
	                			stmt2.setInt(2, Integer.parseInt(value[0].toString().trim()));
	                			stmt2.setInt(1, codigoCotizacion);
	                		}
	                		else if(key.indexOf("idServicio")!=-1)
	                			stmt2.setInt(3, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("idSitio")!=-1)
	                			stmt2.setInt(4, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("idEstado")!=-1)
	                			stmt2.setInt(5, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("precio")!=-1)
	                			stmt2.setFloat(6, Float.parseFloat(value[0].toString().trim()));
	                		else if(key.indexOf("descuento")!=-1){
	                			stmt2.setFloat(7, Float.parseFloat(value[0].toString().trim()));
	                		}else if(key.indexOf("total")!=-1){
	                			stmt2.setFloat(8, Float.parseFloat(value[0].toString().trim()));
	                			stmt2.addBatch(); //funciona para el registro de multiples insert's sql
	                		}
	                	}
	                    i++;
	            }
			
	            stmt2.executeBatch();
	            con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
   
    
		
		
	}
	
	
	public void UpdateCotizacion(){
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String idCotizacion=(String)super.getRequest().getParameter("idCotizacion");
		java.sql.PreparedStatement stmt1=null;
		java.sql.PreparedStatement stmt2=null;
		java.sql.PreparedStatement stmt3=null;
		con=Conexion.getconexion();
		Map<String, String[]> tableData = super.getRequest().getParameterMap();
		
		try {
			con.setAutoCommit(false);
			Statement s=con.createStatement();
			String sql1 ="UPDATE cotizacion set idOdontologo=?,idPaciente=?,Descripcion=?,idTipoDocumento=?,Valor=?,idEstado=? "
					+ " WHERE idCotizacion="+idCotizacion;
			String sql2="INSERT into detalle_cotizacion (idCotizacion,idDiente,idServicio,idDiagnostico_Sitio,idDiagnostico_Estado,Precio,Descuento,total)"
					+ " values (?,?,?,?,?,?,?,?)";
			int codigoCotizacion=0;
			java.sql.ResultSet result = null;
			stmt1 =con.prepareStatement(sql1);
			stmt2=con.prepareStatement(sql2);
			
			stmt1.setString(4, "COT"); //codigo tipo documento es cotizacion
			stmt1.setInt(6, 1);
			
			stmt3=con.prepareStatement("DELETE from detalle_cotizacion WHERE idCotizacion=?");
			stmt3.setInt(1, Integer.parseInt(idCotizacion.trim()));
			stmt3.executeUpdate(); //borra detalle de la cotizacion
			
			Set set = tableData.entrySet();
	        Iterator it = set.iterator();
	        int i =1;
	            while(it.hasNext()){
	            	//System.out.println("**"+i2+"**");
	                Map.Entry<String,String[]> entry = (Map.Entry<String,String[]>)it.next();
	 
	                String key             = entry.getKey();
	                String[] value         = entry.getValue();
	 
	              /*  System.out.println("Key is "+key+"<br>");
	 
	                    if(value.length>1){    
	                        for (int i = 0; i < value.length; i++) {
	                        	System.out.println("<li>aloha" + value[i].toString() + "</li><br>");
	                        }
	                    }else
	                    	System.out.println("Value is "+value[0].toString()+"<br>");
	 
	                    System.out.println("-------------------<br>");*/
	                	if(i>0 && i<7){
	                		//System.out.println(key);
	                			if (key.compareTo("idPaciente")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setInt(2, Integer.parseInt(value[0].toString()));	                				
	                			}else if (key.compareTo("idOdontologo")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setInt(1, Integer.parseInt(value[0].toString()));
	                			}else if (key.compareTo("descripcion")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setString(3, value[0].toString());
	                			}else if (key.compareTo("Total")==0){
	                				System.out.println(key+":"+value[0].toString());
	                				stmt1.setFloat(5, Float.parseFloat(value[0].toString()));
	                				stmt1.executeUpdate();
	                				/*if(stmt1!=null){
	                					result = stmt1.getGeneratedKeys();
	                					 if (result.next()) {
                                         	codigoCotizacion=result.getInt(1);
                                         }
	                				}*/
	                			}
	                	}else if (i>6){
	                		
	                		if(key.indexOf("idDiente")!=-1)	{
	                			stmt2.setInt(2, Integer.parseInt(value[0].toString().trim()));
	                			stmt2.setInt(1, Integer.parseInt(idCotizacion.trim()));
	                		}
	                		else if(key.indexOf("idServicio")!=-1)
	                			stmt2.setInt(3, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("idSitio")!=-1)
	                			stmt2.setInt(4, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("idEstado")!=-1)
	                			stmt2.setInt(5, Integer.parseInt(value[0].toString().trim()));
	                		else if(key.indexOf("precio")!=-1)
	                			stmt2.setFloat(6, Float.parseFloat(value[0].toString().trim()));
	                		else if(key.indexOf("descuento")!=-1){
	                			stmt2.setFloat(7, Float.parseFloat(value[0].toString().trim()));
	                		}else if(key.indexOf("total")!=-1){
	                			stmt2.setFloat(8, Float.parseFloat(value[0].toString().trim()));
	                			stmt2.addBatch(); //funciona para el registro de multiples insert's sql
	                		}
	                	}
	                    i++;
	            }
			
	            stmt2.executeBatch();
	            con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
   
    
		
		
	}
	
	
	public void getCotizaciones(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				StringBuilder CotizacionesResultado= new StringBuilder();
				
		try{
			//System.out.println("idClinica:"+idClinica+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT idcotizacion,concat(idTipoDocumento,'-',idcotizacion) as codigoCot,FechaCotizacion,Descripcion,Valor "
										+"FROM cotizacion "
										+"where idPaciente="+idPaciente+" and idEstado=1");
			
			
			
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re.next()){
				re.beforeFirst();
			while(re.next()){
				CotizacionesResultado.append("<tr class='rowcot' data-id='"+re.getInt("idcotizacion")+"'>");
				CotizacionesResultado.append("<td>"+re.getString("codigoCot")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("Descripcion")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("FechaCotizacion")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("Valor")+"</td>");
				CotizacionesResultado.append("</tr>");
			}
				
				
			con.close();
			}else
				CotizacionesResultado.append("<div class=\"alert alert-info\"><strong>Atencion!</strong> No hay informacion que mostrar.</div>");
		}catch(SQLException es){
			es.printStackTrace();
		}
		
		//System.out.println(CotizacionesResultado.toString()+"*******************");
		pr.write(CotizacionesResultado.toString());
		//return PacienteResultado.toString();
		
	}
	
	
}
