package Common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import sun.print.resources.serviceui;
import Peticion.BasePeticiones;
import conexion.Conexion;

public class CatalogosCOM extends BasePeticiones {
	Conexion Conexion=new Conexion();
	private java.sql.Connection con=null;
	
	public String GetServicioById(String idServicio) {
		//String idServicio=(String)super.getRequest().getParameter("idServicio");
		String NombreServicio="";
	/*	PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		try{
			//System.out.println("idodontologo:"+idOdontologo+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT Snombre FROM servicio WHERE idServicio="+idServicio);
			
			if(re!=null){
				if(re.next()){
					NombreServicio=re.getString("Snombre");
				}
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			}
		
		//System.out.println(Servicios.toString()+"*******************");
		return NombreServicio;
		//return Servicios.toString();
		
	}
	
	public String GetPrecioServicioById(String idServicio) {
		String Precio="";
	
		try{
			
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT Precio FROM servicio WHERE idServicio="+idServicio);
			
			if(re!=null){
				if(re.next()){
					Precio=re.getString("Precio");
				}
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			}
		
		
		return Precio;
		
	}
	
	
	public void AjaxGetPrecioServicioById() {
		String idServicio= (String) super.getRequest().getParameter("idServicio");
		Double Precio=0.0;
		PrintWriter pr=null;
	
		try{
			pr=super.getResponse().getWriter();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT Precio FROM servicio WHERE idServicio="+idServicio);
			
			if(re!=null){
				if(re.next()){
					Precio=re.getDouble("Precio");
				}
			}
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		pr.print(Precio);
		
	}
	
	public String ServiciosMostrar(String idClinica){//muestra tabla de servicio al iniciar la pagina
		//String idOdontologo=(String)super.getRequest().getParameter("idOdontologo");
		/*PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		StringBuilder Servicios= new StringBuilder();
		
		
		boolean band=false;
		try{
			//System.out.println("idodontologo:"+idOdontologo+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select s.idServicio,s.Snombre,s.Descripcion as des_ser,s.Precio,e.Nombre,ts.Descripcion as desc_ts,s.idEstado"
					+ " from servicio s inner join tiposervicio ts on s.idTipoServicio=ts.idTipoServicio"
					+ " inner join estado e on s.idEstado=e.idEstado  where idClinica="+idClinica);
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re!=null){
			Servicios.append("<div id=\"result\">");
			Servicios.append("<table id=\"tabla\"class=\"table table-bordered\"><th></th>");
			Servicios.append("<thead>");
			Servicios.append("<th>Servicio</th><th>Descripcion</th>");
			Servicios.append("<th>Precio</th>");
			Servicios.append("<th>Tipo servicio</th>");
			Servicios.append("<th>Estado</th>");
			Servicios.append("<th>Accion</th>");
			Servicios.append("</thead>");
			Servicios.append("<tbody>");
			while(re.next()){
				if(re.getString("idEstado").compareTo("11")==0)
				Servicios.append("<tr class=\"inactive-row\">");
				//Servicios.append("<td><img class=\"img\" src=\"VerImg?idPac="+re.getInt("IdPaciente")+"\"></td>");

				Servicios.append("<td>"+re.getString("Snombre")+"</td>");
				Servicios.append("<td>"+re.getString("des_ser")+"</td>");
				Servicios.append("<td>"+re.getString("Precio")+"</td>");
				Servicios.append("<td>"+re.getString("desc_ts")+"</td>");
				Servicios.append("<td>"+re.getString("Nombre")+"</td>");//campo nombre del estado
				Servicios.append("<td>");
					Servicios.append("<button id=\"update_ser\" value="+re.getInt("idServicio")+" class=\"btn btn-warning btn-xs\" rel=\"tooltip\" title=\"Editar Servicio\">");
				//Servicios.append(" onclick=\"$(location).attr('href','ActualizarPaciente.jsp?idPaciente="+re.getInt("idPaciente")+"');\" style=\"margin-bottom: 0px;\">");
					Servicios.append("<span class=\"glyphicon glyphicon-edit\"></span> </button>");
				Servicios.append("</td>");
				Servicios.append("</tr>");
				band=true;
			}
			Servicios.append("</tbody>");
			Servicios.append("</table>");
			Servicios.append("</div>");
			con.close();
			}	
				if(band==false){
					Servicios.delete(0, Servicios.length());
					Servicios.append("<div class=\"alert alert-info\"><strong>Atencion!</strong> No hay informacion que mostrar.</div>");			
				}
			}catch(SQLException es){
			es.printStackTrace();
		}
		
		System.out.println(Servicios.toString()+"*******************");
		//pr.write(Servicios.toString());
		return Servicios.toString();
		
	}
	
	
	public void AjaxRegistrarServicios(){ //registra, muestra tabla de servicios implementando ajax
		String idClinica=(String)super.getRequest().getParameter("idClinica");
		String precio=(String)super.getRequest().getParameter("precio");
		String servicio=(String)super.getRequest().getParameter("servicio");
		String descripcion=(String)super.getRequest().getParameter("descripcion");
		String tipoServicio=(String)super.getRequest().getParameter("tipoServicio");
		String estado=(String)super.getRequest().getParameter("estado");
		String moneda=(String)super.getRequest().getParameter("moneda");
		
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try{
			//System.out.println("idodontologo:"+idOdontologo+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			s.executeUpdate("insert into servicio(idClinica,idTipoServicio,Precio,Descripcion,Snombre,idEstado,moneda) "
					+ "values('"+idClinica+"',"+tipoServicio+","+precio+",'"+descripcion+"','"+servicio+"',"+estado+",'"+moneda+"')");
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			}
		
		//System.out.println(Servicios.toString()+"*******************");
		pr.write(ServiciosMostrar(idClinica));
		//return Servicios.toString();
		
	}
	
	public void AjaxActualizarServicios(){ //registra, muestra tabla de servicios implementando ajax
		String idClinica=(String)super.getRequest().getParameter("idClinica");
		String precio=(String)super.getRequest().getParameter("precio");
		String servicio=(String)super.getRequest().getParameter("servicio");
		String descripcion=(String)super.getRequest().getParameter("descripcion");
		String tipoServicio=(String)super.getRequest().getParameter("tipoServicio");
		String estado=(String)super.getRequest().getParameter("estado");
		String moneda=(String)super.getRequest().getParameter("moneda");
		String idServicio=(String)super.getRequest().getParameter("idServicio");
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try{
			//System.out.println("idodontologo:"+idOdontologo+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			s.executeUpdate("update servicio set idClinica="+idClinica+",idTipoServicio="+tipoServicio+",Precio="+precio+",Descripcion='"+descripcion+"',Snombre='"+servicio+"',idEstado="+estado+",moneda='"+moneda+"' "
					+ " where idServicio="+idServicio);
			con.close();
			}catch(SQLException es){
				es.printStackTrace();
			}
		
		//System.out.println(Servicios.toString()+"*******************");
		pr.write(ServiciosMostrar(idClinica));
		//return Servicios.toString();
		
	}
	
	
	
	public String TipoServicios(){//crea control seleccionador con los tipos de servicios 
		StringBuilder TiposServicios= new StringBuilder();
		
		boolean band=false;
		try {
			con=Conexion.getconexion();
			Statement s = con.createStatement();
			ResultSet re=s.executeQuery("select * from tiposervicio");
			
			if(re!=null){
				//TiposServicios.append("<select id=\"tipoServicio\" name=\"tipoServicio\" style=\"margin-left:25px;\" class=\"form-control\">");
				TiposServicios.append("<option value='0'>Seleccionar Tipo Servicio</option>");
				while(re.next()){			
				TiposServicios.append("<option value='"+re.getInt("idTipoServicio")+"'>"+re.getString("Descripcion")+"</option>");
				band=true;
				}
				//TiposServicios.append("</select>");
			}
				if(band==false){
					TiposServicios.delete(0, TiposServicios.length());
					TiposServicios.append("<select><option value='-1'>Ocurrio un problema</option></select>");
				}
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return TiposServicios.toString();
		
	}
	
	
	public String TipoServicios(int tiposer){//crea control seleccionador con los tipos de servicios (con parametro)
		StringBuilder TiposServicios= new StringBuilder();
		
		boolean band=false;
		try {
			con=Conexion.getconexion();
			Statement s = con.createStatement();
			ResultSet re=s.executeQuery("select * from tiposervicio");
			
			if(re!=null){
				//TiposServicios.append("<select id=\"tipoServicio\" name=\"tipoServicio\" style=\"margin-left:25px;\" class=\"form-control\">");
				TiposServicios.append("<option value='0'>Seleccionar Tipo Servicio</option>");
				while(re.next()){			
				TiposServicios.append("<option value='"+re.getInt("idTipoServicio")+"' "+(String)((re.getInt("idTipoServicio")==tiposer)?"selected":"")+" >"+re.getString("Descripcion")+"</option>");
				band=true;
				}
				//TiposServicios.append("</select>");
			}
				if(band==false){
					TiposServicios.delete(0, TiposServicios.length());
					TiposServicios.append("<select><option value='-1'>Ocurrio un problema</option></select>");
				}
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return TiposServicios.toString();
		
	}
	
	
	public void MostrarRegServicios(){//formulario de registro de nuevo servicio
		StringBuilder form=new StringBuilder();
		EstadosCOM est=new EstadosCOM();
		
		form.append("<div id=\"sernew\" class=\"panel panel-success\" style=\"display:none\">"+
				"<div class=\"panel panel-heading\">"+
					"<label>Registrar servicio</label>"+
				"</div>");
		
		form.append("<div class=\"input-group\">"+
				"<div class=\"row\">"+
						"<div class=\"col-sm-6\">"+
							"<input style=\"margin-left:10px;\" id=\"servicio\" type=\"text\" name=\"nombre\" class=\"form-control\" "+
								"placeholder=\"Servicio\" required=\"required\" />"+
						"</div>"+
						"<div class=\"col-sm-2\">"+
						"<select id=\"moneda\">"+
							"<option value=\"NIO\">NIO</option>"+
							"<option value=\"USD\">USD</option>"+
						"</select>"+
						"</div>"+
						"<div class=\"col-sm-3\" >"+
							"<input type=\"number\" id=\"precio\" name=\"precio\" class=\"form-control\""+
								"placeholder=\"Precio\" required=\"required\""+
								"pattern=\"^[0-9].[0-9]{2}$\" />"+
						"</div>"+
					"</div>"+
					"<br>"+
					"<div class=\"row\">"+
						"<div class=\"col-sm-13\">"+
							"<input style=\"margin-left:25px;\" type=\"text\" id=\"descripcion\" name=\"descripcion\" class=\"form-control\""+
								"placeholder=\"Descripcion\" />"+
						"</div>"+
					"</div>"+"<br>"+
					"<div class=\"row\">"+
						"<div class=\"col-sm-6\">"+
							"<select id=\"tipoServicio\" name=\"tipoServicio\" style=\"margin-left:10px;\" class=\"form-control\">"+
								" "+TipoServicios()+" "+
							"</select>"+
						"</div>"+
						"<div class=\"col-sm-3\">"+
							"<select id=\"estado\" name=\"estado\" class=\"form-control\">"+
								""+est.getEstadosPorTipo("4")+""+//rellena estados tipo 4 %>"+
							"</select>"+
						"</div>"+
					"</div>"
					+ "</div>"+
					"<br>"
					+ "<div class=\"panel panel-footer\">"+
					"<button id=\"submit\" type=\"button\" class=\"btn btn-primary\">Guardar</button>"+
				"</div>"
				
				);
		form.append("</div>");
		PrintWriter pr = null;
		try {
			pr = super.getResponse().getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pr.write(form.toString());
		
		
	}
	
	public void MostrarUpServicios(){//formulario de actualizacion de servicios
		StringBuilder form=new StringBuilder();
		EstadosCOM est=new EstadosCOM();
		String idServicio=(String)super.getRequest().getParameter("idServicio");
		form.append("<input type=\"hidden\" id=\"idServicio\" name=\"idServicio\" value=\""+idServicio+"\" />");
		
		try{
		con=Conexion.getconexion();
		Statement s=con.createStatement();
		ResultSet re=s.executeQuery("select * from servicio where idServicio="+idServicio);
		//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
		if(re!=null){
		 while(re.next()){
		
		form.append("<div id=\"serup\" class=\"panel panel-info\" style=\"display:none\">"+
				"<div class=\"panel panel-heading\">"+
					"<label>Actualizar servicio</label>"+
				"</div>");
		
		form.append("<div class=\"input-group\">"+
				"<div class=\"row\">"+
						"<div class=\"col-sm-6\">"+
							"<input style=\"margin-left:10px;\" id=\"servicio\" type=\"text\" name=\"nombre\" class=\"form-control\" "+
								"placeholder=\"Servicio\" required=\"required\" value=\""+re.getString("Snombre")+"\"/>"+
						"</div>"+
						"<div class=\"col-sm-2\">"+
						"<select id=\"moneda\">"+
							"<option value=\"NIO\" "+(String)((re.getString("moneda").compareTo("NIO")==0)?"selected":"")+">NIO</option>"+
							"<option value=\"USD\" "+(String)((re.getString("moneda").compareTo("USD")==0)?"selected":"")+">USD</option>"+
						"</select>"+
						"</div>"+
						"<div class=\"col-sm-3\" >"+
							"<input type=\"number\" id=\"precio\" name=\"precio\" value=\""+re.getFloat("Precio")+"\" class=\"form-control\""+
								"placeholder=\"Precio\" required=\"required\""+
								"pattern=\"^[0-9].[0-9]{2}$\" />"+
						"</div>"+
					"</div>"+
					"<br>"+
					"<div class=\"row\">"+
						"<div class=\"col-sm-13\">"+
							"<input style=\"margin-left:25px;\" type=\"text\" id=\"descripcion\" name=\"descripcion\" value=\""+re.getString("Descripcion")+"\" class=\"form-control\""+
								"placeholder=\"Descripcion\" />"+
						"</div>"+
					"</div>"+"<br>"+
					"<div class=\"row\">"+
						"<div class=\"col-sm-6\">"+
							"<select id=\"tipoServicio\" name=\"tipoServicio\" style=\"margin-left:10px;\" class=\"form-control\">"+
								" "+TipoServicios(re.getInt("idTipoServicio"))+" "+
							"</select>"+
						"</div>"+
						"<div class=\"col-sm-3\">"+
							"<select id=\"estado\" name=\"estado\" class=\"form-control\">"+
								""+est.getEstadosPorTiposelected(4,re.getInt("idEstado"))+""+//rellena estados tipo 4 %>"+
							"</select>"+
						"</div>"+
					"</div>"
					+ "</div>"+
					"<br>"
					+ "<div class=\"panel panel-footer\">"+
					"<button id=\"update\" type=\"button\" class=\"btn btn-primary\">Actualizar</button>"+
				"</div>"
				
				);
		form.append("</div>");
		}
		}
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		PrintWriter pr = null;
		try {
			pr = super.getResponse().getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		pr.write(form.toString());
		
		
	}
	
	public String ListaTipoServicios() {
		StringBuilder tps = new StringBuilder();

		try {
			con = Conexion.getconexion();
			Statement s = con.createStatement();
			ResultSet re = s.executeQuery("SELECT * FROM tiposervicio");
			if(re!=null){
				while (re.next()) {
					tps.append("<option value='"+re.getInt("idTipoServicio")+"'>"+re.getString("Descripcion")+"</option>");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tps.toString();
	}
	
	//Retorna un tag option con la lista de servicios de un usuario en especifico  
	public void ListaSerPorTipo(){
		StringBuilder ser = new StringBuilder();
		String idTipoServicio=(String)super.getRequest().getParameter("idTipoServicio");
		String idClinica=(String)super.getRequest().getParameter("idClinica");
		PrintWriter pr=null;

		try {
			pr=super.getResponse().getWriter();
			con = Conexion.getconexion();
			Statement s = con.createStatement();
			ResultSet re = s.executeQuery("SELECT * FROM servicio WHERE idTipoServicio="+idTipoServicio+" and idClinica="+idClinica +" and idEstado=10");
			if(re!=null){
				ser.append("<option value=-1>Seleccionar servicio.... </option>");
				while (re.next()) {
					ser.append("<option value='"+re.getInt("idServicio")+"'>"+re.getString("Snombre")+"</option>");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pr.write(ser.toString());
		
	}
	
	
		
}
	
	

