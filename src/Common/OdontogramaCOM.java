package Common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;

import Peticion.BasePeticiones;
import conexion.Conexion;

public class OdontogramaCOM extends BasePeticiones {
	Conexion Conexion = new Conexion();
	private java.sql.Connection con = null;
	
	
	public String getOdontograma(){
		//String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		//PrintWriter pr=null;
		
		//pr=super.getResponse().getWriter();
		//super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		StringBuilder Resultado= new StringBuilder();
				
		try{
			//System.out.println("idClinica:"+idClinica+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select * from diente ");
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re.first()){
				re.beforeFirst();
				while(re.next()){
				Resultado.append("var img"+re.getInt("idDiente")+" = new Image(); \n"+
					"img"+re.getInt("idDiente")+".src = '"+re.getString("RutaImg")+"'; \n"+
					"fabric.Image.fromURL(img"+re.getInt("idDiente")+".src, function(oImg) { \n"+
						//"oImg.set({ \n	}) \n"+
						" var group"+re.getInt("idDiente")+" = new fabric.Group([oImg], {"
								+"\n id:"+re.getInt("idDiente")+",\n"+
								"left : "+re.getFloat("axis_x")+", \n"+
								"top : "+re.getFloat("axis_y")+", \n"+
								"perPixelTargetFind : true, \n"+
								"targetFindTolerance : 4, \n"+
								"hasControls : false, \n"+
								"hasBorders : false, \n"+
								"selectable : false, \n"+
								"hoverCursor : 'pointer', \n"
								+ "renderOnAddRemove: true, \n"
								+ "propiedad: 1 \n"+
								 "});"+
						"canvas.add(group"+re.getInt("idDiente")+"); \n"+
					"}) \n");
				}  
				
			con.close();
			}else
				Resultado.append("");
		}catch(SQLException es){
			es.printStackTrace();
		}
		//System.out.println(CreditosResultado.toString()+"*******************");
		//pr.write(Resultado.toString());
		return Resultado.toString();
		//return PacienteResultado.toString();
	}
	
	public void getEstadosDientes(){
		StringBuilder Resultado= new StringBuilder();
		
		PrintWriter pr=null;
		
		
		
		try {
			pr=super.getResponse().getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		con=Conexion.getconexion();
		Statement s;
		try {
			s = con.createStatement();
			ResultSet re=s.executeQuery("SELECT * FROM swgpo.estadodiente;");
			if(re.first()){
				re.beforeFirst();
				while(re.next()){
					Resultado.append("<div class=\"radio col-xs-4 col-sm-4 col-md-4\">");
					Resultado.append("<label><input data-id="+re.getInt("idEstadoDiente")+" data-src='"+re.getString("img")+"' type=\"radio\" name=\"optradio\">"+re.getString("DescripcionEstado")+"</label>");
					Resultado.append("</div>");
				}
			}else Resultado.append("No hay resultados");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pr.write(Resultado.toString());
		
	}
	
	public void getOdontogramaJSON(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		StringBuilder Resultado= new StringBuilder();
		
		PrintWriter pr=null;
	
		try {
			pr=super.getResponse().getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		con=Conexion.getconexion();
		Statement s;
		try {
			s = con.createStatement();
			ResultSet re=s.executeQuery("SELECT * FROM swgpo.odontograma where PacienteO_idPaciente="+idPaciente);
			if(re.next()){
					Resultado.append(re.getString("JSON"));
			}else {
				ResultSet re2=s.executeQuery("SELECT * FROM JSON");
				if(re2.next()){
						Resultado.append(re2.getString("JSON"));
				}
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		super.getResponse().setContentType("application/json");
	    
		
		pr.write(Resultado.toString());
		pr.flush();
		
	}
	
	public void getOdontogramaJSONvacio(){
		StringBuilder Resultado= new StringBuilder();
		
		PrintWriter pr=null;
	
		try {
			pr=super.getResponse().getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		con=Conexion.getconexion();
		Statement s;
		try {
			s = con.createStatement();
			ResultSet re=s.executeQuery("SELECT * FROM JSON");
			if(re.next()){
					Resultado.append(re.getString("JSON"));
			}else {
						Resultado.append("");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		super.getResponse().setContentType("application/json");
	    
		
		pr.write(Resultado.toString());
		pr.flush();
		
	}
	
	public void saveOdonto(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		String desc=(String)super.getRequest().getParameter("descripcion");
		String json=(String)super.getRequest().getParameter("json");
		
		super.getResponse().setContentType("text/html;charset=UTF-8");
	    try {
			super.getRequest().setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		java.sql.PreparedStatement stmt1=null;
		
		try {
			//Statement s=con.createStatement();
			
			con=Conexion.getconexion();
			
			Statement s = con.createStatement();
			ResultSet re=s.executeQuery("SELECT * FROM swgpo.odontograma where PacienteO_idPaciente="+idPaciente);
			if(re.next()){
				String sql1 ="update odontograma set Descripcion=?,JSON=? where PacienteO_idPaciente="+idPaciente;
				//java.sql.ResultSet result = null;
				stmt1 =con.prepareStatement(sql1);
				 //codigo tipo documento es cotizacion
				stmt1.setString(1, desc);
				stmt1.setString(2, json);
				stmt1.executeUpdate();
			}else{
			
			String sql1 ="Insert into odontograma (PacienteO_idPaciente,Descripcion,JSON)"
					+ " values(?,?,?)";
			 
			//java.sql.ResultSet result = null;
			stmt1 =con.prepareStatement(sql1);
			
			stmt1.setString(1, idPaciente); //codigo tipo documento es cotizacion
			stmt1.setString(2, desc);
			stmt1.setString(3, json);
			
			stmt1.executeUpdate();
			}
		
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public void getHistorial(){
		String idPaciente=(String)super.getRequest().getParameter("idPaciente");
		String idDiente=(String)super.getRequest().getParameter("idDiente");
		PrintWriter pr=null;
		
		try {
			pr=super.getResponse().getWriter();
			super.getResponse().setContentType( "text/html; charset=iso-8859-1" );
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				StringBuilder CotizacionesResultado= new StringBuilder();
				
		try{
			//System.out.println("idClinica:"+idClinica+busqueda);
			//
			//con=Conexion.getconexion();
			con=Conexion.getconexion();
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("select a.idAtencion,FechaAtencion,(select Snombre from servicio ser where ser.idServicio=da.idServicio) as servicio,"+
										"Precio from atencion a "+
										"inner join detalle_atencion da "+
										"on a.idAtencion=da.idDetalleAtencion "+
										"where a.idPaciente="+idPaciente+" and da.idDiente="+idDiente+"");
			
					
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			if(re.next()){
				re.beforeFirst();
			while(re.next()){
				CotizacionesResultado.append("<tr class='rowhist'>");
				CotizacionesResultado.append("<td>"+re.getString("a.idAtencion")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("FechaAtencion")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("servicio")+"</td>");
				CotizacionesResultado.append("<td>"+re.getString("Precio")+"</td>");
				CotizacionesResultado.append("</tr>");
				
		
			}
				
				
			con.close();
			}else
				CotizacionesResultado.append("<tr class=\"alert alert-info\"  colspan='4'> <strong>Atencion!</strong> No hay informacion que mostrar.</tr>");
		}catch(SQLException es){
			es.printStackTrace();
		}
		
		System.out.println(CotizacionesResultado.toString());
		//System.out.println(CotizacionesResultado.toString()+"*******************");
		pr.write(CotizacionesResultado.toString());
		//return PacienteResultado.toString();
		
	}
	
	
}
