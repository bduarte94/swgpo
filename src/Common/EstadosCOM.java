package Common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import conexion.Conexion;
import Peticion.BasePeticiones;

public class EstadosCOM extends BasePeticiones {
	Conexion Conexion=new Conexion();
	private java.sql.Connection con=null;
	
	public String getEstadosPorTipo(String idTipoEstado){
		StringBuilder estado=new StringBuilder();
		try{
		con=Conexion.getconexion();
		Statement s=con.createStatement();
		ResultSet re=s.executeQuery("select * from estado e inner join tipoestado te on e.idtipoestado=te.idtipoestado "
									+ "where te.idtipoestado="+idTipoEstado);
		
		if(re!=null){
			while (re.next()) {
				estado.append("<option value="+re.getString("idEstado")+">"+re.getString("Nombre")+"</option>");
			}
			
		}
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		
		return estado.toString();
		
	}
	
	public String getEstadosPorTiposelected(int idTipoEstado,int idestado){
		StringBuilder estado=new StringBuilder();
		try{
		con=Conexion.getconexion();
		Statement s=con.createStatement();
		ResultSet re=s.executeQuery("select * from estado e inner join tipoestado te on e.idTipoEstado=te.idTipoEstado "
									+ "where te.idTipoEstado="+idTipoEstado);
		
		if(re!=null){
			while (re.next()) {
				estado.append("<option value="+re.getString("idEstado")+" "+(String)((re.getInt("idEstado")==idestado)?"selected":"")+" >"+re.getString("Nombre")+"</option>");
			}
			
		}
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		
		return estado.toString();
		
	}
	

}
