package Common;

import java.io.IOException;
import java.io.PrintWriter;

import Dao.ClinicaDAO;
import Peticion.BasePeticiones;
import conexion.Conexion;

public class ClinicaCOM extends BasePeticiones{
	public void insertarClinica(){
		
		String nom= super.getRequest().getParameter("txtNom1");
		String siglas= super.getRequest().getParameter("txtSiglas");
		String dir= super.getRequest().getParameter("txtDir");
		String tel= super.getRequest().getParameter("txtTel");
		String ruc= super.getRequest().getParameter("txtRuc");
		String user= super.getRequest().getParameter("txtlogin");
		String pass= super.getRequest().getParameter("txtpass");
		String email= super.getRequest().getParameter("txtcorreo");
		String prop= super.getRequest().getParameter("txtPropietario");
		ClinicaDAO clinicaDAO=new ClinicaDAO();
		
		String uri = super.getRequest().getScheme() + "://" +   // "http" + "://
				super.getRequest().getServerName() +       // "myhost"
	             ":" +                           // ":"
	             super.getRequest().getServerPort() +       // "8080"
	             //super.getRequest().getRequestURI();       // "/people"
	             super.getRequest().getContextPath().toString();
		
		
	             //"?" +                           // "?"
	             //request.getQueryString(); 
		
		if(clinicaDAO.RegistrarClinica(nom, siglas, dir, tel, ruc, pass, user,prop,email,uri)){
			try {
				super.getResponse().sendRedirect("RegistrarClinica.jsp?ok=1");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else
			try {
				super.getResponse().sendRedirect("RegistrarClinica.jsp?ok=0");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	
	public String getNombreclinica(String idClinica){
			ClinicaDAO cl=new ClinicaDAO();
		return cl.getClinicaNombre(idClinica); 
	}
	
}
