package com.dhtmlx.calendar;

import com.dhtmlx.planner.DHXEvent;

public class Event extends DHXEvent{
	public String idPaciente;
	public String idOdontologo1;
	
	public void setPaciente(String Paciente){
		this.idPaciente = Paciente; 
	}
	public String getPaciente(){
		return this.idPaciente;
	}
	
	public void setOdontologo(String Odontologo){
		this.idOdontologo1 = Odontologo; 
	}
	public String getOdontologo(){
		return this.idOdontologo1;
	}
}