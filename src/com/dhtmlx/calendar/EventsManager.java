package com.dhtmlx.calendar;

import com.dhtmlx.planner.DHXEventsManager;
import com.dhtmlx.planner.DHXStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.dhtmlx.planner.DHXEv;
import com.dhtmlx.planner.DHXEvent;
import com.dhtmlx.planner.data.DHXCollection;

import Entidades.Odontologo;
import Entidades.Paciente;
import conexion.Conexion;
public class EventsManager extends DHXEventsManager{
	private java.sql.Connection con=null;
	public Conexion conexion=new Conexion();
	private String idOdontologo;
	private String idClinica;
	
	public EventsManager(HttpServletRequest request,String idOdontologo,String idClinica){
		
		super(request);
		this.idOdontologo=idOdontologo;
		this.idClinica = idClinica;
	}
	
	public Iterable<DHXEv> getEvents(){
		ArrayList<DHXEv>events=new ArrayList<DHXEv>();
		Date fecha=new Date();
		Date time=new Date();
		Date time2=new Date();
		SimpleDateFormat fe=new SimpleDateFormat("M/d/YYYY");
		SimpleDateFormat ti=new SimpleDateFormat("HH:mm");
		//Calendar cal = Calendar.getInstance();
		
		try{
			
			con=conexion.getconexion();
			Statement s=con.createStatement();
			String query = "";
			if(this.idOdontologo!=null && this.idOdontologo.length() > 0)
				query = "select idCita,Fecha,start_date,PacienteC_idPaciente,concat(Pnombre1,'',Papellido1) as paciente,text,end_date,OdontologoC_idOdontologo from cita,paciente "+
						"where IdPaciente=PacienteC_idPaciente and OdontologoC_idOdontologo="+this.idOdontologo+" order by idCita";
			else
				query = "select idCita,Fecha,start_date,PacienteC_idPaciente,concat(Pnombre1,' ',Papellido1) as paciente,`text`,end_date,o.idOdontologo,concat(o.Onombre1,' ',o.Oapellido1) as otontologo,c.OdontologoC_IdOdontologo "+
						"from cita c "+
						"inner join odontologo o "+
						"on o.idOdontologo=c.OdontologoC_IdOdontologo "+
						"inner join paciente p "+
						"on p.IdPaciente=c.PacienteC_idPaciente "+
						"inner join clinica cli "+
						"on cli.idClinica = o.idClinica "+
						"where cli.idClinica ="+idClinica+
						" order by idCita";
			System.out.println(query);
			ResultSet re=s.executeQuery(query);
			//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
			while(re.next()){
				//Event e=new Event();
				//fecha=re.getDate("Fecha");
				time=re.getTimestamp("start_date");
				time2=re.getTimestamp("end_date");	
				Event ev1=new Event();
				
				ev1.setId(re.getInt("idCita"));
				//ev1.setidPaciente(re.getInt("PacienteC_idPaciente"));
				//ev1.setStart_date(""+fe.format(fecha).toString()+" "+ti.format(time).toString());
				ev1.setStart_date(time); 
				//cal.setTime(time);
				//cal.add(Calendar.HOUR, 2);
				//time=cal.getTime();
				//cal.setTime(time2);
				//cal.add(Calendar.HOUR, 2);
				//time2=cal.getTime();	
				//ev1.setEnd_date(""+fe.format(fecha).toString()+" "+ti.format(time2).toString());
				ev1.setEnd_date(time2);
				ev1.setText(re.getString("text"));
				ev1.setPaciente(""+re.getInt("PacienteC_idPaciente"));
				ev1.setOdontologo(""+re.getInt("OdontologoC_IdOdontologo"));
				super.addResponseAttribute("idPaciente", ""+re.getInt("PacienteC_idPaciente"));
				super.addResponseAttribute("idOdontologo1", ""+re.getInt("OdontologoC_IdOdontologo"));
				
				events.add(ev1);
				//System.out.print(events.get(0).getEnd_date().toString());
			}
			
			con.close();
		}catch(SQLException es){
			es.printStackTrace();
		}
		
		

		return events;
	}
	
	@Override
	public DHXStatus saveEvent(DHXEv ev1, DHXStatus status) {
					//DHXEv ev=new Event();
		
				    //ev = new Event();
					con=conexion.getconexion();
					java.sql.PreparedStatement ps = null;
					java.sql.ResultSet result = null;
                   Event ev = (Event) ev1;
                   try {
                                 String query = null;
                                 String start_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(ev.getStart_date());
                                 String end_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(ev.getEnd_date());
                                 if (status == DHXStatus.UPDATE) {
                                                query = "UPDATE cita SET text=?, start_date=?, end_date=?, PacienteC_idPaciente=?,OdontologoC_IdOdontologo=? WHERE idCita=?";
                                                ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                                                ps.setString(1, ev.getText());
                                                ps.setString(2, start_date);
                                                ps.setString(3, end_date);
                                                
                                                ps.setInt(4, Integer.parseInt(super.getRequest().getParameter("idPaciente")));
                                                if (this.idOdontologo != null && this.idOdontologo.length() > 0)
                                                	ps.setInt(5, Integer.parseInt(this.idOdontologo));
                                                else
                                                	ps.setInt(5, Integer.parseInt(super.getRequest().getParameter("idOdontologo1")));
                                                
                                                ps.setInt(6, ev.getId());
                                                //System.out.print(event.);
                                              //  System.out.print("***PACIENTE:"+ev.getidPaciente());
                                 } else if (status == DHXStatus.INSERT) {
                                                query = "INSERT INTO cita (idCita,text, start_date, end_date,PacienteC_idPaciente,OdontologoC_IdOdontologo) VALUES (null, ?,?, ?, ?,?)";
                                                ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                                                ps.setString(1, ev.getText());
                                                ps.setString(2, start_date);
                                                ps.setString(3, end_date);
                                               //ps.setInt(4, Integer.parseInt(ev.getPaciente()));
                                                ps.setInt(4, Integer.parseInt(super.getRequest().getParameter("idPaciente")));
                                                System.out.println("idOdontologo>>>"+idOdontologo);
                                                if (this.idOdontologo != null && this.idOdontologo.length() > 0)
                                                	ps.setInt(5, Integer.parseInt(this.idOdontologo));
                                                else
                                                	ps.setInt(5, Integer.parseInt(super.getRequest().getParameter("idOdontologo1")));
                                               //System.out.println("++++++++++++ "+ev.getPaciente());
                        

                                 } else if (status == DHXStatus.DELETE) {
                                                query = "DELETE FROM cita WHERE idCita=? LIMIT 1";
                                                ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                                                ps.setInt(1, ev.getId());
                                 }

                                 if (ps!=null) {
                                                ps.executeUpdate();
                                                result = ps.getGeneratedKeys();
                                                if (result.next()) {
                                                	ev1.setId(result.getInt(1));
                                                               
                                                }
                                 }

                   } catch (SQLException e) {
                                 e.printStackTrace();
                   } finally {
            if (result != null) try { result.close(); } catch (SQLException e) {}
            if (ps != null) try { ps.close(); } catch (SQLException e) {}
            if (con != null) try { con.close(); } catch (SQLException e) {}
        }

                   return status;
    }

    @Override
    public DHXEv createEvent(String id, DHXStatus status) {
                   return new Event();
    }
    
    
    @Override
	public HashMap<String, Iterable<DHXCollection>> getCollections() {
		//List<User> users = getUsers();
		//ArrayList<DHXCollection> users_list = new ArrayList<DHXCollection>();
		//for (int i = 0; i < users.size(); i++) {
		//	users_list.add(new DHXCollection(users.get(i).getId().toString(), users.get(i).getName()));
	//	}

		HashMap<String,Iterable<DHXCollection>> c = new HashMap<String,Iterable<DHXCollection>>();
		//c.put("users", users_list);

		//ArrayList<DHXCollection> status = new ArrayList<DHXCollection>();
		//status.add(new DHXCollection("pending", "Pending"));
		//status.add(new DHXCollection("started", "Started"));
		//status.add(new DHXCollection("completed", "Completed"));
		//c.put("status", status);
		
		con=conexion.getconexion();
		ArrayList<DHXCollection> paciente = new ArrayList<DHXCollection>();
		ArrayList<DHXCollection> odontologo = new ArrayList<DHXCollection>();
		try {
			Statement s1=con.createStatement();
			Statement s2=con.createStatement();
			ResultSet re=s1.executeQuery("SELECT IdPaciente,concat(Pnombre1,' ',Pnombre2,' ',Papellido1,' ',Papellido2) as nombre FROM paciente where idClinica="+this.idClinica+"; ");
			ResultSet re1=s2.executeQuery("SELECT idOdontologo,concat(Onombre1,' ',Oapellido1) as nombre FROM odontologo where idClinica="+this.idClinica);
			while(re.next()){
				
				paciente.add(new Paciente(""+re.getInt("IdPaciente"), re.getString("nombre")));
				
			}
			while(re1.next()){
				
				odontologo.add(new Odontologo(""+re1.getInt("idOdontologo"), re1.getString("nombre")));
				
			}
			c.put("paciente", paciente);
			c.put("odontologo", odontologo);
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		}
		
		
		return c;
	}

    
    
}
	
