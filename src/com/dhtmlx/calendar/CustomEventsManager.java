package com.dhtmlx.calendar;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;






//import org.apache.struts.demoapp_struts.util.HibernateUtil;
import org.hibernate.Session;
//import org.hibernate.criterion.Restrictions;

//import Dao.BaseDao;

import com.dhtmlx.hibernateUtil.HibernateUtil;
import com.dhtmlx.planner.DHXEv;
import com.dhtmlx.planner.DHXEventsManager;
import com.dhtmlx.planner.DHXStatus;
import com.dhtmlx.planner.data.DHXCollection;

import conexion.Conexion;

public class CustomEventsManager extends DHXEventsManager {
	private java.sql.Connection con=null;
	//private BaseDao basedao=new BaseDao();
	public Conexion conexion=new Conexion();
	private String idOdontologo;
	public CustomEventsManager(HttpServletRequest request,String idOdontologo) {
		super(request);
		this.idOdontologo=idOdontologo;
	}

	
	public Iterable<DHXEv> getEvents() {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		//Session session = basedao.getCurrentSession()
		
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		List<DHXEv> evs = new ArrayList<DHXEv>();
		try {
			session= HibernateUtil.getSessionFactory().openSession();
			evs = (List<DHXEv>)session.createCriteria(Event.class).list();
		} catch (RuntimeException ex) {
			throw new RuntimeException(ex);
		} finally{
			session.flush();
			session.close();
		}
		
    	return evs;
	}

	@Override
	public DHXStatus saveEvent(DHXEv event, DHXStatus status) {
		//HttpSession s = getRequest().getSession(true);
		//Boolean employee = !s.getAttribute("type").toString().equals("manager");

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();

			if (status == DHXStatus.UPDATE) {
				session.update(event);
			} else if (status == DHXStatus.DELETE) {
				//if (employee) return DHXStatus.ERROR;
				session.delete(event);
			} else if (status == DHXStatus.INSERT) {
				//if (employee) return DHXStatus.ERROR;
				session.save(event);
			}
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally{
			session.flush();
			session.close();
		}
		return status;
	}

	@Override
	public DHXEv createEvent(String id, DHXStatus status) {
		return new Event();
	}

	@Override
	public HashMap<String, Iterable<DHXCollection>> getCollections() {
		//List<User> users = getUsers();
		//ArrayList<DHXCollection> users_list = new ArrayList<DHXCollection>();
		//for (int i = 0; i < users.size(); i++) {
		//	users_list.add(new DHXCollection(users.get(i).getId().toString(), users.get(i).getName()));
		//}

		HashMap<String,Iterable<DHXCollection>> c = new HashMap<String,Iterable<DHXCollection>>();
		//c.put("users", users_list);

		//ArrayList<DHXCollection> status = new ArrayList<DHXCollection>();
		//status.add(new DHXCollection("pending", "Pending"));
		//status.add(new DHXCollection("started", "Started"));
		//status.add(new DHXCollection("completed", "Completed"));
		//c.put("status", status);

		con=conexion.getconexion();
		ArrayList<DHXCollection> paciente = new ArrayList<DHXCollection>();
		try {
			Statement s=con.createStatement();
			ResultSet re=s.executeQuery("SELECT IdPaciente,concat(Pnombre1,' ',Pnombre2,' ',Papellido1,' ',Papellido2) as nombre FROM swgpo.paciente where Odontologo_idOdontologo="+this.idOdontologo+"; ");
			while(re.next()){
				
				paciente.add(new DHXCollection(""+re.getInt("IdPaciente"), re.getString("nombre")));
				
			}
			c.put("paciente", paciente);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		}
		
		
		return c;
	}

	/*public List<User> getUsers() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<User> users = new ArrayList<User>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			users = session.createCriteria(User.class).add(Restrictions.eq("type", "employee")).list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally{
			session.flush();
			session.close();
		}
		return users;
	}

	public User getUser(String id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<User> users = new ArrayList<User>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			users = session.createCriteria(User.class).add(Restrictions.eq("id", Integer.parseInt(id))).list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally{
			session.flush();
			session.close();
		}
		if (users.size() > 0)
			return users.get(0);
		else
			return null;
	}*/
}