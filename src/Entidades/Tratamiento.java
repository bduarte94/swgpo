
package Entidades;

public class Tratamiento {
    private int idtratamiento;
    private int tipotratamiento;
    private String descripcion;
    private float duracion;
    private float costo;

    /**
     * @return the idtratamiento
     */
    public int getIdtratamiento() {
        return idtratamiento;
    }

    /**
     * @param idtratamiento the idtratamiento to set
     */
    public void setIdtratamiento(int idtratamiento) {
        this.idtratamiento = idtratamiento;
    }

    /**
     * @return the tipotratamiento
     */
    public int getTipotratamiento() {
        return tipotratamiento;
    }

    /**
     * @param tipotratamiento the tipotratamiento to set
     */
    public void setTipotratamiento(int tipotratamiento) {
        this.tipotratamiento = tipotratamiento;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the duracion
     */
    public float getDuracion() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    /**
     * @return the costo
     */
    public float getCosto() {
        return costo;
    }

    /**
     * @param costo the costo to set
     */
    public void setCosto(float costo) {
        this.costo = costo;
    }
    
    
}
