/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Jimmy
 */
public class Diente {
    private int idDiente;
    private String nombrDiente;

    /**
     * @return the idDiente
     */
    public int getIdDiente() {
        return idDiente;
    }

    /**
     * @param idDiente the idDiente to set
     */
    public void setIdDiente(int idDiente) {
        this.idDiente = idDiente;
    }

    /**
     * @return the nombrDiente
     */
    public String getNombrDiente() {
        return nombrDiente;
    }

    /**
     * @param nombrDiente the nombrDiente to set
     */
    public void setNombrDiente(String nombrDiente) {
        this.nombrDiente = nombrDiente;
    }
    
}
