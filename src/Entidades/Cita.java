package Entidades;

import java.sql.Date;
import java.sql.Time;

public class Cita {
	private int idCita;
	private Date Fecha;
	private Time Hora;
	private String Descripcion;
	private int Estado_idEstado;
	
	public int getidCita(){
		return idCita;
	}
	public void setidCita(int idCita){
		this.idCita=idCita;
	}
	
	public Date getFecha(){
		return Fecha;
	}
	public void setFechaAtencion(Date Fecha){
		this.Fecha=Fecha;
	}
	
	public Time getHora(){
		return Hora;
	}
	public void setHora(Time Hora){
		this.Hora=Hora;
	}
	
	public String getDescripcion(){
		return Descripcion;
	}
	public void setDescripcion(String Descripcion){
		this.Descripcion=Descripcion;
	}
	
	public int getEstado_idEstado(){
		return idCita;
	}
	public void setEstado_idEstado(int Estado_idEstado){
		this.Estado_idEstado=Estado_idEstado;
	}
	
	
}
