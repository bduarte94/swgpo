package Entidades;

import java.sql.Date;

import com.dhtmlx.planner.data.DHXCollection;

public class Paciente extends DHXCollection {
	private int idPaciente;
	private String Nombre1;
	private String Nombre2;
	private String Apellido1;
	private String Apellido2;
	private String NombreCompleto;
	private String telefono;
	private String movil;
	private String companySeguro;
	private String numeroSeguro;
	private int Odontologo_id;
	private int Estado_id;
	private String grupoSanguineo;
	private Date fechaNac;
	private String Ntarjeta;
	private String Npoliza;
	private String observacion;
	private String email;
	private String ciudad;
	
	
	public Paciente(String value, String label) {
		super(value, label);
		setidPaciente(Integer.parseInt(value));
		SetNombreCompleto(label);
		// TODO Auto-generated constructor stub
	}
	
	protected void render() {
      	setLabel(this.NombreCompleto);
	}
	public void SetNombreCompleto(String nc){
		this.NombreCompleto = nc;
		render();
	}
	public String getNombreCompleto(){
		return this.NombreCompleto;
	}
	
	public int getidPacienten(){
		return idPaciente;
	}
	public void setidPaciente(int idPaciente){
		this.idPaciente=idPaciente;
	}
	public int getOdontologo_id(){
		return Odontologo_id;
	}
	public void setOdontologo_id(int Odontologo_id){
		this.Odontologo_id=Odontologo_id;
	}
	
	public int getEstado_id(){
		return  Estado_id;
	}
	public void setEstado_id(int  Estado_id){
		this.Estado_id= Estado_id;
	}
	
	public String getNombre1(){
		return  Nombre1;
	}
	public void setNombre1(String  Nombre1){
		this.Nombre1= Nombre1;
		//render();
	}
	public String getNombre2(){
		return  Nombre2;
	}
	public void setNombre2(String  Nombre2){
		this.Nombre2= Nombre2;
		//render();
	}
	public String getApellido1(){
		return  Apellido1;
	}
	public void setApellido1(String  Apellido1){
		this.Apellido1= Apellido1;
		//render();
	}
	public String getApellido2(){
		return  Apellido2;
	}
	public void setApellido2(String  Apellido2){
		this.Apellido2= Apellido2;
		//render();
	}
	public String gettelefono(){
		return  telefono;
	}
	public void settelefono(String  telefono){
		this.telefono= telefono;
	}
	
	public String getmovil(){
		return  telefono;
	}
	public void setmovil(String  movil){
		this.movil= movil;
	}
	public String getcompanySeguro(){
		return  companySeguro;
	}
	public void setcompanySeguro(String  companySeguro){
		this.companySeguro= companySeguro;
	}
	
	public String getnumeroSeguro(){
		return  numeroSeguro;
	}
	public void setnumeroSeguro(String  numeroSeguro){
		this.numeroSeguro= numeroSeguro;
	}
	
	public String getgrupoSanguineo(){
		return  grupoSanguineo;
	}
	public void setgrupoSanguineo(String  grupoSanguineo){
		this.grupoSanguineo= grupoSanguineo;
	}
	public Date getfechaNac(){
		return  fechaNac;
	}
	public void setfechaNac(Date  fechaNac){
		this.fechaNac= fechaNac;
	}
	
	public String getNtarjeta(){
		return  Ntarjeta;
	}
	public void setNtarjeta(String  Ntarjeta){
		this.Ntarjeta= Ntarjeta;
	}
	public String getNpoliza(){
		return  Npoliza;
	}
	public void setNpoliza(String  Npoliza){
		this.Npoliza= Npoliza;
	}
	public String getobservacion(){
		return  observacion;
	}
	public void setobservacion(String  observacion){
		this.observacion= observacion;
	}
	
	public String getemail(){
		return  email;
	}
	public void setemail(String  email){
		this.email= email;
	}
	
	public String getciudad(){
		return  ciudad;
	}
	public void setciudad(String  ciudad){
		this.ciudad= ciudad;
	}
	
}
