
package Entidades;

public class Odontograma {
    
 	private int idOdonto;
        private int idPacienta;
        private int idDiente;
        private int idTratamiento;
        private int servicio;
        private int estadoDiente;
        private int padecimiento;
        private int atencion;
        
	
    /**
     * @return the idOdonto
     */
    public int getIdOdonto() {
        return idOdonto;
    }

    /**
     * @param idOdonto the idOdonto to set
     */
    public void setIdOdonto(int idOdonto) {
        this.idOdonto = idOdonto;
    }

    /**
     * @return the idPacienta
     */
    public int getIdPacienta() {
        return idPacienta;
    }

    /**
     * @param idPacienta the idPacienta to set
     */
    public void setIdPacienta(int idPacienta) {
        this.idPacienta = idPacienta;
    }

    /**
     * @return the idDiente
     */
    public int getIdDiente() {
        return idDiente;
    }

    /**
     * @param idDiente the idDiente to set
     */
    public void setIdDiente(int idDiente) {
        this.idDiente = idDiente;
    }

    /**
     * @return the idTratamiento
     */
    public int getIdTratamiento() {
        return idTratamiento;
    }

    /**
     * @param idTratamiento the idTratamiento to set
     */
    public void setIdTratamiento(int idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    /**
     * @return the servicio
     */
    public int getServicio() {
        return servicio;
    }

    /**
     * @param servicio the servicio to set
     */
    public void setServicio(int servicio) {
        this.servicio = servicio;
    }

    /**
     * @return the estadoDiente
     */
    public int getEstadoDiente() {
        return estadoDiente;
    }

    /**
     * @param estadoDiente the estadoDiente to set
     */
    public void setEstadoDiente(int estadoDiente) {
        this.estadoDiente = estadoDiente;
    }

    /**
     * @return the padecimiento
     */
    public int getPadecimiento() {
        return padecimiento;
    }

    /**
     * @param padecimiento the padecimiento to set
     */
    public void setPadecimiento(int padecimiento) {
        this.padecimiento = padecimiento;
    }

    /**
     * @return the atencion
     */
    public int getAtencion() {
        return atencion;
    }

    /**
     * @param atencion the atencion to set
     */
    public void setAtencion(int atencion) {
        this.atencion = atencion;
    }

    public Odontograma() {
    }

    public Odontograma(int idOdonto, int idPacienta, int idDiente, int idTratamiento, int servicio, int estadoDiente, int padecimiento, int atencion) {
        this.idOdonto = idOdonto;
        this.idPacienta = idPacienta;
        this.idDiente = idDiente;
        this.idTratamiento = idTratamiento;
        this.servicio = servicio;
        this.estadoDiente = estadoDiente;
        this.padecimiento = padecimiento;
        this.atencion = atencion;
    }
    
    
}
