
package Entidades;

public class Servicio {
    private int idservicio;
    private int tiposervicio;
    private int odontolog;
    private String nombre;
    private String descripcion;
    private float precio;

    /**
     * @return the idservicio
     */
    public int getIdservicio() {
        return idservicio;
    }

    /**
     * @param idservicio the idservicio to set
     */
    public void setIdservicio(int idservicio) {
        this.idservicio = idservicio;
    }

    /**
     * @return the tiposervicio
     */
    public int getTiposervicio() {
        return tiposervicio;
    }

    /**
     * @param tiposervicio the tiposervicio to set
     */
    public void setTiposervicio(int tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    /**
     * @return the odontolog
     */
    public int getOdontolog() {
        return odontolog;
    }

    /**
     * @param odontolog the odontolog to set
     */
    public void setOdontolog(int odontolog) {
        this.odontolog = odontolog;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
}
