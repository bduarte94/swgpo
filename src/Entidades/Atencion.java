package Entidades;

import java.sql.Date;

public class Atencion {
	private int idAtencion;
	private Date FechaAtencion;
	private int Paciente_id;
	private int odontologo_id;
	private int Cita_id;
	
	public int getidAtencion(){
		return idAtencion;
	}
	public void setidAtencion(int idAtencion){
		this.idAtencion=idAtencion;
	}
	

	public Date getFechaAtencion(){
		return FechaAtencion;
	}
	public void setFechaAtencion(Date FechaAtencion){
		this.FechaAtencion=FechaAtencion;
	}
	
	public int getPaciente_id(){
		return Paciente_id;
	}
	public void setPaciente_id(int Paciente_id){
		this.Paciente_id=Paciente_id;
	}
	
	public int getCita_id(){
		return Cita_id;
	}
	public void setCita_id(int Cita_id){
		this.Cita_id=Cita_id;
	}
	public int getodontologo_id(){
		return odontologo_id;
	}
	public void setodontologo_id(int odontologo_id){
		this.odontologo_id=odontologo_id;
	}
	
}
