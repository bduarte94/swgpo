/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import com.dhtmlx.planner.data.DHXCollection;

public class Odontologo extends DHXCollection {
	
	
  

	private int idOdontologo;
    private String Onombre1;
    private String Onombre2;
    private String Oapellido1;
    private String Oapellido2;
    private String Direccion;
    private String Telefono;

    private long movil;
    private int codigo;
    private int idestado;
    private String fechaRegistro;
    private String NombreCompleto = null;
    
    

  	public Odontologo(String value, String label) {
  		super(value, label);
  		setIdOdontologo(Integer.parseInt(value));
  		SetNombreCompleto(label);
  		// TODO Auto-generated constructor stub
  	}

    protected void render() {
      	setLabel(this.NombreCompleto);
	}
	public void SetNombreCompleto(String nc){
		this.NombreCompleto = nc;
		render();
	}
    /**
     * @return the idOdontologo
     */
    public int getIdOdontologo() {
        return idOdontologo;
    }

    /**
     * @param idOdontologo the idOdontologo to set
     */
    public void setIdOdontologo(int idOdontologo) {
        this.idOdontologo = idOdontologo;
    }

    /**
     * @return the Onombre1
     */
    public String getOnombre1() {
        return Onombre1;
    }

    /**
     * @param Onombre1 the Onombre1 to set
     */
    public void setOnombre1(String Onombre1) {
        this.Onombre1 = Onombre1;
    }

    /**
     * @return the Onombre2
     */
    public String getOnombre2() {
        return Onombre2;
    }

    /**
     * @param Onombre2 the Onombre2 to set
     */
    public void setOnombre2(String Onombre2) {
        this.Onombre2 = Onombre2;
    }

    /**
     * @return the Oapellido1
     */
    public String getOapellido1() {
        return Oapellido1;
    }

    /**
     * @param Oapellido1 the Oapellido1 to set
     */
    public void setOapellido1(String Oapellido1) {
        this.Oapellido1 = Oapellido1;
    }

    /**
     * @return the Oapellido2
     */
    public String getOapellido2() {
        return Oapellido2;
    }

    /**
     * @param Oapellido2 the Oapellido2 to set
     */
    public void setOapellido2(String Oapellido2) {
        this.Oapellido2 = Oapellido2;
    }

    /**
     * @return the Direccion
     */
    public String getDireccion() {
        return Direccion;
    }

    /**
     * @param Direccion the Direccion to set
     */
    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    /**
     * @return the Telefono
     */
    public String getTelefono() {
        return Telefono;
    }

    /**
     * @param Telefono the Telefono to set
     */
    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

   

    /**
     * @return the movil
     */
    public long getMovil() {
        return movil;
    }

    /**
     * @param movil the movil to set
     */
    public void setMovil(long movil) {
        this.movil = movil;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the idestado
     */
    public int getIdestado() {
        return idestado;
    }

    /**
     * @param idestado the idestado to set
     */
    public void setIdestado(int idestado) {
        this.idestado = idestado;
    }

    /**
     * @return the fechaRegistro
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * @param fechaRegistro the fechaRegistro to set
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    
}
