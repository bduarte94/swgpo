
package Entidades;


public class Padecimiento {
    
    private int padecimiento;
    private int idcausa;
    private int idsintoma;
    private String descripcion;

    /**
     * @return the padecimiento
     */
    public int getPadecimiento() {
        return padecimiento;
    }

    /**
     * @param padecimiento the padecimiento to set
     */
    public void setPadecimiento(int padecimiento) {
        this.padecimiento = padecimiento;
    }

    /**
     * @return the idcausa
     */
    public int getIdcausa() {
        return idcausa;
    }

    /**
     * @param idcausa the idcausa to set
     */
    public void setIdcausa(int idcausa) {
        this.idcausa = idcausa;
    }

    /**
     * @return the idsintoma
     */
    public int getIdsintoma() {
        return idsintoma;
    }

    /**
     * @param idsintoma the idsintoma to set
     */
    public void setIdsintoma(int idsintoma) {
        this.idsintoma = idsintoma;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
