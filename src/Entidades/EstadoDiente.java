
package Entidades;

public class EstadoDiente {
    private int idEstadoDiente;
    private String descripcion;

    /**
     * @return the idEstadoDiente
     */
    public int getIdEstadoDiente() {
        return idEstadoDiente;
    }

    /**
     * @param idEstadoDiente the idEstadoDiente to set
     */
    public void setIdEstadoDiente(int idEstadoDiente) {
        this.idEstadoDiente = idEstadoDiente;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
