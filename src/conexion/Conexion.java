//Clase de conexion para el servidor base de datos
//Creada por Brayam Duarte el 14 de Agosto del 2016

package conexion;


import java.sql.SQLException;


public class Conexion {
	public String source="";
	public String user="";
	public String pass="";
	private java.sql.Connection con=null;
	
	public Conexion(){
		this.source="jdbc:mysql://localhost:3306/swgpo";
		this.user="swgpo";
		this.pass="swgpo";
	
	}
	
	public java.sql.Connection getconexion(){
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			con=java.sql.DriverManager.getConnection(this.source,this.user,this.pass);
			System.out.print("Conexion a la base de datos establecida "+this.source);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return con;	
	}
	
	

}
