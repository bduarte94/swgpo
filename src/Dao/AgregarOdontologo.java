package Dao;

import java.io.IOException;


import java.io.PrintWriter;

import static java.lang.System.out;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;
import conexion.Conexion;

public class AgregarOdontologo extends HttpServlet {

	
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
	 }
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			processRequest(request, response);
	 }
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
		 		Connection conex=null;
		 		Conexion conexion=new Conexion();
				 Statement sql=null;
				 try{
							 
							//conex=(Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/swgpo","swgpo","swgpo");
							conex=conexion.getconexion();
							 sql=conex.createStatement();
				 }catch(Exception e){
						JOptionPane.showMessageDialog(null, "Error");
				 }	
			String nombre=request.getParameter("txtNom1");
			String nombre2=request.getParameter("txtNom2");
			String apellido=request.getParameter("txtApellido1");
			String apellido2=request.getParameter("txtApellido2");
			//String direccion=request.getParameter("txtDireccion");
			//String movil=request.getParameter("txtMovil");
			//String CodigoMinsa=request.getParameter("txtCodigoMinsa");
			//String fecha=request.getParameter("fecha");
			String login=request.getParameter("txtlogin");
			String pass=request.getParameter("txtpass");
			String correo=request.getParameter("txtcorreo");
			
		
			String query="INSERT INTO odontologo(Onombre1,Onombre2,Oapellido1,Oapellido2,Direccion,"+
							 "Telefono,Movil,CodigoDoc,idEstado) "+ "VALUES ('" +nombre+"',"+"'"+nombre2+"',"
							 +"'"+apellido+"',"+"'"+apellido2+"',"+"null,null,null,null,2);";
						
			int idLastOdonto=0; 
			
			try{
				 sql=conex.createStatement();
				 ResultSet re=sql.executeQuery("select auto_increment from `information_schema`.tables where TABLE_SCHEMA = 'swgpo' and TABLE_NAME = 'odontologo'");
				 if(re.next())
					 idLastOdonto=re.getInt("auto_increment");
			}catch(SQLException e){
				System.out.print(e);
			}
			
			//se envia correo electronico al usuario
			Email email = new Email();
			String de = request.getParameter("txtcorreo");
			//String clave = request.getParameter("txtContrasena");
						
			boolean resultado =email.enviarCorreoRegistroOdontologo(de,login,idLastOdonto);
			
			if (resultado){
				 out.print("Correo electronico enviado");
				 
				 try	 {
					 sql.executeUpdate(query);
					 
					 query="select MAX(IdOdontologo) from odontologo";
					 sql=conex.createStatement();
					 ResultSet re=sql.executeQuery(query);
					 
					 if(re.next())
						 idLastOdonto=re.getInt("MAX(IdOdontologo)");
					 
					 query="insert into usuario(Login,Password,OdontologoU_idOdontologo,TipoU_idTipoUser,EstadoU_idEstado,Email)"
					 		+ " values(?,?,?,?,?,?)";
					 PreparedStatement ps=conex.prepareStatement(query);
					 ps.setString(1, login);
					 ps.setString(2, pass);
					 ps.setInt(3, idLastOdonto);
					 ps.setInt(4,2);
					 ps.setInt(5,1); //cambiar estado a 2 
					 ps.setString(6, correo);
					 ps.executeUpdate();
					 
					 response.sendRedirect("RegistrarOdontologo.jsp?ok=1");
				}
				 catch(SQLException e ){ e.printStackTrace();}	
				
				 
				 
			}
			else{
				 out.print("correo no enviado");
				 response.sendRedirect("RegistrarOdontologo.jsp?ok=0");
			}
			
			
			
			
			
			
	 }

	 @Override
	 public String getServletInfo() {
			return "Short description";
	 }// </editor-fold>

}
