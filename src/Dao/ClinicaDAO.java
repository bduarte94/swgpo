package Dao;

import java.sql.ResultSet;
import java.sql.SQLException;


import java.sql.Statement;

import conexion.Conexion;


public class ClinicaDAO {
	Conexion Conexion=new Conexion();
	private java.sql.Connection con=null;
	
	public boolean RegistrarClinica(String nom, String siglas, String dir, String tel, String ruc, String pass, String user,String prop,String email,String uri) {
		java.sql.PreparedStatement stmt1=null;
		java.sql.PreparedStatement stmt2=null;
		con=Conexion.getconexion();
		boolean retorno=false;
		
		try {
			con.setAutoCommit(false);
			Statement s=con.createStatement();
			
			String sql ="Insert into usuario (Login,Password,TipoU_idTipoUser,EstadoU_IdEstado,Email)"
						+ " values(?,?,?,?,?)";
			stmt1 =con.prepareStatement(sql);
			stmt1.setString(1, user);
			stmt1.setString(2, pass);
			stmt1.setInt(3, 4);
			stmt1.setInt(4, 2);
			stmt1.setString(5, email);
			stmt1.executeUpdate();
			//stmt1.close();
			int idLastUser=-1;
			ResultSet re=s.executeQuery("select auto_increment from `information_schema`.tables where TABLE_SCHEMA = 'swgpo' and TABLE_NAME = 'usuario'");
			 if(re.next())
				 idLastUser=re.getInt("auto_increment")-1;
			
			sql="Insert into clinica (Nombre,Siglas,Telefono,Direccion,RUC,idUsuario,Propietario)"
					+ " values(?,?,?,?,?,?,?)";
			stmt2 =con.prepareStatement(sql);
			stmt2.setString(1, nom);
			stmt2.setString(2, siglas);
			stmt2.setString(3, dir);
			stmt2.setString(4, tel);
			stmt2.setString(5,ruc);
			stmt2.setInt(6, idLastUser);
			stmt2.setString(7, prop);
			stmt2.executeUpdate();
			
			//re=s.executeQuery("select auto_increment from `information_schema`.tables where TABLE_SCHEMA = 'swgpo' and TABLE_NAME = 'clinica'");
			 //if(re.next())
				// idLastUser=re.getInt("auto_increment")-1;
			
			 Email e = new Email();						
			 boolean resultado =e.enviarCorreoRegistroClinica(email,user,idLastUser,uri);
			 
			 if(resultado){
				 con.commit(); //si se envia el correo se registrara la transaccion con exito
				 retorno = true;
			 }else{
				 if (con != null) {
						System.out.println("Rollback"); 
						try {
							// deshace todos los cambios realizados en los datos
							retorno = false;
							con.rollback(); //si sucede una exepcion se genera un rollback
							 
						} catch (SQLException ex1) {
							System.err.println("No se pudo deshacer" + ex1.getMessage());
						}
					}      
			 }
				 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (con != null) {
				System.out.println("Rollback");
				try {
					// deshace todos los cambios realizados en los datos
					retorno = false;
					con.rollback();
					 
				} catch (SQLException ex1) {
					System.err
							.println("No se pudo deshacer" + ex1.getMessage());
				}
			}     
			
		}finally{
			             System.out.println( "cierra conexion a la base de datos" );    
			             try {
			                 if(stmt1!=null) stmt1.close();                
			                 //if(stmt2!=null) stmt2.close();                
			                 if(con!=null) con.close();
			             } catch (SQLException ex) {
			                 System.err.println( ex.getMessage() );    
			             }
			         }      
		return retorno;
	}
	
	public String getClinicaNombre(String idClinica){
		StringBuilder Resultado= new StringBuilder();
				try{
				con=Conexion.getconexion();
				Statement s=con.createStatement();
				ResultSet re=s.executeQuery("select * from clinica where idClinica="+idClinica);
				if(re.next()){
					Resultado.append(re.getString("Nombre"));
				}
			}catch(SQLException es){
				es.printStackTrace();
			}	
			return Resultado.toString();
	}
	
}
