/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.Properties;


import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {
	String correoINS="swgpo.uni@gmail.com";
	String contrasena="software2";
	 public boolean enviarCorreoRegistroClinica(String correoUSER,String usuario,int lastUser,String uri){
			boolean enviado= false;
			
			try{
				 
				 String host= "smtp.gmail.com";
				 Properties prop = System.getProperties();
				 
				 prop.put("mail.smtp.starttls.enable", "true");
				 prop.put("mail.smtp.host",host);
				 prop.put("mail.smtp.user",correoINS);
				 prop.put("mail.smtp.password",contrasena);
				 prop.put("mail.smtp.port", 587);
				 prop.put("mail.smtp.auth","true");
				 
				 
				 Session sesion = Session.getDefaultInstance(prop,null);
				 MimeMessage message = new MimeMessage(sesion);
				 
				 message.setFrom(new InternetAddress(correoINS));
				 
				 message.setRecipient(Message.RecipientType.TO, new InternetAddress(correoUSER));
				 message.setSubject("SOLICITUD DE CUENTA PARA "+usuario);
				 message.setText("dar clic en el siguiente enlace para validar cuenta:\n"
				 		+ uri+"/validaruser.jsp?cod="+lastUser
				 		+ "\nAtt: ODONTOWEB");
				 
				 Transport transporte = sesion.getTransport("smtp");
				 
				 transporte.connect(host,correoINS,contrasena);
				 
				 transporte.sendMessage(message, message.getAllRecipients());
				 
				 transporte.close();
				 
				 enviado=true;
			}catch(Exception e){
				 e.printStackTrace();
			}
				
			return  enviado;
			
	 }
	 
	 public boolean enviarCorreoActivacionCuenta(String correoUSER,String usuario){
			boolean enviado= false;
			
			try{
				 
				 String host= "smtp.gmail.com";
				 Properties prop = System.getProperties();
				 
				 prop.put("mail.smtp.starttls.enable", "true");
				 prop.put("mail.smtp.host",host);
				 prop.put("mail.smtp.user",correoINS);
				 prop.put("mail.smtp.password",contrasena);
				 prop.put("mail.smtp.port", 587);
				 prop.put("mail.smtp.auth","true");
				 
				 
				 Session sesion = Session.getDefaultInstance(prop,null);
				 MimeMessage message = new MimeMessage(sesion);
				 
				 message.setFrom(new InternetAddress(correoINS));
				 
				 message.setRecipient(Message.RecipientType.TO, new InternetAddress(correoUSER));
				 message.setSubject("Activacion de cuenta "+usuario);
				 message.setText("Hola usuario "+usuario+",Su cuenta ha sido activada, le invitamos a iniciar sesion en el sistema"
				 		+ "Att:ODONTOWEB automatic service");
				 
				 Transport transporte = sesion.getTransport("smtp");
				 
				 transporte.connect(host,correoINS,contrasena);
				 
				 transporte.sendMessage(message, message.getAllRecipients());
				 
				 transporte.close();
				 
				 enviado=true;
			}catch(Exception e){
				 e.printStackTrace();
			}
				
			return  enviado;
			
	 }
	 
	 public boolean enviarCorreoCredencialesOdontologo(String correoUSER,String usuario,String LastUser,String pass,String clinica){
			boolean enviado= false;
			
			try{
				 
				 String host= "smtp.gmail.com";
				 Properties prop = System.getProperties();
				 
				 prop.put("mail.smtp.starttls.enable", "true");
				 prop.put("mail.smtp.host",host);
				 prop.put("mail.smtp.user",correoINS);
				 prop.put("mail.smtp.password",contrasena);
				 prop.put("mail.smtp.port", 587);
				 prop.put("mail.smtp.auth","true");
				 
				 
				 Session sesion = Session.getDefaultInstance(prop,null);
				 MimeMessage message = new MimeMessage(sesion);
				 
				 message.setFrom(new InternetAddress(correoINS));
				 
				 message.setRecipient(Message.RecipientType.TO, new InternetAddress(correoUSER));
				 message.setSubject("Notificación de creación de credenciales para ODONTOWEB");
				 message.setText("Estimado "+usuario+"\n"
				 		+ "La clinica "+clinica+" le ha otorgado los siguientes acceso para ODONTOWEB:\n"
				 		+ "Usuario: "+LastUser+"\n"
				 		+ "Contraseña: "+pass+"\n\n"
				 		+ "SISTEMA WEB DE GESTIÓN DE PACIENTES ODONTOLÓGICOS\n"
				 		+ "ODONTOWEB");
				 
				 Transport transporte = sesion.getTransport("smtp");
				 
				 transporte.connect(host,correoINS,contrasena);
				 
				 transporte.sendMessage(message, message.getAllRecipients());
				 
				 transporte.close();
				 
				 enviado=true;
			}catch(Exception e){
				 e.printStackTrace();
			}
				
			return  enviado;
			
	 }
	 
}
