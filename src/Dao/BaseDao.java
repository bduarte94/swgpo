package Dao;

import org.hibernate.Session;

import com.dhtmlx.hibernateUtil.HibernateUtil;


public class BaseDao {

    public Session getCurrentSession(){
        return HibernateUtil.getSessionFactory().getCurrentSession();
    }
}