/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

/**
 *
 * @author Jimmy
 */
import Entidades.Atencion;
import Entidades.Diente;
import Entidades.EstadoDiente;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import Entidades.Odontograma;
import Entidades.Odontologo;
import Entidades.Paciente;
import Entidades.Padecimiento;
import Entidades.Servicio;
import Entidades.Tratamiento;
import java.sql.PreparedStatement;

public class OdontogramaDAO {
    
    public static LinkedList<Odontograma> getMedicos()
   {
      LinkedList<Odontograma> listaMedico=new LinkedList<Odontograma>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("select idOdontologo from odontologo");
         while (rs.next())
         {
            Odontograma odo = new Odontograma();
            
            odo.setIdOdonto(rs.getInt("idOdontologo"));
            
            listaMedico.add(odo);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return listaMedico;
   }
    
    public static LinkedList<Odontologo> getNombreMedico()
   {
       LinkedList <Odontologo> listaOdonto = new  LinkedList<Odontologo>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("select idOdontologo from odontologo");
                  

         while (rs.next())
         {
             
            Odontologo odo = new Odontologo();
            odo.setIdOdontologo(rs.getInt("idOdontologo"));
            listaOdonto.add(odo);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaOdonto;
   }
    
    public static LinkedList<Paciente> getidPaciente()
   {
       LinkedList <Paciente> listaPaciente = new  LinkedList<Paciente>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("select  IdPaciente from paciente");
                  

         while (rs.next())
         {
             
            Paciente pac = new Paciente();
            pac.setidPaciente(rs.getInt("IdPaciente"));
            listaPaciente.add(pac);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaPaciente;
   }
    
     public static LinkedList<Paciente> getNombrePaciente()
   {
       LinkedList <Paciente> listaPaciente = new  LinkedList<Paciente>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("select  Pnombre1, Papellido1 from paciente");
                  

         while (rs.next())
         {
             
            Paciente pac = new Paciente();
            pac.setNombre1(rs.getString("Pnombre1"));
            pac.setApellido1(rs.getString("Papellido1"));
            listaPaciente.add(pac);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaPaciente;
   }
     
     public static LinkedList<Diente> getidDiente()
   {
       LinkedList <Diente> listaDiente = new  LinkedList<Diente>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("select idDiente from Diente");
                  

         while (rs.next())
         {
             
            Diente pac = new Diente();
            pac.setIdDiente(rs.getInt("idDiente"));
            listaDiente.add(pac);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaDiente;
   }
     
      public static LinkedList<Tratamiento> getTratamiento()
   {
       LinkedList <Tratamiento> listaTratamiento = new  LinkedList<Tratamiento>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT idTratamiento FROM swgpo.tratamiento;");
                  

         while (rs.next())
         {
             
            Tratamiento tra = new Tratamiento();
            tra.setIdtratamiento(rs.getInt("idTratamiento"));
            listaTratamiento.add(tra);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaTratamiento;
   }
      
        public static LinkedList<Tratamiento> getDescripcionTratamiento()
   {
       LinkedList <Tratamiento> listaTratamiento = new  LinkedList<Tratamiento>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT Descripcion FROM swgpo.tratamiento;");
                  

         while (rs.next())
         {
             
            Tratamiento tra = new Tratamiento();
            tra.setDescripcion(rs.getString("Descripcion"));
            listaTratamiento.add(tra);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaTratamiento;
   }
      
        public static LinkedList<Servicio> getServicio()
   {
       LinkedList <Servicio> listaServicio = new  LinkedList<Servicio>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT idServicio FROM swgpo.servicio;");
                  

         while (rs.next())
         {
             
            Servicio ser = new Servicio();
            ser.setIdservicio(rs.getInt("idServicio"));
            listaServicio.add(ser);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaServicio;
   }
        
         public static LinkedList<Servicio> getNombreServicio()
   {
       LinkedList <Servicio> listaServicio = new  LinkedList<Servicio>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT Snombre FROM swgpo.servicio;");
                  

         while (rs.next())
         {
             
            Servicio ser = new Servicio();
            ser.setNombre(rs.getString("Snombre"));
            listaServicio.add(ser);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaServicio;
   }
    
         public static LinkedList<EstadoDiente> getEstadoDiente()
   {
       LinkedList <EstadoDiente> listaEstadoDiente = new  LinkedList<EstadoDiente>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT idEstadoDiente FROM swgpo.estadodiente;");
                  

         while (rs.next())
         {
             
            EstadoDiente ser = new EstadoDiente();
            ser.setIdEstadoDiente(rs.getInt("idEstadoDiente"));
            listaEstadoDiente.add(ser);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaEstadoDiente;
   }
         
            public static LinkedList<EstadoDiente> getNombreEstadoDiente()
   {
       LinkedList <EstadoDiente> listaEstadoDiente = new  LinkedList<EstadoDiente>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT DescripcionEstado FROM swgpo.estadodiente;");
                  

         while (rs.next())
         {
             
            EstadoDiente ser = new EstadoDiente();
            ser.setDescripcion(rs.getString("DescripcionEstado"));
            listaEstadoDiente.add(ser);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
       return listaEstadoDiente;
   }
         
           public static LinkedList<Padecimiento> getPadecimiento()
   {
       LinkedList <Padecimiento> listaPadecimientos = new  LinkedList<Padecimiento>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT idPadecimiento FROM swgpo.padecimiento;");
                  

         while (rs.next())
         {
             
            Padecimiento pad = new Padecimiento();
            pad.setPadecimiento(rs.getInt("idPadecimiento"));
            listaPadecimientos.add(pad);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   return listaPadecimientos;
   }
           
                public static LinkedList<Padecimiento> getDescrPadecimiento()
   {
       LinkedList <Padecimiento> listaPadecimientos = new  LinkedList<Padecimiento>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT Descripcion FROM swgpo.padecimiento;");
                  

         while (rs.next())
         {
             
            Padecimiento pad = new Padecimiento();
            pad.setDescripcion(rs.getString("Descripcion"));
            listaPadecimientos.add(pad);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   return listaPadecimientos;
   }
                
                public static LinkedList<Atencion> getAtencion()
   {
       LinkedList <Atencion> listaAtencion = new  LinkedList<Atencion>();
      try
      {
         Class.forName("org.gjt.mm.mysql.Driver");
         Connection conexion = DriverManager.getConnection(
            "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
         Statement st = conexion.createStatement();
         ResultSet rs = st.executeQuery("SELECT idAtencion FROM swgpo.atencion;");
                  

         while (rs.next())
         {
             
            Atencion pad = new Atencion();
            pad.setidAtencion(rs.getInt("idAtencion"));
            listaAtencion.add(pad);
         }
         rs.close();
         st.close();
         conexion.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   return listaAtencion;
   }
           
           public boolean insertarRegistro(String idPaciente,String idDiente,String tratamiento, String Servicio,
                                            String estado,String padecimiento,String atencion){
               
               PreparedStatement stmt = null;
               
               try{
                   
                    Class.forName("org.gjt.mm.mysql.Driver");
                    Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
                   
                   String sql = "Insert into odontograma values(?,?,?,?,?,?,?,?)";
                   stmt = conexion.prepareStatement(sql);
                   
                   stmt.setString(1, null);
                   stmt.setString(2, idPaciente);
                   stmt.setString(3, idDiente);
                   stmt.setString(4, tratamiento);
                   stmt.setString(5, Servicio);
                   stmt.setString(6, estado);
                   stmt.setString(7, padecimiento);
                   stmt.setString(8, atencion);
                   
                   
                   stmt.executeUpdate();
                   
                    if (stmt != null){
                        conexion.close();
                        stmt.close();
                        stmt = null;
                        return true;
                    }
                 
               }
               catch(Exception e){
                   return false;
               }
               
               return true;

           }
           
            public boolean editar(String idPaciente,String idDiente,String tratamiento, String Servicio,
                                            String estado,String padecimiento,String atencion,String idOdonto){
               
               Statement stmt = null;
               
               try{
                   
                    Class.forName("org.gjt.mm.mysql.Driver");
                    Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost/swgpo", "swgpo", "swgpo");
                   
                                        
                    
                   String sql = "Update odontograma set PacienteO_idPaciente ='" + idPaciente + "', DienteO_idDiente='" + idDiente +
                           "',TratamientoO_idTratamiento='"+tratamiento+"',ServicioO_idServicio='"+ Servicio+"',"
                           + "EstadoDienteO_idEstadoDiente='" + estado + "',PadecimientoO_idPadecimiento='" + padecimiento + "'"
                           + ",AtencionO_idAtencion='" + atencion + "' where idOdontograma='" + idOdonto + "'";
                       
                   
                   stmt = conexion.createStatement();
                   stmt.execute(sql);
                   
                    if (stmt != null){
                        conexion.close();
                        stmt.close();
                        stmt = null;
                        return true;
                    }
                 
               }
               catch(Exception e){
                   return false;
               }
               
               return true;
           }


}

         