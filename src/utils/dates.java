package utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class dates {
	
	//Método que calcula la edad
	public int CalculateAge(Date date){
		GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, age;         
       
        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(date);
        age = y - cal.get(Calendar.YEAR);
        //System.out.println("obteniendo año de nacimiento: "+date.getYear()+" y:"+y+a);
        if ((m < cal.get(Calendar.MONTH))|| ((m == cal.get(Calendar.MONTH)) && (d < cal.get(Calendar.DAY_OF_MONTH)))) {
                --age;
        }
        if(age < 0)
                throw new IllegalArgumentException("Age < 0");
        return age;
	} 
}
