package Login;

import java.io.IOException;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpSession;

import utils.StringUtils;
import Peticion.BasePeticiones;
import conexion.Conexion;
public class Login extends BasePeticiones{
	private java.sql.Connection con=null;
	public Conexion conexion=new Conexion();
	
	public void iniciarSesion(){
		if(StringUtils.isNotBlank(super.getRequest().getParameter("txtUsuario")) && StringUtils.isNotBlank(super.getRequest().getParameter("txtPasword"))){
			String user=super.getRequest().getParameter("txtUsuario");
			String password=super.getRequest().getParameter("txtPasword");
			int idUser = 0;
			int idOdontologo=0;
			int estado=0;
			String stalogin = "";
			String staPass = "";
			int tipoUser=0;
			ResultSet re=null;
			try{
				
				con=conexion.getconexion();
				Statement s=con.createStatement();
				re=s.executeQuery("select * from usuario,tipousuario where login='"+user+"' and Password='"+password+"' and TipoU_idTipoUser=idTipoUsuario;");
				//si fuera el caso que necesitAMOS insertar valores se ocupa s.executeUpdate
				while(re.next()){
					stalogin=re.getString("Login");
					idUser=re.getInt("idUsuario");
					staPass=re.getString("Password");
					tipoUser=re.getInt("idTipoUsuario");
					if(re.getObject("OdontologoU_idOdontologo")!=null)
					idOdontologo=re.getInt("OdontologoU_idOdontologo");
					estado=re.getInt("EstadoU_idEstado");
				}
				
				HttpSession Session = super.getRequest().getSession();
				
				if(user.compareTo(stalogin)==0&&password.compareTo(staPass)==0&&estado==1){
					Session.setAttribute("idUser", idUser);
					Session.setAttribute("session", true);
					Session.setAttribute("login", user);
					Session.setAttribute("tipoUser", tipoUser);
					if(tipoUser==1){ //el tipo de usuario 1 equivale a Administrador del sistema						
						super.getResponse().sendRedirect("InicioAdmin.jsp");
					}
					else{
						String sql="";
						if (tipoUser == 2){ //usuario odontologo
							sql = "SELECT idClinica,idOdontologo FROM odontologo WHERE idUsuario="+idUser;
							re=s.executeQuery(sql);
							if(re.next()){
								Session.setAttribute("idClinica", re.getInt("idClinica"));
								Session.setAttribute("idOdontologo", re.getInt("idOdontologo"));
							}
							re.close();
							con.close();
						}else if (tipoUser == 4){ //usuario admon clinica
							sql = "SELECT idClinica FROM clinica WHERE idUsuario="+idUser;
							re=s.executeQuery(sql);
							if(re.next()){
								Session.setAttribute("idClinica", re.getInt("idClinica"));
							}
							con.close();
						}else if (tipoUser==5){//usuario asistente
							con.close();
						}else if (tipoUser==6){//usuario paciente
							con.close();
						}
							//Session.setAttribute("idOdontologo", idOdontologo);
						super.getResponse().sendRedirect("Inicio.jsp");
					}
				}else{
					if(!(user.compareTo(stalogin)==0&&password.compareTo(staPass)==0))
						super.getResponse().sendRedirect("Index.jsp?log=nok");
					if(estado!=1)
						super.getResponse().sendRedirect("Index.jsp?log=nok2");
				}
		}catch(SQLException es){
			es.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else{
			try {
				super.getResponse().sendRedirect("Index.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
	}
	
}
