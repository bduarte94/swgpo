package Peticion;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BasePeticiones {
  private HttpServletRequest request;
  private HttpServletResponse response;

  public HttpServletRequest getRequest() {
    return request;
  }

  public void setRequest(HttpServletRequest request) {
    this.request = request;
  }

  public HttpServletResponse getResponse() {
    return response;
  }

  public void setResponse(HttpServletResponse response) {
    this.response = response;
  }
  
  public String getContextPath(){
	  return request.getServletContext().getRealPath("/");
  }

  public void enviarRespuestaHtml(String respuestaHtml) throws IOException{
    HttpServletResponse respuestaHttp = this.response;
    respuestaHttp.setContentType("text/html;charset=UTF-8");
    respuestaHttp.getWriter().write(respuestaHtml);
  }
}
