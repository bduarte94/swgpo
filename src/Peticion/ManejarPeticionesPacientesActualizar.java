package Peticion;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;

import conexion.Conexion;

/**
 * Servlet implementation class ManejarPeticionesPacientesActualizar
 */
@WebServlet(description = "actualiza informacion de los pacientes", urlPatterns = { "/ManejarPeticionesPacientesActualizar" })
public class ManejarPeticionesPacientesActualizar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManejarPeticionesPacientesActualizar() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private String dirName;
    private java.sql.Connection con=null;
    public Conexion conexion=new Conexion();
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // read the uploadDir from the servlet parameters
        dirName= this.getServletContext().getRealPath("/");

        if (dirName == null) {
            throw new ServletException("Please supply uploadDir parameter");
        }
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}
	
	private void processRequest(HttpServletRequest request,HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		@SuppressWarnings("deprecation")
		//String dirName = request.getRealPath("/");
		HttpSession session =request.getSession();
		int idClinica=Integer.parseInt(session.getAttribute("idClinica").toString());
		int idPaciente=0;
		PreparedStatement stmt = null;
		PreparedStatement stmt1 = null;
		PreparedStatement stmt2 = null;
		Connection conexion=null;
		try{
			conexion = this.conexion.getconexion();
			 MultipartRequest multi=new MultipartRequest(request,dirName,5*1024*1024);
			 idPaciente=Integer.parseInt(multi.getParameter("idPaciente"));
            conexion.setAutoCommit(false); 
            String sql = "Update paciente set Pnombre1=?,Pnombre2=?,Papellido1=?,Papellido2=?,telefono=?,movil=?,companySeguro=?,NumeroSeguro=?,Estado_idEstado=?,GrupoSanguineo=?,FechaNac=?,Observacion=?,Email=?,Ciudad=?,Sexo=?,Direccion=? where idPaciente="+idPaciente;
            String sql1="Update historial set Ant_Personales=?,Ant_familiares=?,Alerguias=?,Cirugias=?,Tratamientos=? where idPaciente="+idPaciente;
            stmt = conexion.prepareStatement(sql);
            stmt1= conexion.prepareStatement(sql1);
            Enumeration params = multi.getParameterNames(); //se obtienen todos los parametros desde el jsp
          while(params.hasMoreElements()) {
                String name = (String)params.nextElement();
                String value = multi.getParameter(name);
  
         
          if(name.equals("txtNomPac"))
        	  stmt.setString(1, value);
          if(name.equals("txtNomPac2"))
        	  stmt.setString(2, value);
          if(name.equals("txtApelPac"))
        	  stmt.setString(3, value);
          if(name.equals("txtApelPac2"))
        	  stmt.setString(4, value);
          if(name.equals("txtTel"))
        	  stmt.setString(5, value);
          if(name.equals("txtMov"))
        	  stmt.setString(6, value);
          if(name.equals("txtseguro"))
        	  stmt.setString(7, value);
          if(name.equals("txtNseguro"))
        	  stmt.setString(8, value);
          if(name.equals("txtEstado"))
        	  stmt.setString(9, value);
          if(name.equals("txtTipoSangre"))
        	  stmt.setString(10, value);
          if(name.equals("fechaNac"))
        	  stmt.setString(11,value);
          //if(name.equals("txtTarjeta"))
          	//stmt.setString(12,null);
          //if(name.equals("txtPoliza"))
          	//stmt.setString(14,null);
          if(name.equals("txtObservacion"))
        	  stmt.setString(12,value);
          if(name.equals("txtEmail"))
        	  stmt.setString(13,value); 
          if(name.equals("ciudad"))
        	  stmt.setString(14,value);
          if(name.equals("sexo"))
        	  stmt.setString(15,value);
          if(name.equals("txtDireccion"))
        	  stmt.setString(16,value);
          
          //parametros historial
          if(name.equals("antPer"))
        	  stmt1.setString(1, value);
          if(name.equals("antFam"))
        	  stmt1.setString(2, value);
          if(name.equals("Ale"))
        	  stmt1.setString(3, value);
       	  if(name.equals("CIR"))
       		 stmt1.setString(4, value);	  
 		  if(name.equals("TRAT"))
 			 stmt1.setString(5, value);		  
      	  
          
            }
            /*stmt.setString(1, null);
            stmt.setInt(10,1); //estado de paciente en activo
            stmt.setObject(20, null);//parametro de usuario
            stmt.setInt(21, idClinica);//parametro de clinica
*/            stmt.executeUpdate();
          //se obtiene ultimo id para registrar fotografia
          /*int idPaciente=0;
          Statement s=conexion.createStatement();
			ResultSet re=s.executeQuery("SELECT MAX(idPaciente) AS id FROM paciente");
			while(re.next()){
				idPaciente=re.getInt("id");
			}
			stmt1.setString(1, null);
			stmt1.setInt(2, idPaciente);
			stmt1.setString(8, null); */
			stmt1.executeUpdate();
			
             //System.out.print(idPaciente+"***************");
             Enumeration files=multi.getFileNames(); //Se obtienen archivos desdel el JSP (fotos)
			 File f=null;
			// Part filePart = null;
			 //InputStream inputStream = null;
             while(files.hasMoreElements()){
            	 //System.out.print("******ciclo while*********");
			 	 String name=(String)files.nextElement();
                 System.out.println("name1: "+multi.getFilesystemName(name));
                 String filename=multi.getFilesystemName(new String(name.getBytes(Charset.forName("UTF-8")), Charset.forName("iso-8859-1")));
                 System.out.println("filename: "+filename);
                 String type=multi.getContentType(name);
                 f=multi.getFile(name);
               /*  try {
					filePart = request.getPart(name);
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
                 if (f != null) {
                 //if (filePart != null) {
              	   byte[] bFile = new byte[(int) f.length()];
                     	System.out.print("***SIZE: "+f.length()+"**"+name);
              	   	FileInputStream fis = new FileInputStream(f);
                    fis.read(bFile);
                   // inputStream = filePart.getInputStream(); 
                    Statement s=conexion.createStatement();
    				ResultSet re=s.executeQuery("select * from foto where PacienteF_idPaciente="+idPaciente);
    				int idFoto=-1;
    				if(re.next())
    					idFoto=re.getInt("idFoto");
    				if(idFoto==-1){
    					sql = "Insert into foto values(?,?,?,?,?,?)";
                        stmt2 = conexion.prepareStatement(sql);
                         
                        stmt2.setString(1,null);
                        stmt2.setString(2,filename);
                        stmt2.setBytes(3, bFile);
                       // stmt2.setBlob(3, inputStream);
                        stmt2.setString(4,type);
                        stmt2.setInt(5,idPaciente);
                        stmt2.setString(6,null);
    				}else
    				{
    					sql = "update foto set FotoName=?,foto=?,Extension=? where idFoto="+idFoto; 
    					stmt2 = conexion.prepareStatement(sql);
    	                stmt2.setString(1,filename);
    	                stmt2.setBytes(2, bFile);
    	                   // stmt2.setBlob(3, inputStream);
    	                stmt2.setString(3,type);
    				}
                    stmt2.executeUpdate();
                    fis.close();
                    stmt2.close();
                 }else{
                	 System.out.print("ERROR EN EL ARCHIVO*****************");
                 }
                 
             }
             
                      
             stmt.close();
             stmt1.close();
             conexion.commit();
             conexion.close();
             response.sendRedirect("ActualizarPaciente.jsp?idPaciente="+idPaciente+"&reg=ok");
				      
		}catch(SQLException es){
			es.printStackTrace();
			//if(conexion!=null) 
			try {
					conexion.rollback();
					response.sendRedirect("ActualizarPaciente.jsp?idPaciente="+idPaciente+"&reg=nok");
					
				 	
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			 try {
				 conexion.rollback();
				 response.sendRedirect("ActualizarPaciente.jsp?idPaciente="+idPaciente+"&reg=nok");
					
				} catch (IOException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
		}
	
	}

}
