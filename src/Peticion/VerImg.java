package Peticion;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import Common.PacienteCOM;
import conexion.Conexion;

/**
 * Servlet implementation class VerImg
 */
@WebServlet("/VerImg")
public class VerImg extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Conexion Conexion=new Conexion();
	private java.sql.Connection con=null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerImg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultSet re=null;
		
		// TODO Auto-generated method stub
		try {
	        //HttpSession sesion=request.getSession();
	        response.setContentType("image/jpeg");
	        PacienteCOM p = new PacienteCOM();
	        ServletOutputStream out2 = response.getOutputStream();
	        //String idProducto = String.valueOf(sesion.getAttribute("idPac"));
	        //int idpac = Integer.parseInt(idProducto);
	        int idpac=Integer.parseInt( request.getParameter("idPac"));
	        System.out.println(idpac);
	        byte[] imag = p.obtenImagenPac(idpac);
	        if (imag != null) {
	            
	            out2.write(imag);
	            System.out.println("Entro el if"+imag.length);
	        }
	        else{
	            //JOptionPane.showMessageDialog(null,"Else Null"); 
	        	String rutaimg="";
	        	con=Conexion.getconexion();
	        	//Statement s=con.createStatement();
	        	PreparedStatement stmt = con.prepareStatement("select Sexo from paciente where idPaciente="+idpac);
	        	re=stmt.executeQuery();
	        	if(re.next()){
	        		if(re.getString("Sexo").compareTo("Masculino")==0){
	        			rutaimg="/img/userM.png";
	        		}else
	        			rutaimg="/img/userW.jpg";
	        	}
	        	re.close();
	        	con.close();
	        	ServletContext context = getServletContext();
	        	String fullPath = context.getRealPath(rutaimg);
                	//String fullPath = context.getRealPath()//.getRealPath("/img/userW.jpg");
                	System.out.println("Entre en el else ****"+fullPath);
                	File fi = new File(fullPath);
                	out2.write( Files.readAllBytes(fi.toPath()));
                	
                
	        }
	        }catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	}

}
