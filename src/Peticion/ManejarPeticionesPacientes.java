package Peticion;

import java.io.IOException;




import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import com.oreilly.servlet.MultipartRequest;


//import com.oreilly.servlet.multipart.Part;
import javax.servlet.http.Part;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import conexion.Conexion;
/**
 * Servlet implementation class ManejarPeticionesPacientes
 */
@WebServlet("/ManejarPeticionesPacientes")
public class ManejarPeticionesPacientes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManejarPeticionesPacientes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
    private String dirName;
    private java.sql.Connection con=null;
    public Conexion conexion=new Conexion();
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // read the uploadDir from the servlet parameters
        dirName= this.getServletContext().getRealPath("/");

        if (dirName == null) {
            throw new ServletException("Please supply uploadDir parameter");
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		HttpSession session = request.getSession();
		int idClinica=Integer.parseInt(session.getAttribute("idClinica").toString());
		PreparedStatement stmt = null;
		PreparedStatement stmt1 = null;
		PreparedStatement stmt2 = null;
		Connection conexion=null;
		try{
			 
			 MultipartRequest multi=new MultipartRequest(request,dirName,5*1024*1024);
             conexion = this.conexion.getconexion();
            conexion.setAutoCommit(false); 
            String sql = "Insert into paciente values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            String sql1="Insert into historial values(?,?,?,?,?,?,?,?);";
            stmt = conexion.prepareStatement(sql);
            stmt1= conexion.prepareStatement(sql1);
            Enumeration params = multi.getParameterNames(); //se obtienen todos los parametros desde el jsp
          while(params.hasMoreElements()) {
                String name = (String)params.nextElement();
                String value = multi.getParameter(name);
  
         
          if(name.equals("txtNomPac"))
        	  stmt.setString(2, value);
          if(name.equals("txtNomPac2"))
        	  stmt.setString(3, value);
          if(name.equals("txtApelPac"))
        	  stmt.setString(4, value);
          if(name.equals("txtApelPac2"))
        	  stmt.setString(5, value);
          if(name.equals("txtTel"))
        	  stmt.setString(6, value);
          if(name.equals("txtMov"))
        	  stmt.setString(7, value);
          if(name.equals("txtseguro"))
        	  stmt.setString(8, value);
          if(name.equals("txtNseguro"))
        	  stmt.setString(9, value);
          if(name.equals("txtTipoSangre"))
        	  stmt.setString(11, value);
          if(name.equals("fechaNac"))
        	  stmt.setString(12,value);
          //if(name.equals("txtTarjeta"))
          	stmt.setString(13,null);
          //if(name.equals("txtPoliza"))
          	stmt.setString(14,null);
          if(name.equals("txtObservacion"))
        	  stmt.setString(15,value);
          if(name.equals("txtEmail"))
        	  stmt.setString(16,value); 
          if(name.equals("ciudad"))
        	  stmt.setString(17,value);
          if(name.equals("sexo"))
        	  stmt.setString(18,value);
          if(name.equals("txtDireccion"))
        	  stmt.setString(19,value);
          
          //parametros historial
          if(name.equals("antPer"))
        	  stmt1.setString(3, value);
          if(name.equals("antFam"))
        	  stmt1.setString(4, value);
          if(name.equals("ale"))
        	  stmt1.setString(5, value);
       	  if(name.equals("trat"))
       		 stmt1.setString(6, value);	  
 		  if(name.equals("cir"))
 			 stmt1.setString(7, value);		  
      	  
          
            }
            stmt.setString(1, null);
            stmt.setInt(10,1); //estado de paciente en activo
            stmt.setObject(20, null);//parametro de usuario
            stmt.setInt(21, idClinica);//parametro de clinica
            stmt.executeUpdate();
          //se obtiene ultimo id para registrar fotografia
          int idPaciente=0;
          Statement s=conexion.createStatement();
			ResultSet re=s.executeQuery("SELECT MAX(idPaciente) AS id FROM paciente");
			while(re.next()){
				idPaciente=re.getInt("id");
			}
			stmt1.setString(1, null);
			stmt1.setInt(2, idPaciente);
			stmt1.setString(8, null);
			stmt1.executeUpdate();
			
             System.out.print(idPaciente+"***************");
             Enumeration files=multi.getFileNames(); //Se obtienen archivos desdel el JSP (fotos)
			 File f=null;
			// Part filePart = null;
			 //InputStream inputStream = null;
             while(files.hasMoreElements()){
            	 System.out.print("******ciclo while*********");
			 	 String name=(String)files.nextElement();
                 System.out.println("name1: "+multi.getFilesystemName(name));
                 String filename=multi.getFilesystemName(new String(name.getBytes(Charset.forName("UTF-8")), Charset.forName("iso-8859-1")));
                 System.out.println("filename: "+filename);
                 String type=multi.getContentType(name);
                 f=multi.getFile(name);
               /*  try {
					filePart = request.getPart(name);
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
                 if (f != null) {
                 //if (filePart != null) {
              	   byte[] bFile = new byte[(int) f.length()];
                     	System.out.print("***SIZE: "+f.length()+"**"+name);
              	   	FileInputStream fis = new FileInputStream(f);
                    fis.read(bFile);
                   // inputStream = filePart.getInputStream(); 
                 
                    sql = "Insert into foto values(?,?,?,?,?,?)";
                    stmt2 = conexion.prepareStatement(sql);
                     
                    stmt2.setString(1,null);
                    stmt2.setString(2,filename);
                    stmt2.setBytes(3, bFile);
                   // stmt2.setBlob(3, inputStream);
                    stmt2.setString(4,type);
                    stmt2.setInt(5,idPaciente);
                    stmt2.setString(6,null);
                    stmt2.executeUpdate();
                    System.out.print("Insercion correcta a la base de datos");
                     //con.commit();
                    fis.close();
                    stmt2.close();
                 }else{
                	 System.out.print("ERROR EN EL ARCHIVO*****************");
                 }
                 
             }
             
           /*  InputStream inputStream = null; // input stream of the upload file

             // obtains the upload file part in this multipart request
             Part filePart;
			try {
				filePart = request.getPart("foto");
				if (filePart != null) {
	                // prints out some information for debugging
	                String filename=filePart.getName();
	                String type=filePart.getContentType();
					System.out.println(filePart.getName());
	                System.out.println(filePart.getSize());
	                System.out.println(filePart.getContentType());

	                // obtains input stream of the upload file
	                inputStream = filePart.getInputStream();
	                
	                if (inputStream != null) {
	                    // fetches input stream of the upload file for the blob column
	                    

	                    sql = "Insert into foto values(?,?,?,?,?,?)";
	                    stmt2 = conexion.prepareStatement(sql);
	                     
	                    stmt2.setString(1,null);
	                    stmt2.setString(2,filename);
	                    stmt2.setBlob(3, inputStream);
	                    stmt2.setString(4,type);
	                    stmt2.setInt(5,idPaciente);
	                    stmt2.setString(6,null);
	                    stmt2.executeUpdate();
	                    
	                }
	                
	                
	               }else{
	            	   System.out.print("ERROR EN FOTOGRAFIA");
	            	   
	               }
	            
	             
				
				
				
			} catch (IllegalStateException | ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
               */
            
             stmt.close();
             stmt1.close();
             conexion.commit();
             conexion.close();
            response.sendRedirect("RegistrarPaciente.jsp?reg=ok");          
		}catch(SQLException es){
			es.printStackTrace();
			//if(conexion!=null) 
			try {
					conexion.rollback();
				 	response.sendRedirect("RegistrarPaciente.jsp?reg=nok");
				 	
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			 try {
				 conexion.rollback();
					response.sendRedirect("RegistrarPaciente.jsp?reg=nok");
				} catch (IOException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
		}
		
	}

}
