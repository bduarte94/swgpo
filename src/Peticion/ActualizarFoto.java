package Peticion;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;
import conexion.Conexion;
/**
 * Servlet implementation class ActualizarFoto
 */
@WebServlet("/ActualizarFoto")
public class ActualizarFoto extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String dirName = null;
    public Conexion conexion=new Conexion();   
    /**
     * @see HttpServlet#HttpServlet()
     */
	public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // read the uploadDir from the servlet parameters
        dirName= this.getServletContext().getRealPath("/");

        if (dirName == null) {
            throw new ServletException("Please supply uploadDir parameter");
        }
    }
	
    public ActualizarFoto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		HttpSession session = request.getSession();
		int idOdontologo=Integer.parseInt(session.getAttribute("idOdontologo").toString());
		int idPaciente=0;  
		PreparedStatement stmt2 = null;
		   try{
			   
			   MultipartRequest multi=new MultipartRequest(request,dirName,5*1024*1024);
			 
             Connection conexion = this.conexion.getconexion();
            		 
             Enumeration files=multi.getFileNames(); 
             Enumeration param=multi.getParameterNames();
			 while(param.hasMoreElements()){
				 String name = (String)param.nextElement();
	               String value = multi.getParameter(name);
	               
	              if(name.compareTo("idPaciente")==0)
	            	  idPaciente=Integer.parseInt(value);
			 }
             
             
             File f=null;
             while(files.hasMoreElements()){
            	 System.out.print("******ciclo while*********");
			 	String name=(String)files.nextElement();
                 System.out.println("name1: "+multi.getFilesystemName(name));
                 String filename=multi.getFilesystemName(new String(name.getBytes(Charset.forName("UTF-8")), Charset.forName("iso-8859-1")));
                 System.out.println("filename: "+filename);
                 String type=multi.getContentType(name);
                 f=multi.getFile(name);
                 if (f != null) {
              	   byte[] bFile = new byte[(int) f.length()];
                     System.out.print("********"+f.length()+"**"+name);
              	   		FileInputStream fis = new FileInputStream(f);
                     fis.read(bFile);
                     
                 
                     
                     String sql = "update foto set FotoName=?,foto=?,Extension=?,FechaRegistro=? where PacienteF_idPaciente="+idPaciente;
                     System.out.println(sql);
                     stmt2 = conexion.prepareStatement(sql);
                     
                     // stmt2.setString(1,null);
                     stmt2.setString(1,filename);
                     stmt2.setBlob(2, fis);
                     stmt2.setString(3,type);
                     //stmt2.setInt(4,Integer.parseInt(request.getParameter("idPaciente")));
                     stmt2.setString(4,null);
                     
                     stmt2.executeUpdate();
                     //con.commit();
                     fis.close();
                 }else{
                	 System.out.print("ERROR EN EL ARCHIVO*****************");
                 }
                 
                 
             }
             
             if (stmt2 != null){
                 conexion.close();
                 stmt2.close();
                 stmt2 = null;
               }
             
            response.sendRedirect("EditarFoto.jsp?idPaciente="+idPaciente+"&reg=ok");          
		}catch(SQLException es){
			es.printStackTrace();
			 try {
					response.sendRedirect("EditarFoto.jsp?idPaciente="+idPaciente+"&reg=nok");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			 try {
					response.sendRedirect("EditarFoto.jsp??idPaciente="+idPaciente+"&reg=nok");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
		}
		
	}

}
