package Peticion;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ManejarPeticiones")
public class ManejarPeticiones extends HttpServlet {
    private static final long serialVersionUID = 1L;
   
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    protected void doPost(  HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //Le mandamos un parametro que haga match con una clase especifica y un metodo especifico
        //Se utiliza instropeccion para crear una instancia de la clase que viene en el request como parametro.
        try{
            request.setCharacterEncoding("iso-8859-1");
            //Obtener la clase que vamos a instanciar
            Class<?> action = Class.forName(request.getParameter("class").toString());
            Object actionObject = action.newInstance();

            //Setear parametros de request y response en el objeto instanciado esto es inyeccion de dependencias.
            Class<?> requestClass = HttpServletRequest.class;
            Object requestParam = request;
            Class<?> respondeClass = HttpServletResponse.class;
            Object responseParam = response;
            Method setRequest = action.getMethod("setRequest", requestClass);
            setRequest.invoke(actionObject, requestParam);
            Method setReponse = action.getMethod("setResponse", respondeClass);
            setReponse.invoke(actionObject, responseParam);

            //Metodo de inicio
            Method startPoint = action.getMethod(request.getParameter("method").toString());
            startPoint.invoke(actionObject);
        } catch(ClassNotFoundException notFoundClass){
            //Si no encontramos el metodo entonces la operacion es invalida.
            //deberiamos de poder encontrarlo porque sabemos lo que estamos buscando.
            response.sendRedirect("invalido.jsp");
            notFoundClass.printStackTrace();
        } catch (InstantiationException instantationException) {
            instantationException.printStackTrace();
        } catch (IllegalAccessException illegalAccessException) {
            illegalAccessException.printStackTrace();
        } catch (NoSuchMethodException noMethodException) {
            //Si no encontramos el metodo entonces la operacion es invalida.
            //deberiamos de poder encontrarlo porque sabemos lo que estamos buscando.
            response.sendRedirect("invalido.jsp");
            noMethodException.printStackTrace();
        } catch (SecurityException securityException) {
            securityException.printStackTrace();
        } catch (IllegalArgumentException argumentException) {
            argumentException.printStackTrace();
        } catch (InvocationTargetException invocationException) {
            invocationException.printStackTrace();
        }
    }
}